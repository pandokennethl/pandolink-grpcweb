import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class RequestAccessed extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): RequestAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): RequestAccessed;
  hasUserData(): boolean;
  clearUserData(): RequestAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RequestAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: RequestAccessed): RequestAccessed.AsObject;
  static serializeBinaryToWriter(message: RequestAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RequestAccessed;
  static deserializeBinaryFromReader(message: RequestAccessed, reader: jspb.BinaryReader): RequestAccessed;
}

export namespace RequestAccessed {
  export type AsObject = {
    requestGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class RequestUpdated extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): RequestUpdated;

  getBefore(): RequestUpdatedModel | undefined;
  setBefore(value?: RequestUpdatedModel): RequestUpdated;
  hasBefore(): boolean;
  clearBefore(): RequestUpdated;

  getAfter(): RequestUpdatedModel | undefined;
  setAfter(value?: RequestUpdatedModel): RequestUpdated;
  hasAfter(): boolean;
  clearAfter(): RequestUpdated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): RequestUpdated;
  hasUserData(): boolean;
  clearUserData(): RequestUpdated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RequestUpdated.AsObject;
  static toObject(includeInstance: boolean, msg: RequestUpdated): RequestUpdated.AsObject;
  static serializeBinaryToWriter(message: RequestUpdated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RequestUpdated;
  static deserializeBinaryFromReader(message: RequestUpdated, reader: jspb.BinaryReader): RequestUpdated;
}

export namespace RequestUpdated {
  export type AsObject = {
    requestGuid: string,
    before?: RequestUpdatedModel.AsObject,
    after?: RequestUpdatedModel.AsObject,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class TemplateAcessed extends jspb.Message {
  getTemplateGuid(): string;
  setTemplateGuid(value: string): TemplateAcessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): TemplateAcessed;
  hasUserData(): boolean;
  clearUserData(): TemplateAcessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateAcessed.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateAcessed): TemplateAcessed.AsObject;
  static serializeBinaryToWriter(message: TemplateAcessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateAcessed;
  static deserializeBinaryFromReader(message: TemplateAcessed, reader: jspb.BinaryReader): TemplateAcessed;
}

export namespace TemplateAcessed {
  export type AsObject = {
    templateGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class SignatoryInfoAccessed extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): SignatoryInfoAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): SignatoryInfoAccessed;
  hasUserData(): boolean;
  clearUserData(): SignatoryInfoAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SignatoryInfoAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: SignatoryInfoAccessed): SignatoryInfoAccessed.AsObject;
  static serializeBinaryToWriter(message: SignatoryInfoAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SignatoryInfoAccessed;
  static deserializeBinaryFromReader(message: SignatoryInfoAccessed, reader: jspb.BinaryReader): SignatoryInfoAccessed;
}

export namespace SignatoryInfoAccessed {
  export type AsObject = {
    signatoryGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class SignatoryUserChanged extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): SignatoryUserChanged;

  getChangedUserIdsList(): Array<string>;
  setChangedUserIdsList(value: Array<string>): SignatoryUserChanged;
  clearChangedUserIdsList(): SignatoryUserChanged;
  addChangedUserIds(value: string, index?: number): SignatoryUserChanged;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): SignatoryUserChanged;
  hasUserData(): boolean;
  clearUserData(): SignatoryUserChanged;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SignatoryUserChanged.AsObject;
  static toObject(includeInstance: boolean, msg: SignatoryUserChanged): SignatoryUserChanged.AsObject;
  static serializeBinaryToWriter(message: SignatoryUserChanged, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SignatoryUserChanged;
  static deserializeBinaryFromReader(message: SignatoryUserChanged, reader: jspb.BinaryReader): SignatoryUserChanged;
}

export namespace SignatoryUserChanged {
  export type AsObject = {
    signatoryGuid: string,
    changedUserIdsList: Array<string>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class DocumentActionTaken extends jspb.Message {
  getDocumentGuid(): string;
  setDocumentGuid(value: string): DocumentActionTaken;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): DocumentActionTaken;

  getRequestGuid(): string;
  setRequestGuid(value: string): DocumentActionTaken;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): DocumentActionTaken;
  hasUserData(): boolean;
  clearUserData(): DocumentActionTaken;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentActionTaken.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentActionTaken): DocumentActionTaken.AsObject;
  static serializeBinaryToWriter(message: DocumentActionTaken, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentActionTaken;
  static deserializeBinaryFromReader(message: DocumentActionTaken, reader: jspb.BinaryReader): DocumentActionTaken;
}

export namespace DocumentActionTaken {
  export type AsObject = {
    documentGuid: string,
    signatoryGuid: string,
    requestGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class SendRequestLink extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): SendRequestLink;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): SendRequestLink;

  getContactType(): pando_api_shared_pb.ContactType;
  setContactType(value: pando_api_shared_pb.ContactType): SendRequestLink;

  getContactInfo(): string;
  setContactInfo(value: string): SendRequestLink;

  getContactOverridden(): boolean;
  setContactOverridden(value: boolean): SendRequestLink;

  getRequestedBy(): pando_api_shared_pb.UserMetadata | undefined;
  setRequestedBy(value?: pando_api_shared_pb.UserMetadata): SendRequestLink;
  hasRequestedBy(): boolean;
  clearRequestedBy(): SendRequestLink;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendRequestLink.AsObject;
  static toObject(includeInstance: boolean, msg: SendRequestLink): SendRequestLink.AsObject;
  static serializeBinaryToWriter(message: SendRequestLink, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendRequestLink;
  static deserializeBinaryFromReader(message: SendRequestLink, reader: jspb.BinaryReader): SendRequestLink;
}

export namespace SendRequestLink {
  export type AsObject = {
    userId: string,
    signatoryGuid: string,
    contactType: pando_api_shared_pb.ContactType,
    contactInfo: string,
    contactOverridden: boolean,
    requestedBy?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class RequestUpdatedModel extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): RequestUpdatedModel;

  getSignatoryGuidsList(): Array<string>;
  setSignatoryGuidsList(value: Array<string>): RequestUpdatedModel;
  clearSignatoryGuidsList(): RequestUpdatedModel;
  addSignatoryGuids(value: string, index?: number): RequestUpdatedModel;

  getDocumentGuidsList(): Array<string>;
  setDocumentGuidsList(value: Array<string>): RequestUpdatedModel;
  clearDocumentGuidsList(): RequestUpdatedModel;
  addDocumentGuids(value: string, index?: number): RequestUpdatedModel;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): RequestUpdatedModel;
  hasUserData(): boolean;
  clearUserData(): RequestUpdatedModel;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RequestUpdatedModel.AsObject;
  static toObject(includeInstance: boolean, msg: RequestUpdatedModel): RequestUpdatedModel.AsObject;
  static serializeBinaryToWriter(message: RequestUpdatedModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RequestUpdatedModel;
  static deserializeBinaryFromReader(message: RequestUpdatedModel, reader: jspb.BinaryReader): RequestUpdatedModel;
}

export namespace RequestUpdatedModel {
  export type AsObject = {
    requestGuid: string,
    signatoryGuidsList: Array<string>,
    documentGuidsList: Array<string>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export enum Events { 
  REQUEST_CREATED = 0,
  REQUEST_RETRIEVED = 1,
  REQUEST_DELETED = 2,
  REQUEST_COMPLETED = 3,
  REQUEST_UPDATED = 10,
  TEMPLATE_CREATED = 20,
  TEMPLATE_RETRIEVED = 21,
  TEMPLATE_UPDATED = 22,
  TEMPLATE_DELETED = 23,
  SIGNATURE_ADOPTED = 30,
  SIGNATORY_USER_ADDED = 40,
  SIGNATORY_USER_REMOVED = 41,
  DOCUMENT_SIGNED = 50,
  DOCUMENT_DECLINED = 51,
  DOCUMENT_ACKNOWLEDGED = 52,
  DOCUMENT_OPT_IN = 53,
  DOCUMENT_OPT_OUT = 54,
  DOCUMENT_RETRIEVED = 55,
  SEND_REQUEST_LINK = 60,
}
