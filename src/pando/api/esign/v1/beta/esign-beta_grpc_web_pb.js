/**
 * @fileoverview gRPC-Web generated client stub for pando.api.esign.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('../../../../../google/api/annotations_pb.js')

var pando_api_extensions_pb = require('../../../../../pando/api/extensions_pb.js')

var pando_api_shared_pb = require('../../../../../pando/api/shared_pb.js')
const proto = {};
proto.pando = {};
proto.pando.api = {};
proto.pando.api.esign = {};
proto.pando.api.esign.v1 = require('./esign-beta_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.esign.v1.ESignBetaServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.esign.v1.ESignBetaServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.esign.v1.ExecClaimSignatoryRequest,
 *   !proto.pando.api.Empty>}
 */
const methodDescriptor_ESignBetaService_ExecClaimSignatory = new grpc.web.MethodDescriptor(
  '/pando.api.esign.v1.ESignBetaService/ExecClaimSignatory',
  grpc.web.MethodType.UNARY,
  proto.pando.api.esign.v1.ExecClaimSignatoryRequest,
  pando_api_shared_pb.Empty,
  /**
   * @param {!proto.pando.api.esign.v1.ExecClaimSignatoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  pando_api_shared_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.pando.api.esign.v1.ExecClaimSignatoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.esign.v1.ESignBetaServiceClient.prototype.execClaimSignatory =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.esign.v1.ESignBetaService/ExecClaimSignatory',
      request,
      metadata || {},
      methodDescriptor_ESignBetaService_ExecClaimSignatory,
      callback);
};


/**
 * @param {!proto.pando.api.esign.v1.ExecClaimSignatoryRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.Empty>}
 *     Promise that resolves to the response
 */
proto.pando.api.esign.v1.ESignBetaServicePromiseClient.prototype.execClaimSignatory =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.esign.v1.ESignBetaService/ExecClaimSignatory',
      request,
      metadata || {},
      methodDescriptor_ESignBetaService_ExecClaimSignatory);
};


module.exports = proto.pando.api.esign.v1;

