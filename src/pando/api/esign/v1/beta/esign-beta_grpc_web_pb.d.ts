import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_esign_v1_beta_esign$beta_pb from '../../../../../pando/api/esign/v1/beta/esign-beta_pb';


export class ESignBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execClaimSignatory(
    request: pando_api_esign_v1_beta_esign$beta_pb.ExecClaimSignatoryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class ESignBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execClaimSignatory(
    request: pando_api_esign_v1_beta_esign$beta_pb.ExecClaimSignatoryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

