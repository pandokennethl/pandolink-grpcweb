import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../../pando/api/esign/v1/shared_pb';
import * as pando_api_esign_v1_admin_pb from '../../../../../pando/api/esign/v1/admin_pb';


export class CreateESignBetaRequest extends jspb.Message {
  getDocumentsList(): Array<pando_api_esign_v1_admin_pb.ESignDocument>;
  setDocumentsList(value: Array<pando_api_esign_v1_admin_pb.ESignDocument>): CreateESignBetaRequest;
  clearDocumentsList(): CreateESignBetaRequest;
  addDocuments(value?: pando_api_esign_v1_admin_pb.ESignDocument, index?: number): pando_api_esign_v1_admin_pb.ESignDocument;

  getSignatoriesList(): Array<CreateSignatoryInfo>;
  setSignatoriesList(value: Array<CreateSignatoryInfo>): CreateESignBetaRequest;
  clearSignatoriesList(): CreateESignBetaRequest;
  addSignatories(value?: CreateSignatoryInfo, index?: number): CreateSignatoryInfo;

  getRecord(): pando_api_shared_pb.DataRecord | undefined;
  setRecord(value?: pando_api_shared_pb.DataRecord): CreateESignBetaRequest;
  hasRecord(): boolean;
  clearRecord(): CreateESignBetaRequest;

  getRecordGuid(): string;
  setRecordGuid(value: string): CreateESignBetaRequest;

  getSendLink(): boolean;
  setSendLink(value: boolean): CreateESignBetaRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateESignBetaRequest;

  getDataSourceCase(): CreateESignBetaRequest.DataSourceCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateESignBetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateESignBetaRequest): CreateESignBetaRequest.AsObject;
  static serializeBinaryToWriter(message: CreateESignBetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateESignBetaRequest;
  static deserializeBinaryFromReader(message: CreateESignBetaRequest, reader: jspb.BinaryReader): CreateESignBetaRequest;
}

export namespace CreateESignBetaRequest {
  export type AsObject = {
    documentsList: Array<pando_api_esign_v1_admin_pb.ESignDocument.AsObject>,
    signatoriesList: Array<CreateSignatoryInfo.AsObject>,
    record?: pando_api_shared_pb.DataRecord.AsObject,
    recordGuid: string,
    sendLink: boolean,
    organizationCode: string,
  }

  export enum DataSourceCase { 
    DATA_SOURCE_NOT_SET = 0,
    RECORD = 3,
    RECORD_GUID = 4,
  }
}

export class GetTemplatesByTemplateVersionRequest extends jspb.Message {
  getTemplateSvcTemplateVersionGuid(): string;
  setTemplateSvcTemplateVersionGuid(value: string): GetTemplatesByTemplateVersionRequest;

  getPage(): number;
  setPage(value: number): GetTemplatesByTemplateVersionRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetTemplatesByTemplateVersionRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetTemplatesByTemplateVersionRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetTemplatesByTemplateVersionRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetTemplatesByTemplateVersionRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplatesByTemplateVersionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplatesByTemplateVersionRequest): GetTemplatesByTemplateVersionRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplatesByTemplateVersionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplatesByTemplateVersionRequest;
  static deserializeBinaryFromReader(message: GetTemplatesByTemplateVersionRequest, reader: jspb.BinaryReader): GetTemplatesByTemplateVersionRequest;
}

export namespace GetTemplatesByTemplateVersionRequest {
  export type AsObject = {
    templateSvcTemplateVersionGuid: string,
    page: number,
    organizationCode: string,
    resultsPerPage: number,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class CreateSignatoryInfo extends jspb.Message {
  getName(): string;
  setName(value: string): CreateSignatoryInfo;

  getContactInfo(): pando_api_shared_pb.ContactInfo | undefined;
  setContactInfo(value?: pando_api_shared_pb.ContactInfo): CreateSignatoryInfo;
  hasContactInfo(): boolean;
  clearContactInfo(): CreateSignatoryInfo;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): CreateSignatoryInfo;

  getUserId(): string;
  setUserId(value: string): CreateSignatoryInfo;

  getClaimCode(): string;
  setClaimCode(value: string): CreateSignatoryInfo;

  getSignatoryOwnerCase(): CreateSignatoryInfo.SignatoryOwnerCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateSignatoryInfo.AsObject;
  static toObject(includeInstance: boolean, msg: CreateSignatoryInfo): CreateSignatoryInfo.AsObject;
  static serializeBinaryToWriter(message: CreateSignatoryInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateSignatoryInfo;
  static deserializeBinaryFromReader(message: CreateSignatoryInfo, reader: jspb.BinaryReader): CreateSignatoryInfo;
}

export namespace CreateSignatoryInfo {
  export type AsObject = {
    name: string,
    contactInfo?: pando_api_shared_pb.ContactInfo.AsObject,
    isRequired: boolean,
    userId: string,
    claimCode: string,
  }

  export enum SignatoryOwnerCase { 
    SIGNATORY_OWNER_NOT_SET = 0,
    USER_ID = 4,
    CLAIM_CODE = 5,
  }
}

