/**
 * @fileoverview gRPC-Web generated client stub for pando.api.esign.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('../../../../../google/api/annotations_pb.js')

var google_protobuf_field_mask_pb = require('google-protobuf/google/protobuf/field_mask_pb.js')

var pando_api_extensions_pb = require('../../../../../pando/api/extensions_pb.js')

var pando_api_shared_pb = require('../../../../../pando/api/shared_pb.js')

var pando_api_esign_v1_shared_pb = require('../../../../../pando/api/esign/v1/shared_pb.js')

var pando_api_esign_v1_admin_pb = require('../../../../../pando/api/esign/v1/admin_pb.js')
const proto = {};
proto.pando = {};
proto.pando.api = {};
proto.pando.api.esign = {};
proto.pando.api.esign.v1 = require('./admin-beta_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.esign.v1.ESignAdminBetaServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.esign.v1.ESignAdminBetaServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.esign.v1.CreateESignBetaRequest,
 *   !proto.pando.api.esign.v1.ESignRequest>}
 */
const methodDescriptor_ESignAdminBetaService_CreateRequest = new grpc.web.MethodDescriptor(
  '/pando.api.esign.v1.ESignAdminBetaService/CreateRequest',
  grpc.web.MethodType.UNARY,
  proto.pando.api.esign.v1.CreateESignBetaRequest,
  pando_api_esign_v1_shared_pb.ESignRequest,
  /**
   * @param {!proto.pando.api.esign.v1.CreateESignBetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  pando_api_esign_v1_shared_pb.ESignRequest.deserializeBinary
);


/**
 * @param {!proto.pando.api.esign.v1.CreateESignBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.esign.v1.ESignRequest)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.esign.v1.ESignRequest>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.esign.v1.ESignAdminBetaServiceClient.prototype.createRequest =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.esign.v1.ESignAdminBetaService/CreateRequest',
      request,
      metadata || {},
      methodDescriptor_ESignAdminBetaService_CreateRequest,
      callback);
};


/**
 * @param {!proto.pando.api.esign.v1.CreateESignBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.esign.v1.ESignRequest>}
 *     Promise that resolves to the response
 */
proto.pando.api.esign.v1.ESignAdminBetaServicePromiseClient.prototype.createRequest =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.esign.v1.ESignAdminBetaService/CreateRequest',
      request,
      metadata || {},
      methodDescriptor_ESignAdminBetaService_CreateRequest);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.esign.v1.GetTemplatesByTemplateVersionRequest,
 *   !proto.pando.api.esign.v1.TemplateList>}
 */
const methodDescriptor_ESignAdminBetaService_GetTemplatesByTemplateVersion = new grpc.web.MethodDescriptor(
  '/pando.api.esign.v1.ESignAdminBetaService/GetTemplatesByTemplateVersion',
  grpc.web.MethodType.UNARY,
  proto.pando.api.esign.v1.GetTemplatesByTemplateVersionRequest,
  pando_api_esign_v1_shared_pb.TemplateList,
  /**
   * @param {!proto.pando.api.esign.v1.GetTemplatesByTemplateVersionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  pando_api_esign_v1_shared_pb.TemplateList.deserializeBinary
);


/**
 * @param {!proto.pando.api.esign.v1.GetTemplatesByTemplateVersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.esign.v1.TemplateList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.esign.v1.TemplateList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.esign.v1.ESignAdminBetaServiceClient.prototype.getTemplatesByTemplateVersion =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.esign.v1.ESignAdminBetaService/GetTemplatesByTemplateVersion',
      request,
      metadata || {},
      methodDescriptor_ESignAdminBetaService_GetTemplatesByTemplateVersion,
      callback);
};


/**
 * @param {!proto.pando.api.esign.v1.GetTemplatesByTemplateVersionRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.esign.v1.TemplateList>}
 *     Promise that resolves to the response
 */
proto.pando.api.esign.v1.ESignAdminBetaServicePromiseClient.prototype.getTemplatesByTemplateVersion =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.esign.v1.ESignAdminBetaService/GetTemplatesByTemplateVersion',
      request,
      metadata || {},
      methodDescriptor_ESignAdminBetaService_GetTemplatesByTemplateVersion);
};


module.exports = proto.pando.api.esign.v1;

