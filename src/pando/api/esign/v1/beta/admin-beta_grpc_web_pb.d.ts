import * as grpcWeb from 'grpc-web';

import * as pando_api_esign_v1_beta_admin$beta_pb from '../../../../../pando/api/esign/v1/beta/admin-beta_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../../pando/api/esign/v1/shared_pb';


export class ESignAdminBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createRequest(
    request: pando_api_esign_v1_beta_admin$beta_pb.CreateESignBetaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  getTemplatesByTemplateVersion(
    request: pando_api_esign_v1_beta_admin$beta_pb.GetTemplatesByTemplateVersionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.TemplateList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.TemplateList>;

}

export class ESignAdminBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createRequest(
    request: pando_api_esign_v1_beta_admin$beta_pb.CreateESignBetaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  getTemplatesByTemplateVersion(
    request: pando_api_esign_v1_beta_admin$beta_pb.GetTemplatesByTemplateVersionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.TemplateList>;

}

