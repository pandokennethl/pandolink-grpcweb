import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';


export class ExecClaimSignatoryRequest extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): ExecClaimSignatoryRequest;

  getClaimCode(): string;
  setClaimCode(value: string): ExecClaimSignatoryRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecClaimSignatoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecClaimSignatoryRequest): ExecClaimSignatoryRequest.AsObject;
  static serializeBinaryToWriter(message: ExecClaimSignatoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecClaimSignatoryRequest;
  static deserializeBinaryFromReader(message: ExecClaimSignatoryRequest, reader: jspb.BinaryReader): ExecClaimSignatoryRequest;
}

export namespace ExecClaimSignatoryRequest {
  export type AsObject = {
    signatoryGuid: string,
    claimCode: string,
  }
}

