import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_esign_v1_admin_pb from '../../../../pando/api/esign/v1/admin_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../pando/api/esign/v1/shared_pb';


export class ESignAdminServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createRequest(
    request: pando_api_esign_v1_admin_pb.CreateESignRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  createOrUpdateTemplate(
    request: pando_api_esign_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.Template>;

  getRequest(
    request: pando_api_esign_v1_admin_pb.GetESignRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  getRequestList(
    request: pando_api_esign_v1_admin_pb.GetESignRequestList,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequestList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequestList>;

  getTemplate(
    request: pando_api_esign_v1_admin_pb.GetTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.Template>;

  getTemplateList(
    request: pando_api_esign_v1_admin_pb.GetTemplateListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.TemplateList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.TemplateList>;

  getTemplateReports(
    request: pando_api_esign_v1_admin_pb.GetTemplateReportsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_admin_pb.GetTemplateReportsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_admin_pb.GetTemplateReportsResponse>;

  deleteRequest(
    request: pando_api_esign_v1_admin_pb.GetESignRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteTemplate(
    request: pando_api_esign_v1_admin_pb.GetTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execAddSignatory(
    request: pando_api_esign_v1_admin_pb.ExecSignatoryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  execRemoveSignatory(
    request: pando_api_esign_v1_admin_pb.ExecSignatoryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  execAddDocument(
    request: pando_api_esign_v1_admin_pb.ExecDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

  execRemoveDocument(
    request: pando_api_esign_v1_admin_pb.ExecDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.ESignRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.ESignRequest>;

}

export class ESignAdminServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createRequest(
    request: pando_api_esign_v1_admin_pb.CreateESignRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  createOrUpdateTemplate(
    request: pando_api_esign_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.Template>;

  getRequest(
    request: pando_api_esign_v1_admin_pb.GetESignRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  getRequestList(
    request: pando_api_esign_v1_admin_pb.GetESignRequestList,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequestList>;

  getTemplate(
    request: pando_api_esign_v1_admin_pb.GetTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.Template>;

  getTemplateList(
    request: pando_api_esign_v1_admin_pb.GetTemplateListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.TemplateList>;

  getTemplateReports(
    request: pando_api_esign_v1_admin_pb.GetTemplateReportsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_admin_pb.GetTemplateReportsResponse>;

  deleteRequest(
    request: pando_api_esign_v1_admin_pb.GetESignRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteTemplate(
    request: pando_api_esign_v1_admin_pb.GetTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execAddSignatory(
    request: pando_api_esign_v1_admin_pb.ExecSignatoryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  execRemoveSignatory(
    request: pando_api_esign_v1_admin_pb.ExecSignatoryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  execAddDocument(
    request: pando_api_esign_v1_admin_pb.ExecDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

  execRemoveDocument(
    request: pando_api_esign_v1_admin_pb.ExecDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.ESignRequest>;

}

