import * as jspb from 'google-protobuf'

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class DocumentOrTemplate extends jspb.Message {
  getTemplateGuid(): string;
  setTemplateGuid(value: string): DocumentOrTemplate;

  getDocumentGuid(): string;
  setDocumentGuid(value: string): DocumentOrTemplate;

  getDataCase(): DocumentOrTemplate.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentOrTemplate.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentOrTemplate): DocumentOrTemplate.AsObject;
  static serializeBinaryToWriter(message: DocumentOrTemplate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentOrTemplate;
  static deserializeBinaryFromReader(message: DocumentOrTemplate, reader: jspb.BinaryReader): DocumentOrTemplate;
}

export namespace DocumentOrTemplate {
  export type AsObject = {
    templateGuid: string,
    documentGuid: string,
  }

  export enum DataCase { 
    DATA_NOT_SET = 0,
    TEMPLATE_GUID = 1,
    DOCUMENT_GUID = 2,
  }
}

export class Document extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): Document;

  getTemplateGuid(): string;
  setTemplateGuid(value: string): Document;

  getRecordGuid(): string;
  setRecordGuid(value: string): Document;

  getName(): string;
  setName(value: string): Document;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Document;

  getStatus(): ESignStatus;
  setStatus(value: ESignStatus): Document;

  getType(): ESignType;
  setType(value: ESignType): Document;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): Document;

  getRenderedHtml(): string;
  setRenderedHtml(value: string): Document;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): Document;

  getSignatoryName(): string;
  setSignatoryName(value: string): Document;

  getVaultDocumentGuid(): string;
  setVaultDocumentGuid(value: string): Document;

  getStatusName(): string;
  setStatusName(value: string): Document;

  getActionTypeName(): string;
  setActionTypeName(value: string): Document;

  getDateCreatedUtc(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreatedUtc(value?: google_protobuf_timestamp_pb.Timestamp): Document;
  hasDateCreatedUtc(): boolean;
  clearDateCreatedUtc(): Document;

  getDateCompletedUtc(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCompletedUtc(value?: google_protobuf_timestamp_pb.Timestamp): Document;
  hasDateCompletedUtc(): boolean;
  clearDateCompletedUtc(): Document;

  getDateActionUtc(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateActionUtc(value?: google_protobuf_timestamp_pb.Timestamp): Document;
  hasDateActionUtc(): boolean;
  clearDateActionUtc(): Document;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Document.AsObject;
  static toObject(includeInstance: boolean, msg: Document): Document.AsObject;
  static serializeBinaryToWriter(message: Document, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Document;
  static deserializeBinaryFromReader(message: Document, reader: jspb.BinaryReader): Document;
}

export namespace Document {
  export type AsObject = {
    guid: string,
    templateGuid: string,
    recordGuid: string,
    name: string,
    organizationCode: string,
    status: ESignStatus,
    type: ESignType,
    isRequired: boolean,
    renderedHtml: string,
    signatoryGuid: string,
    signatoryName: string,
    vaultDocumentGuid: string,
    statusName: string,
    actionTypeName: string,
    dateCreatedUtc?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateCompletedUtc?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateActionUtc?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class DocumentListItem extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DocumentListItem;

  getTemplateGuid(): string;
  setTemplateGuid(value: string): DocumentListItem;

  getRecordGuid(): string;
  setRecordGuid(value: string): DocumentListItem;

  getName(): string;
  setName(value: string): DocumentListItem;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): DocumentListItem;

  getStatus(): ESignStatus;
  setStatus(value: ESignStatus): DocumentListItem;

  getType(): ESignType;
  setType(value: ESignType): DocumentListItem;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): DocumentListItem;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): DocumentListItem;

  getSignatoryName(): string;
  setSignatoryName(value: string): DocumentListItem;

  getDateCreatedUtc(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreatedUtc(value?: google_protobuf_timestamp_pb.Timestamp): DocumentListItem;
  hasDateCreatedUtc(): boolean;
  clearDateCreatedUtc(): DocumentListItem;

  getVaultDocumentGuid(): string;
  setVaultDocumentGuid(value: string): DocumentListItem;

  getDateCompletedUtc(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCompletedUtc(value?: google_protobuf_timestamp_pb.Timestamp): DocumentListItem;
  hasDateCompletedUtc(): boolean;
  clearDateCompletedUtc(): DocumentListItem;

  getActionType(): ActionType;
  setActionType(value: ActionType): DocumentListItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentListItem.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentListItem): DocumentListItem.AsObject;
  static serializeBinaryToWriter(message: DocumentListItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentListItem;
  static deserializeBinaryFromReader(message: DocumentListItem, reader: jspb.BinaryReader): DocumentListItem;
}

export namespace DocumentListItem {
  export type AsObject = {
    guid: string,
    templateGuid: string,
    recordGuid: string,
    name: string,
    organizationCode: string,
    status: ESignStatus,
    type: ESignType,
    isRequired: boolean,
    signatoryGuid: string,
    signatoryName: string,
    dateCreatedUtc?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    vaultDocumentGuid: string,
    dateCompletedUtc?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    actionType: ActionType,
  }
}

export class DocumentList extends jspb.Message {
  getDocumentsList(): Array<DocumentListItem>;
  setDocumentsList(value: Array<DocumentListItem>): DocumentList;
  clearDocumentsList(): DocumentList;
  addDocuments(value?: DocumentListItem, index?: number): DocumentListItem;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): DocumentList;
  hasPagination(): boolean;
  clearPagination(): DocumentList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentList.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentList): DocumentList.AsObject;
  static serializeBinaryToWriter(message: DocumentList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentList;
  static deserializeBinaryFromReader(message: DocumentList, reader: jspb.BinaryReader): DocumentList;
}

export namespace DocumentList {
  export type AsObject = {
    documentsList: Array<DocumentListItem.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class ESignRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): ESignRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): ESignRequest;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): ESignRequest;
  hasDateCreated(): boolean;
  clearDateCreated(): ESignRequest;

  getStatus(): ESignStatus;
  setStatus(value: ESignStatus): ESignRequest;

  getSignatoryGuidsList(): Array<string>;
  setSignatoryGuidsList(value: Array<string>): ESignRequest;
  clearSignatoryGuidsList(): ESignRequest;
  addSignatoryGuids(value: string, index?: number): ESignRequest;

  getDocumentGuidsList(): Array<string>;
  setDocumentGuidsList(value: Array<string>): ESignRequest;
  clearDocumentGuidsList(): ESignRequest;
  addDocumentGuids(value: string, index?: number): ESignRequest;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): ESignRequest;

  getDateCompleted(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCompleted(value?: google_protobuf_timestamp_pb.Timestamp): ESignRequest;
  hasDateCompleted(): boolean;
  clearDateCompleted(): ESignRequest;

  getDocumentNamesList(): Array<string>;
  setDocumentNamesList(value: Array<string>): ESignRequest;
  clearDocumentNamesList(): ESignRequest;
  addDocumentNames(value: string, index?: number): ESignRequest;

  getSignatoryNamesList(): Array<string>;
  setSignatoryNamesList(value: Array<string>): ESignRequest;
  clearSignatoryNamesList(): ESignRequest;
  addSignatoryNames(value: string, index?: number): ESignRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESignRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ESignRequest): ESignRequest.AsObject;
  static serializeBinaryToWriter(message: ESignRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESignRequest;
  static deserializeBinaryFromReader(message: ESignRequest, reader: jspb.BinaryReader): ESignRequest;
}

export namespace ESignRequest {
  export type AsObject = {
    guid: string,
    organizationCode: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    status: ESignStatus,
    signatoryGuidsList: Array<string>,
    documentGuidsList: Array<string>,
    isDeleted: boolean,
    dateCompleted?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    documentNamesList: Array<string>,
    signatoryNamesList: Array<string>,
  }
}

export class ESignRequestList extends jspb.Message {
  getEsignRequestsList(): Array<ESignRequest>;
  setEsignRequestsList(value: Array<ESignRequest>): ESignRequestList;
  clearEsignRequestsList(): ESignRequestList;
  addEsignRequests(value?: ESignRequest, index?: number): ESignRequest;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): ESignRequestList;
  hasPagination(): boolean;
  clearPagination(): ESignRequestList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESignRequestList.AsObject;
  static toObject(includeInstance: boolean, msg: ESignRequestList): ESignRequestList.AsObject;
  static serializeBinaryToWriter(message: ESignRequestList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESignRequestList;
  static deserializeBinaryFromReader(message: ESignRequestList, reader: jspb.BinaryReader): ESignRequestList;
}

export namespace ESignRequestList {
  export type AsObject = {
    esignRequestsList: Array<ESignRequest.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class Template extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): Template;

  getTemplateSvcTemplateVersionGuid(): string;
  setTemplateSvcTemplateVersionGuid(value: string): Template;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Template;

  getName(): string;
  setName(value: string): Template;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): Template;

  getEsignType(): ESignType;
  setEsignType(value: ESignType): Template;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Template.AsObject;
  static toObject(includeInstance: boolean, msg: Template): Template.AsObject;
  static serializeBinaryToWriter(message: Template, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Template;
  static deserializeBinaryFromReader(message: Template, reader: jspb.BinaryReader): Template;
}

export namespace Template {
  export type AsObject = {
    guid: string,
    templateSvcTemplateVersionGuid: string,
    organizationCode: string,
    name: string,
    isDeleted: boolean,
    esignType: ESignType,
  }
}

export class TemplateList extends jspb.Message {
  getTemplatesList(): Array<Template>;
  setTemplatesList(value: Array<Template>): TemplateList;
  clearTemplatesList(): TemplateList;
  addTemplates(value?: Template, index?: number): Template;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): TemplateList;
  hasPagination(): boolean;
  clearPagination(): TemplateList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateList.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateList): TemplateList.AsObject;
  static serializeBinaryToWriter(message: TemplateList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateList;
  static deserializeBinaryFromReader(message: TemplateList, reader: jspb.BinaryReader): TemplateList;
}

export namespace TemplateList {
  export type AsObject = {
    templatesList: Array<Template.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class SignatoryInfo extends jspb.Message {
  getName(): string;
  setName(value: string): SignatoryInfo;

  getContactInfo(): pando_api_shared_pb.ContactInfo | undefined;
  setContactInfo(value?: pando_api_shared_pb.ContactInfo): SignatoryInfo;
  hasContactInfo(): boolean;
  clearContactInfo(): SignatoryInfo;

  getUserId(): string;
  setUserId(value: string): SignatoryInfo;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): SignatoryInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SignatoryInfo.AsObject;
  static toObject(includeInstance: boolean, msg: SignatoryInfo): SignatoryInfo.AsObject;
  static serializeBinaryToWriter(message: SignatoryInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SignatoryInfo;
  static deserializeBinaryFromReader(message: SignatoryInfo, reader: jspb.BinaryReader): SignatoryInfo;
}

export namespace SignatoryInfo {
  export type AsObject = {
    name: string,
    contactInfo?: pando_api_shared_pb.ContactInfo.AsObject,
    userId: string,
    isRequired: boolean,
  }
}

export class Signatory extends jspb.Message {
  getSignatoryInfo(): SignatoryInfo | undefined;
  setSignatoryInfo(value?: SignatoryInfo): Signatory;
  hasSignatoryInfo(): boolean;
  clearSignatoryInfo(): Signatory;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): Signatory;

  getDetailCase(): Signatory.DetailCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Signatory.AsObject;
  static toObject(includeInstance: boolean, msg: Signatory): Signatory.AsObject;
  static serializeBinaryToWriter(message: Signatory, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Signatory;
  static deserializeBinaryFromReader(message: Signatory, reader: jspb.BinaryReader): Signatory;
}

export namespace Signatory {
  export type AsObject = {
    signatoryInfo?: SignatoryInfo.AsObject,
    signatoryGuid: string,
  }

  export enum DetailCase { 
    DETAIL_NOT_SET = 0,
    SIGNATORY_INFO = 2,
    SIGNATORY_GUID = 3,
  }
}

export class ChartData extends jspb.Message {
  getSurveyName(): string;
  setSurveyName(value: string): ChartData;

  getCompleteCount(): number;
  setCompleteCount(value: number): ChartData;

  getIncompleteCount(): number;
  setIncompleteCount(value: number): ChartData;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChartData.AsObject;
  static toObject(includeInstance: boolean, msg: ChartData): ChartData.AsObject;
  static serializeBinaryToWriter(message: ChartData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChartData;
  static deserializeBinaryFromReader(message: ChartData, reader: jspb.BinaryReader): ChartData;
}

export namespace ChartData {
  export type AsObject = {
    surveyName: string,
    completeCount: number,
    incompleteCount: number,
  }
}

export enum ESignStatus { 
  INCOMPLETE = 0,
  UNACCEPTABLE = 1,
  ACCEPTABLE = 2,
}
export enum ESignType { 
  ACKNOWLEDGEMENT = 0,
  OPT_IN = 1,
  SIGNATURE = 2,
}
export enum SignatureType { 
  GENERATED = 0,
  HANDWRITTEN = 1,
  NOT_SET = 2,
}
export enum ActionType { 
  ACKNOWLEDGE = 0,
  DECLINE = 1,
  OPTIN = 2,
  OPTOUT = 3,
  SIGN = 4,
}
