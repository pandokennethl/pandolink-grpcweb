import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_esign_v1_esign_pb from '../../../../pando/api/esign/v1/esign_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../pando/api/esign/v1/shared_pb';


export class ESignServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  adoptSignature(
    request: pando_api_esign_v1_esign_pb.AdoptSignatureRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.SignatoryRequestDetail) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.SignatoryRequestDetail>;

  getRequestDetail(
    request: pando_api_esign_v1_esign_pb.GetESignRequestDetail,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.SignatoryRequestDetail) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.SignatoryRequestDetail>;

  getDocument(
    request: pando_api_esign_v1_esign_pb.GetDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.Document) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.Document>;

  getDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.DocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.DocumentList>;

  getToDoDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.DocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.DocumentList>;

  getRecentlyCompletedDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.DocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.DocumentList>;

  getCompletedDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.DocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.DocumentList>;

  getIncompleteDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_shared_pb.DocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_shared_pb.DocumentList>;

  execSignDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.ESignRequestShort) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execAcknowledgeDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.ESignRequestShort) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execDeclineDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.ESignRequestShort) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execOptInDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.ESignRequestShort) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execOptOutDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_esign_v1_esign_pb.ESignRequestShort) => void
  ): grpcWeb.ClientReadableStream<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execAddSignatoryOwner(
    request: pando_api_esign_v1_esign_pb.ExecAddSignatoryOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execRemoveSignatoryOwner(
    request: pando_api_esign_v1_esign_pb.ExecRemoveSignatoryOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class ESignServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  adoptSignature(
    request: pando_api_esign_v1_esign_pb.AdoptSignatureRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.SignatoryRequestDetail>;

  getRequestDetail(
    request: pando_api_esign_v1_esign_pb.GetESignRequestDetail,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.SignatoryRequestDetail>;

  getDocument(
    request: pando_api_esign_v1_esign_pb.GetDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.Document>;

  getDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.DocumentList>;

  getToDoDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.DocumentList>;

  getRecentlyCompletedDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.DocumentList>;

  getCompletedDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.DocumentList>;

  getIncompleteDocumentList(
    request: pando_api_esign_v1_esign_pb.GetDocumentSummaryRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_shared_pb.DocumentList>;

  execSignDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execAcknowledgeDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execDeclineDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execOptInDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execOptOutDocument(
    request: pando_api_esign_v1_esign_pb.SignDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_esign_v1_esign_pb.ESignRequestShort>;

  execAddSignatoryOwner(
    request: pando_api_esign_v1_esign_pb.ExecAddSignatoryOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execRemoveSignatoryOwner(
    request: pando_api_esign_v1_esign_pb.ExecRemoveSignatoryOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

