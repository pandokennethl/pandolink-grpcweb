import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../pando/api/esign/v1/shared_pb';


export class CreateESignRequest extends jspb.Message {
  getDocumentsList(): Array<ESignDocument>;
  setDocumentsList(value: Array<ESignDocument>): CreateESignRequest;
  clearDocumentsList(): CreateESignRequest;
  addDocuments(value?: ESignDocument, index?: number): ESignDocument;

  getSignatoriesList(): Array<pando_api_esign_v1_shared_pb.SignatoryInfo>;
  setSignatoriesList(value: Array<pando_api_esign_v1_shared_pb.SignatoryInfo>): CreateESignRequest;
  clearSignatoriesList(): CreateESignRequest;
  addSignatories(value?: pando_api_esign_v1_shared_pb.SignatoryInfo, index?: number): pando_api_esign_v1_shared_pb.SignatoryInfo;

  getRecord(): pando_api_shared_pb.DataRecord | undefined;
  setRecord(value?: pando_api_shared_pb.DataRecord): CreateESignRequest;
  hasRecord(): boolean;
  clearRecord(): CreateESignRequest;

  getRecordGuid(): string;
  setRecordGuid(value: string): CreateESignRequest;

  getSendLink(): boolean;
  setSendLink(value: boolean): CreateESignRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateESignRequest;

  getDataSourceCase(): CreateESignRequest.DataSourceCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateESignRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateESignRequest): CreateESignRequest.AsObject;
  static serializeBinaryToWriter(message: CreateESignRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateESignRequest;
  static deserializeBinaryFromReader(message: CreateESignRequest, reader: jspb.BinaryReader): CreateESignRequest;
}

export namespace CreateESignRequest {
  export type AsObject = {
    documentsList: Array<ESignDocument.AsObject>,
    signatoriesList: Array<pando_api_esign_v1_shared_pb.SignatoryInfo.AsObject>,
    record?: pando_api_shared_pb.DataRecord.AsObject,
    recordGuid: string,
    sendLink: boolean,
    organizationCode: string,
  }

  export enum DataSourceCase { 
    DATA_SOURCE_NOT_SET = 0,
    RECORD = 3,
    RECORD_GUID = 4,
  }
}

export class CreateOrUpdateTemplateRequest extends jspb.Message {
  getTemplate(): pando_api_esign_v1_shared_pb.Template | undefined;
  setTemplate(value?: pando_api_esign_v1_shared_pb.Template): CreateOrUpdateTemplateRequest;
  hasTemplate(): boolean;
  clearTemplate(): CreateOrUpdateTemplateRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateTemplateRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateTemplateRequest): CreateOrUpdateTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateTemplateRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateTemplateRequest, reader: jspb.BinaryReader): CreateOrUpdateTemplateRequest;
}

export namespace CreateOrUpdateTemplateRequest {
  export type AsObject = {
    template?: pando_api_esign_v1_shared_pb.Template.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class GetTemplateRequest extends jspb.Message {
  getTemplateGuid(): string;
  setTemplateGuid(value: string): GetTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateRequest): GetTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateRequest;
  static deserializeBinaryFromReader(message: GetTemplateRequest, reader: jspb.BinaryReader): GetTemplateRequest;
}

export namespace GetTemplateRequest {
  export type AsObject = {
    templateGuid: string,
  }
}

export class GetTemplateListRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetTemplateListRequest;

  getIncludeDeleted(): boolean;
  setIncludeDeleted(value: boolean): GetTemplateListRequest;

  getPage(): number;
  setPage(value: number): GetTemplateListRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetTemplateListRequest;

  getFilter(): string;
  setFilter(value: string): GetTemplateListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateListRequest): GetTemplateListRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplateListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateListRequest;
  static deserializeBinaryFromReader(message: GetTemplateListRequest, reader: jspb.BinaryReader): GetTemplateListRequest;
}

export namespace GetTemplateListRequest {
  export type AsObject = {
    organizationCode: string,
    includeDeleted: boolean,
    page: number,
    resultsPerPage: number,
    filter: string,
  }
}

export class GetESignRequest extends jspb.Message {
  getEsignRequestGuid(): string;
  setEsignRequestGuid(value: string): GetESignRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetESignRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetESignRequest): GetESignRequest.AsObject;
  static serializeBinaryToWriter(message: GetESignRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetESignRequest;
  static deserializeBinaryFromReader(message: GetESignRequest, reader: jspb.BinaryReader): GetESignRequest;
}

export namespace GetESignRequest {
  export type AsObject = {
    esignRequestGuid: string,
  }
}

export class GetESignRequestList extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): GetESignRequestList;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetESignRequestList;

  getIsCompleted(): boolean;
  setIsCompleted(value: boolean): GetESignRequestList;

  getIncludeDeleted(): boolean;
  setIncludeDeleted(value: boolean): GetESignRequestList;

  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetESignRequestList;
  hasStart(): boolean;
  clearStart(): GetESignRequestList;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetESignRequestList;
  hasEnd(): boolean;
  clearEnd(): GetESignRequestList;

  getPage(): number;
  setPage(value: number): GetESignRequestList;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetESignRequestList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetESignRequestList.AsObject;
  static toObject(includeInstance: boolean, msg: GetESignRequestList): GetESignRequestList.AsObject;
  static serializeBinaryToWriter(message: GetESignRequestList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetESignRequestList;
  static deserializeBinaryFromReader(message: GetESignRequestList, reader: jspb.BinaryReader): GetESignRequestList;
}

export namespace GetESignRequestList {
  export type AsObject = {
    userId: string,
    organizationCode: string,
    isCompleted: boolean,
    includeDeleted: boolean,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    page: number,
    resultsPerPage: number,
  }
}

export class UpdateESignRequest extends jspb.Message {
  getRequest(): pando_api_esign_v1_shared_pb.ESignRequest | undefined;
  setRequest(value?: pando_api_esign_v1_shared_pb.ESignRequest): UpdateESignRequest;
  hasRequest(): boolean;
  clearRequest(): UpdateESignRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateESignRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateESignRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateESignRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateESignRequest): UpdateESignRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateESignRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateESignRequest;
  static deserializeBinaryFromReader(message: UpdateESignRequest, reader: jspb.BinaryReader): UpdateESignRequest;
}

export namespace UpdateESignRequest {
  export type AsObject = {
    request?: pando_api_esign_v1_shared_pb.ESignRequest.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class ExecSignatoryRequest extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): ExecSignatoryRequest;

  getSignatoriesList(): Array<pando_api_esign_v1_shared_pb.Signatory>;
  setSignatoriesList(value: Array<pando_api_esign_v1_shared_pb.Signatory>): ExecSignatoryRequest;
  clearSignatoriesList(): ExecSignatoryRequest;
  addSignatories(value?: pando_api_esign_v1_shared_pb.Signatory, index?: number): pando_api_esign_v1_shared_pb.Signatory;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecSignatoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecSignatoryRequest): ExecSignatoryRequest.AsObject;
  static serializeBinaryToWriter(message: ExecSignatoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecSignatoryRequest;
  static deserializeBinaryFromReader(message: ExecSignatoryRequest, reader: jspb.BinaryReader): ExecSignatoryRequest;
}

export namespace ExecSignatoryRequest {
  export type AsObject = {
    requestGuid: string,
    signatoriesList: Array<pando_api_esign_v1_shared_pb.Signatory.AsObject>,
  }
}

export class ExecDocumentRequest extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): ExecDocumentRequest;

  getDocumentsList(): Array<pando_api_esign_v1_shared_pb.DocumentOrTemplate>;
  setDocumentsList(value: Array<pando_api_esign_v1_shared_pb.DocumentOrTemplate>): ExecDocumentRequest;
  clearDocumentsList(): ExecDocumentRequest;
  addDocuments(value?: pando_api_esign_v1_shared_pb.DocumentOrTemplate, index?: number): pando_api_esign_v1_shared_pb.DocumentOrTemplate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecDocumentRequest): ExecDocumentRequest.AsObject;
  static serializeBinaryToWriter(message: ExecDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecDocumentRequest;
  static deserializeBinaryFromReader(message: ExecDocumentRequest, reader: jspb.BinaryReader): ExecDocumentRequest;
}

export namespace ExecDocumentRequest {
  export type AsObject = {
    requestGuid: string,
    documentsList: Array<pando_api_esign_v1_shared_pb.DocumentOrTemplate.AsObject>,
  }
}

export class ESignDocument extends jspb.Message {
  getTemplateGuid(): string;
  setTemplateGuid(value: string): ESignDocument;

  getIsRequred(): boolean;
  setIsRequred(value: boolean): ESignDocument;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESignDocument.AsObject;
  static toObject(includeInstance: boolean, msg: ESignDocument): ESignDocument.AsObject;
  static serializeBinaryToWriter(message: ESignDocument, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESignDocument;
  static deserializeBinaryFromReader(message: ESignDocument, reader: jspb.BinaryReader): ESignDocument;
}

export namespace ESignDocument {
  export type AsObject = {
    templateGuid: string,
    isRequred: boolean,
  }
}

export class GetTemplateReportsRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetTemplateReportsRequest;

  getIncludeCompleted(): boolean;
  setIncludeCompleted(value: boolean): GetTemplateReportsRequest;

  getIncludeIncomplete(): boolean;
  setIncludeIncomplete(value: boolean): GetTemplateReportsRequest;

  getPage(): number;
  setPage(value: number): GetTemplateReportsRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetTemplateReportsRequest;

  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetTemplateReportsRequest;
  hasStart(): boolean;
  clearStart(): GetTemplateReportsRequest;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetTemplateReportsRequest;
  hasEnd(): boolean;
  clearEnd(): GetTemplateReportsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateReportsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateReportsRequest): GetTemplateReportsRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplateReportsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateReportsRequest;
  static deserializeBinaryFromReader(message: GetTemplateReportsRequest, reader: jspb.BinaryReader): GetTemplateReportsRequest;
}

export namespace GetTemplateReportsRequest {
  export type AsObject = {
    organizationCode: string,
    includeCompleted: boolean,
    includeIncomplete: boolean,
    page: number,
    resultsPerPage: number,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GetTemplateReportsResponse extends jspb.Message {
  getDocumentsList(): Array<pando_api_esign_v1_shared_pb.Document>;
  setDocumentsList(value: Array<pando_api_esign_v1_shared_pb.Document>): GetTemplateReportsResponse;
  clearDocumentsList(): GetTemplateReportsResponse;
  addDocuments(value?: pando_api_esign_v1_shared_pb.Document, index?: number): pando_api_esign_v1_shared_pb.Document;

  getChartData(): pando_api_esign_v1_shared_pb.ChartData | undefined;
  setChartData(value?: pando_api_esign_v1_shared_pb.ChartData): GetTemplateReportsResponse;
  hasChartData(): boolean;
  clearChartData(): GetTemplateReportsResponse;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): GetTemplateReportsResponse;
  hasPagination(): boolean;
  clearPagination(): GetTemplateReportsResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateReportsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateReportsResponse): GetTemplateReportsResponse.AsObject;
  static serializeBinaryToWriter(message: GetTemplateReportsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateReportsResponse;
  static deserializeBinaryFromReader(message: GetTemplateReportsResponse, reader: jspb.BinaryReader): GetTemplateReportsResponse;
}

export namespace GetTemplateReportsResponse {
  export type AsObject = {
    documentsList: Array<pando_api_esign_v1_shared_pb.Document.AsObject>,
    chartData?: pando_api_esign_v1_shared_pb.ChartData.AsObject,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

