import * as jspb from 'google-protobuf'

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_esign_v1_shared_pb from '../../../../pando/api/esign/v1/shared_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class AdoptSignatureRequest extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): AdoptSignatureRequest;

  getType(): pando_api_esign_v1_shared_pb.SignatureType;
  setType(value: pando_api_esign_v1_shared_pb.SignatureType): AdoptSignatureRequest;

  getImage(): Uint8Array | string;
  getImage_asU8(): Uint8Array;
  getImage_asB64(): string;
  setImage(value: Uint8Array | string): AdoptSignatureRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AdoptSignatureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AdoptSignatureRequest): AdoptSignatureRequest.AsObject;
  static serializeBinaryToWriter(message: AdoptSignatureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AdoptSignatureRequest;
  static deserializeBinaryFromReader(message: AdoptSignatureRequest, reader: jspb.BinaryReader): AdoptSignatureRequest;
}

export namespace AdoptSignatureRequest {
  export type AsObject = {
    signatoryGuid: string,
    type: pando_api_esign_v1_shared_pb.SignatureType,
    image: Uint8Array | string,
  }
}

export class GetESignRequestDetail extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): GetESignRequestDetail;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetESignRequestDetail.AsObject;
  static toObject(includeInstance: boolean, msg: GetESignRequestDetail): GetESignRequestDetail.AsObject;
  static serializeBinaryToWriter(message: GetESignRequestDetail, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetESignRequestDetail;
  static deserializeBinaryFromReader(message: GetESignRequestDetail, reader: jspb.BinaryReader): GetESignRequestDetail;
}

export namespace GetESignRequestDetail {
  export type AsObject = {
    signatoryGuid: string,
  }
}

export class GetDocumentRequest extends jspb.Message {
  getDocumentGuid(): string;
  setDocumentGuid(value: string): GetDocumentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDocumentRequest): GetDocumentRequest.AsObject;
  static serializeBinaryToWriter(message: GetDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDocumentRequest;
  static deserializeBinaryFromReader(message: GetDocumentRequest, reader: jspb.BinaryReader): GetDocumentRequest;
}

export namespace GetDocumentRequest {
  export type AsObject = {
    documentGuid: string,
  }
}

export class GetDocumentListRequest extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): GetDocumentListRequest;

  getDocumentGuidList(): DocumentGuidList | undefined;
  setDocumentGuidList(value?: DocumentGuidList): GetDocumentListRequest;
  hasDocumentGuidList(): boolean;
  clearDocumentGuidList(): GetDocumentListRequest;

  getUserId(): string;
  setUserId(value: string): GetDocumentListRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetDocumentListRequest;

  getPage(): number;
  setPage(value: number): GetDocumentListRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetDocumentListRequest;

  getDetailCase(): GetDocumentListRequest.DetailCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDocumentListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDocumentListRequest): GetDocumentListRequest.AsObject;
  static serializeBinaryToWriter(message: GetDocumentListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDocumentListRequest;
  static deserializeBinaryFromReader(message: GetDocumentListRequest, reader: jspb.BinaryReader): GetDocumentListRequest;
}

export namespace GetDocumentListRequest {
  export type AsObject = {
    requestGuid: string,
    documentGuidList?: DocumentGuidList.AsObject,
    userId: string,
    organizationCode: string,
    page: number,
    resultsPerPage: number,
  }

  export enum DetailCase { 
    DETAIL_NOT_SET = 0,
    REQUEST_GUID = 1,
    DOCUMENT_GUID_LIST = 2,
    USER_ID = 3,
  }
}

export class SignDocumentRequest extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): SignDocumentRequest;

  getDocumentGuid(): string;
  setDocumentGuid(value: string): SignDocumentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SignDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SignDocumentRequest): SignDocumentRequest.AsObject;
  static serializeBinaryToWriter(message: SignDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SignDocumentRequest;
  static deserializeBinaryFromReader(message: SignDocumentRequest, reader: jspb.BinaryReader): SignDocumentRequest;
}

export namespace SignDocumentRequest {
  export type AsObject = {
    signatoryGuid: string,
    documentGuid: string,
  }
}

export class ExecAddSignatoryOwnerRequest extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): ExecAddSignatoryOwnerRequest;

  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): ExecAddSignatoryOwnerRequest;
  clearUserIdsList(): ExecAddSignatoryOwnerRequest;
  addUserIds(value: string, index?: number): ExecAddSignatoryOwnerRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecAddSignatoryOwnerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecAddSignatoryOwnerRequest): ExecAddSignatoryOwnerRequest.AsObject;
  static serializeBinaryToWriter(message: ExecAddSignatoryOwnerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecAddSignatoryOwnerRequest;
  static deserializeBinaryFromReader(message: ExecAddSignatoryOwnerRequest, reader: jspb.BinaryReader): ExecAddSignatoryOwnerRequest;
}

export namespace ExecAddSignatoryOwnerRequest {
  export type AsObject = {
    signatoryGuid: string,
    userIdsList: Array<string>,
  }
}

export class ExecRemoveSignatoryOwnerRequest extends jspb.Message {
  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): ExecRemoveSignatoryOwnerRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecRemoveSignatoryOwnerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecRemoveSignatoryOwnerRequest): ExecRemoveSignatoryOwnerRequest.AsObject;
  static serializeBinaryToWriter(message: ExecRemoveSignatoryOwnerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecRemoveSignatoryOwnerRequest;
  static deserializeBinaryFromReader(message: ExecRemoveSignatoryOwnerRequest, reader: jspb.BinaryReader): ExecRemoveSignatoryOwnerRequest;
}

export namespace ExecRemoveSignatoryOwnerRequest {
  export type AsObject = {
    signatoryGuid: string,
  }
}

export class SignatoryRequestDetail extends jspb.Message {
  getRequestGuid(): string;
  setRequestGuid(value: string): SignatoryRequestDetail;

  getSignatoryGuid(): string;
  setSignatoryGuid(value: string): SignatoryRequestDetail;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): SignatoryRequestDetail;

  getSignatoryName(): string;
  setSignatoryName(value: string): SignatoryRequestDetail;

  getSignatoryIsRequired(): boolean;
  setSignatoryIsRequired(value: boolean): SignatoryRequestDetail;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): SignatoryRequestDetail;
  hasDateCreated(): boolean;
  clearDateCreated(): SignatoryRequestDetail;

  getDocumentsList(): Array<DocumentShort>;
  setDocumentsList(value: Array<DocumentShort>): SignatoryRequestDetail;
  clearDocumentsList(): SignatoryRequestDetail;
  addDocuments(value?: DocumentShort, index?: number): DocumentShort;

  getSignatureType(): pando_api_esign_v1_shared_pb.SignatureType;
  setSignatureType(value: pando_api_esign_v1_shared_pb.SignatureType): SignatoryRequestDetail;

  getSignatureImage(): Uint8Array | string;
  getSignatureImage_asU8(): Uint8Array;
  getSignatureImage_asB64(): string;
  setSignatureImage(value: Uint8Array | string): SignatoryRequestDetail;

  getRequestStatus(): pando_api_esign_v1_shared_pb.ESignStatus;
  setRequestStatus(value: pando_api_esign_v1_shared_pb.ESignStatus): SignatoryRequestDetail;

  getSignatoryStatus(): pando_api_esign_v1_shared_pb.ESignStatus;
  setSignatoryStatus(value: pando_api_esign_v1_shared_pb.ESignStatus): SignatoryRequestDetail;

  getSignatureIndex(): number;
  setSignatureIndex(value: number): SignatoryRequestDetail;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SignatoryRequestDetail.AsObject;
  static toObject(includeInstance: boolean, msg: SignatoryRequestDetail): SignatoryRequestDetail.AsObject;
  static serializeBinaryToWriter(message: SignatoryRequestDetail, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SignatoryRequestDetail;
  static deserializeBinaryFromReader(message: SignatoryRequestDetail, reader: jspb.BinaryReader): SignatoryRequestDetail;
}

export namespace SignatoryRequestDetail {
  export type AsObject = {
    requestGuid: string,
    signatoryGuid: string,
    organizationCode: string,
    signatoryName: string,
    signatoryIsRequired: boolean,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    documentsList: Array<DocumentShort.AsObject>,
    signatureType: pando_api_esign_v1_shared_pb.SignatureType,
    signatureImage: Uint8Array | string,
    requestStatus: pando_api_esign_v1_shared_pb.ESignStatus,
    signatoryStatus: pando_api_esign_v1_shared_pb.ESignStatus,
    signatureIndex: number,
  }
}

export class GetDocumentSummaryRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): GetDocumentSummaryRequest;

  getPage(): number;
  setPage(value: number): GetDocumentSummaryRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetDocumentSummaryRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetDocumentSummaryRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDocumentSummaryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDocumentSummaryRequest): GetDocumentSummaryRequest.AsObject;
  static serializeBinaryToWriter(message: GetDocumentSummaryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDocumentSummaryRequest;
  static deserializeBinaryFromReader(message: GetDocumentSummaryRequest, reader: jspb.BinaryReader): GetDocumentSummaryRequest;
}

export namespace GetDocumentSummaryRequest {
  export type AsObject = {
    userId: string,
    page: number,
    resultsPerPage: number,
    organizationCode: string,
  }
}

export class DocumentGuidList extends jspb.Message {
  getGuidsList(): Array<string>;
  setGuidsList(value: Array<string>): DocumentGuidList;
  clearGuidsList(): DocumentGuidList;
  addGuids(value: string, index?: number): DocumentGuidList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentGuidList.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentGuidList): DocumentGuidList.AsObject;
  static serializeBinaryToWriter(message: DocumentGuidList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentGuidList;
  static deserializeBinaryFromReader(message: DocumentGuidList, reader: jspb.BinaryReader): DocumentGuidList;
}

export namespace DocumentGuidList {
  export type AsObject = {
    guidsList: Array<string>,
  }
}

export class DocumentShort extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DocumentShort;

  getStatus(): pando_api_esign_v1_shared_pb.ESignStatus;
  setStatus(value: pando_api_esign_v1_shared_pb.ESignStatus): DocumentShort;

  getType(): pando_api_esign_v1_shared_pb.ESignType;
  setType(value: pando_api_esign_v1_shared_pb.ESignType): DocumentShort;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): DocumentShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentShort.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentShort): DocumentShort.AsObject;
  static serializeBinaryToWriter(message: DocumentShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentShort;
  static deserializeBinaryFromReader(message: DocumentShort, reader: jspb.BinaryReader): DocumentShort;
}

export namespace DocumentShort {
  export type AsObject = {
    guid: string,
    status: pando_api_esign_v1_shared_pb.ESignStatus,
    type: pando_api_esign_v1_shared_pb.ESignType,
    isRequired: boolean,
  }
}

export class ESignRequestShort extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): ESignRequestShort;

  getDocumentsList(): Array<DocumentShort>;
  setDocumentsList(value: Array<DocumentShort>): ESignRequestShort;
  clearDocumentsList(): ESignRequestShort;
  addDocuments(value?: DocumentShort, index?: number): DocumentShort;

  getRequestStatus(): pando_api_esign_v1_shared_pb.ESignStatus;
  setRequestStatus(value: pando_api_esign_v1_shared_pb.ESignStatus): ESignRequestShort;

  getSignatoryStatus(): pando_api_esign_v1_shared_pb.ESignStatus;
  setSignatoryStatus(value: pando_api_esign_v1_shared_pb.ESignStatus): ESignRequestShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ESignRequestShort.AsObject;
  static toObject(includeInstance: boolean, msg: ESignRequestShort): ESignRequestShort.AsObject;
  static serializeBinaryToWriter(message: ESignRequestShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ESignRequestShort;
  static deserializeBinaryFromReader(message: ESignRequestShort, reader: jspb.BinaryReader): ESignRequestShort;
}

export namespace ESignRequestShort {
  export type AsObject = {
    guid: string,
    documentsList: Array<DocumentShort.AsObject>,
    requestStatus: pando_api_esign_v1_shared_pb.ESignStatus,
    signatoryStatus: pando_api_esign_v1_shared_pb.ESignStatus,
  }
}

