import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_template_v1_admin_pb from '../../../../pando/api/template/v1/admin_pb';


export class TemplateAdminServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getTemplate(
    request: pando_api_template_v1_admin_pb.TemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.Template>;

  getTemplateList(
    request: pando_api_template_v1_admin_pb.TemplateListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.TemplateList) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.TemplateList>;

  getTemplateVariables(
    request: pando_api_template_v1_admin_pb.TemplateVariablesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.TemplateVariables) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.TemplateVariables>;

  getTemplateVariablesWithType(
    request: pando_api_template_v1_admin_pb.TemplateVariablesWithTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.TemplateVariablesWithTypeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.TemplateVariablesWithTypeResponse>;

  getVariables(
    request: pando_api_template_v1_admin_pb.GetVariableItemsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.VariableItemList) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.VariableItemList>;

  getVariable(
    request: pando_api_template_v1_admin_pb.GetVariableItemRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.VariableItem) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.VariableItem>;

  createOrUpdateTemplate(
    request: pando_api_template_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.Template>;

  createVariable(
    request: pando_api_template_v1_admin_pb.VariableItem,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.VariableItem) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.VariableItem>;

  updateVariable(
    request: pando_api_template_v1_admin_pb.UpdateVariableItemRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.VariableItem) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.VariableItem>;

  deleteTemplate(
    request: pando_api_template_v1_admin_pb.TemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteVariable(
    request: pando_api_template_v1_admin_pb.DeleteVariableItemRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class TemplateAdminServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getTemplate(
    request: pando_api_template_v1_admin_pb.TemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.Template>;

  getTemplateList(
    request: pando_api_template_v1_admin_pb.TemplateListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.TemplateList>;

  getTemplateVariables(
    request: pando_api_template_v1_admin_pb.TemplateVariablesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.TemplateVariables>;

  getTemplateVariablesWithType(
    request: pando_api_template_v1_admin_pb.TemplateVariablesWithTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.TemplateVariablesWithTypeResponse>;

  getVariables(
    request: pando_api_template_v1_admin_pb.GetVariableItemsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.VariableItemList>;

  getVariable(
    request: pando_api_template_v1_admin_pb.GetVariableItemRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.VariableItem>;

  createOrUpdateTemplate(
    request: pando_api_template_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.Template>;

  createVariable(
    request: pando_api_template_v1_admin_pb.VariableItem,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.VariableItem>;

  updateVariable(
    request: pando_api_template_v1_admin_pb.UpdateVariableItemRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.VariableItem>;

  deleteTemplate(
    request: pando_api_template_v1_admin_pb.TemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteVariable(
    request: pando_api_template_v1_admin_pb.DeleteVariableItemRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

