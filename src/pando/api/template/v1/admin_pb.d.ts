import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class CreateOrUpdateTemplateRequest extends jspb.Message {
  getTemplate(): Template | undefined;
  setTemplate(value?: Template): CreateOrUpdateTemplateRequest;
  hasTemplate(): boolean;
  clearTemplate(): CreateOrUpdateTemplateRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateTemplateRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateTemplateRequest): CreateOrUpdateTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateTemplateRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateTemplateRequest, reader: jspb.BinaryReader): CreateOrUpdateTemplateRequest;
}

export namespace CreateOrUpdateTemplateRequest {
  export type AsObject = {
    template?: Template.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class TemplateListRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): TemplateListRequest;

  getFilter(): string;
  setFilter(value: string): TemplateListRequest;

  getPage(): number;
  setPage(value: number): TemplateListRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): TemplateListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateListRequest): TemplateListRequest.AsObject;
  static serializeBinaryToWriter(message: TemplateListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateListRequest;
  static deserializeBinaryFromReader(message: TemplateListRequest, reader: jspb.BinaryReader): TemplateListRequest;
}

export namespace TemplateListRequest {
  export type AsObject = {
    organizationCode: string,
    filter: string,
    page: number,
    resultsPerPage: number,
  }
}

export class TemplateList extends jspb.Message {
  getTemplatesList(): Array<TemplateShort>;
  setTemplatesList(value: Array<TemplateShort>): TemplateList;
  clearTemplatesList(): TemplateList;
  addTemplates(value?: TemplateShort, index?: number): TemplateShort;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): TemplateList;
  hasPagination(): boolean;
  clearPagination(): TemplateList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateList.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateList): TemplateList.AsObject;
  static serializeBinaryToWriter(message: TemplateList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateList;
  static deserializeBinaryFromReader(message: TemplateList, reader: jspb.BinaryReader): TemplateList;
}

export namespace TemplateList {
  export type AsObject = {
    templatesList: Array<TemplateShort.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class TemplateShort extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateShort;

  getTemplateVersionsList(): Array<TemplateVersion>;
  setTemplateVersionsList(value: Array<TemplateVersion>): TemplateShort;
  clearTemplateVersionsList(): TemplateShort;
  addTemplateVersions(value?: TemplateVersion, index?: number): TemplateVersion;

  getName(): string;
  setName(value: string): TemplateShort;

  getDescription(): string;
  setDescription(value: string): TemplateShort;

  getTemplateType(): TemplateType;
  setTemplateType(value: TemplateType): TemplateShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateShort.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateShort): TemplateShort.AsObject;
  static serializeBinaryToWriter(message: TemplateShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateShort;
  static deserializeBinaryFromReader(message: TemplateShort, reader: jspb.BinaryReader): TemplateShort;
}

export namespace TemplateShort {
  export type AsObject = {
    templateId: string,
    templateVersionsList: Array<TemplateVersion.AsObject>,
    name: string,
    description: string,
    templateType: TemplateType,
  }
}

export class TemplateVersion extends jspb.Message {
  getVersionId(): string;
  setVersionId(value: string): TemplateVersion;

  getDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDate(value?: google_protobuf_timestamp_pb.Timestamp): TemplateVersion;
  hasDate(): boolean;
  clearDate(): TemplateVersion;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVersion.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVersion): TemplateVersion.AsObject;
  static serializeBinaryToWriter(message: TemplateVersion, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVersion;
  static deserializeBinaryFromReader(message: TemplateVersion, reader: jspb.BinaryReader): TemplateVersion;
}

export namespace TemplateVersion {
  export type AsObject = {
    versionId: string,
    date?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class TemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateRequest;

  getTemplateVersionId(): string;
  setTemplateVersionId(value: string): TemplateRequest;

  getTemplateCase(): TemplateRequest.TemplateCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateRequest): TemplateRequest.AsObject;
  static serializeBinaryToWriter(message: TemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateRequest;
  static deserializeBinaryFromReader(message: TemplateRequest, reader: jspb.BinaryReader): TemplateRequest;
}

export namespace TemplateRequest {
  export type AsObject = {
    templateId: string,
    templateVersionId: string,
  }

  export enum TemplateCase { 
    TEMPLATE_NOT_SET = 0,
    TEMPLATE_ID = 1,
    TEMPLATE_VERSION_ID = 2,
  }
}

export class Template extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): Template;

  getName(): string;
  setName(value: string): Template;

  getDescription(): string;
  setDescription(value: string): Template;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Template;

  getTemplateHtml(): string;
  setTemplateHtml(value: string): Template;

  getTemplatePdfBytes(): Uint8Array | string;
  getTemplatePdfBytes_asU8(): Uint8Array;
  getTemplatePdfBytes_asB64(): string;
  setTemplatePdfBytes(value: Uint8Array | string): Template;

  getVariablesList(): Array<TemplateVariable>;
  setVariablesList(value: Array<TemplateVariable>): Template;
  clearVariablesList(): Template;
  addVariables(value?: TemplateVariable, index?: number): TemplateVariable;

  getTemplateType(): TemplateType;
  setTemplateType(value: TemplateType): Template;

  getTemplateVersionId(): string;
  setTemplateVersionId(value: string): Template;

  getReferencedTemplateVersionIdsList(): Array<string>;
  setReferencedTemplateVersionIdsList(value: Array<string>): Template;
  clearReferencedTemplateVersionIdsList(): Template;
  addReferencedTemplateVersionIds(value: string, index?: number): Template;

  getContentCase(): Template.ContentCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Template.AsObject;
  static toObject(includeInstance: boolean, msg: Template): Template.AsObject;
  static serializeBinaryToWriter(message: Template, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Template;
  static deserializeBinaryFromReader(message: Template, reader: jspb.BinaryReader): Template;
}

export namespace Template {
  export type AsObject = {
    templateId: string,
    name: string,
    description: string,
    organizationCode: string,
    templateHtml: string,
    templatePdfBytes: Uint8Array | string,
    variablesList: Array<TemplateVariable.AsObject>,
    templateType: TemplateType,
    templateVersionId: string,
    referencedTemplateVersionIdsList: Array<string>,
  }

  export enum ContentCase { 
    CONTENT_NOT_SET = 0,
    TEMPLATE_HTML = 5,
    TEMPLATE_PDF_BYTES = 7,
  }
}

export class TemplateVariable extends jspb.Message {
  getKey(): string;
  setKey(value: string): TemplateVariable;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): TemplateVariable;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariable.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariable): TemplateVariable.AsObject;
  static serializeBinaryToWriter(message: TemplateVariable, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariable;
  static deserializeBinaryFromReader(message: TemplateVariable, reader: jspb.BinaryReader): TemplateVariable;
}

export namespace TemplateVariable {
  export type AsObject = {
    key: string,
    isRequired: boolean,
  }
}

export class TemplateVariablesRequest extends jspb.Message {
  getTemplateVersionGuidsList(): Array<string>;
  setTemplateVersionGuidsList(value: Array<string>): TemplateVariablesRequest;
  clearTemplateVersionGuidsList(): TemplateVariablesRequest;
  addTemplateVersionGuids(value: string, index?: number): TemplateVariablesRequest;

  getDataFormat(): pando_api_shared_pb.FileType;
  setDataFormat(value: pando_api_shared_pb.FileType): TemplateVariablesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariablesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariablesRequest): TemplateVariablesRequest.AsObject;
  static serializeBinaryToWriter(message: TemplateVariablesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariablesRequest;
  static deserializeBinaryFromReader(message: TemplateVariablesRequest, reader: jspb.BinaryReader): TemplateVariablesRequest;
}

export namespace TemplateVariablesRequest {
  export type AsObject = {
    templateVersionGuidsList: Array<string>,
    dataFormat: pando_api_shared_pb.FileType,
  }
}

export class TemplateVariables extends jspb.Message {
  getData(): string;
  setData(value: string): TemplateVariables;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariables.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariables): TemplateVariables.AsObject;
  static serializeBinaryToWriter(message: TemplateVariables, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariables;
  static deserializeBinaryFromReader(message: TemplateVariables, reader: jspb.BinaryReader): TemplateVariables;
}

export namespace TemplateVariables {
  export type AsObject = {
    data: string,
  }
}

export class VariableItem extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): VariableItem;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): VariableItem;

  getType(): pando_api_shared_pb.VariableType;
  setType(value: pando_api_shared_pb.VariableType): VariableItem;

  getKeyName(): string;
  setKeyName(value: string): VariableItem;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): VariableItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VariableItem.AsObject;
  static toObject(includeInstance: boolean, msg: VariableItem): VariableItem.AsObject;
  static serializeBinaryToWriter(message: VariableItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VariableItem;
  static deserializeBinaryFromReader(message: VariableItem, reader: jspb.BinaryReader): VariableItem;
}

export namespace VariableItem {
  export type AsObject = {
    guid: string,
    organizationCode: string,
    type: pando_api_shared_pb.VariableType,
    keyName: string,
    isDeleted: boolean,
  }
}

export class VariableItemList extends jspb.Message {
  getVariablesList(): Array<VariableItem>;
  setVariablesList(value: Array<VariableItem>): VariableItemList;
  clearVariablesList(): VariableItemList;
  addVariables(value?: VariableItem, index?: number): VariableItem;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): VariableItemList;
  hasPagination(): boolean;
  clearPagination(): VariableItemList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VariableItemList.AsObject;
  static toObject(includeInstance: boolean, msg: VariableItemList): VariableItemList.AsObject;
  static serializeBinaryToWriter(message: VariableItemList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VariableItemList;
  static deserializeBinaryFromReader(message: VariableItemList, reader: jspb.BinaryReader): VariableItemList;
}

export namespace VariableItemList {
  export type AsObject = {
    variablesList: Array<VariableItem.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class GetVariableItemsRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetVariableItemsRequest;

  getFilter(): string;
  setFilter(value: string): GetVariableItemsRequest;

  getIncludeDeleted(): boolean;
  setIncludeDeleted(value: boolean): GetVariableItemsRequest;

  getPage(): number;
  setPage(value: number): GetVariableItemsRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetVariableItemsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVariableItemsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVariableItemsRequest): GetVariableItemsRequest.AsObject;
  static serializeBinaryToWriter(message: GetVariableItemsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVariableItemsRequest;
  static deserializeBinaryFromReader(message: GetVariableItemsRequest, reader: jspb.BinaryReader): GetVariableItemsRequest;
}

export namespace GetVariableItemsRequest {
  export type AsObject = {
    organizationCode: string,
    filter: string,
    includeDeleted: boolean,
    page: number,
    resultsPerPage: number,
  }
}

export class GetVariableItemRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): GetVariableItemRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVariableItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVariableItemRequest): GetVariableItemRequest.AsObject;
  static serializeBinaryToWriter(message: GetVariableItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVariableItemRequest;
  static deserializeBinaryFromReader(message: GetVariableItemRequest, reader: jspb.BinaryReader): GetVariableItemRequest;
}

export namespace GetVariableItemRequest {
  export type AsObject = {
    guid: string,
  }
}

export class UpdateVariableItemRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): UpdateVariableItemRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): UpdateVariableItemRequest;

  getType(): pando_api_shared_pb.VariableType;
  setType(value: pando_api_shared_pb.VariableType): UpdateVariableItemRequest;

  getKeyName(): string;
  setKeyName(value: string): UpdateVariableItemRequest;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): UpdateVariableItemRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateVariableItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateVariableItemRequest): UpdateVariableItemRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateVariableItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateVariableItemRequest;
  static deserializeBinaryFromReader(message: UpdateVariableItemRequest, reader: jspb.BinaryReader): UpdateVariableItemRequest;
}

export namespace UpdateVariableItemRequest {
  export type AsObject = {
    guid: string,
    organizationCode: string,
    type: pando_api_shared_pb.VariableType,
    keyName: string,
    isDeleted: boolean,
  }
}

export class DeleteVariableItemRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DeleteVariableItemRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteVariableItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteVariableItemRequest): DeleteVariableItemRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteVariableItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteVariableItemRequest;
  static deserializeBinaryFromReader(message: DeleteVariableItemRequest, reader: jspb.BinaryReader): DeleteVariableItemRequest;
}

export namespace DeleteVariableItemRequest {
  export type AsObject = {
    guid: string,
  }
}

export class TemplateVariablesWithTypeRequest extends jspb.Message {
  getTemplateVersionGuidsList(): Array<string>;
  setTemplateVersionGuidsList(value: Array<string>): TemplateVariablesWithTypeRequest;
  clearTemplateVersionGuidsList(): TemplateVariablesWithTypeRequest;
  addTemplateVersionGuids(value: string, index?: number): TemplateVariablesWithTypeRequest;

  getPage(): number;
  setPage(value: number): TemplateVariablesWithTypeRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): TemplateVariablesWithTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariablesWithTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariablesWithTypeRequest): TemplateVariablesWithTypeRequest.AsObject;
  static serializeBinaryToWriter(message: TemplateVariablesWithTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariablesWithTypeRequest;
  static deserializeBinaryFromReader(message: TemplateVariablesWithTypeRequest, reader: jspb.BinaryReader): TemplateVariablesWithTypeRequest;
}

export namespace TemplateVariablesWithTypeRequest {
  export type AsObject = {
    templateVersionGuidsList: Array<string>,
    page: number,
    resultsPerPage: number,
  }
}

export class TemplateVariableWithType extends jspb.Message {
  getName(): string;
  setName(value: string): TemplateVariableWithType;

  getIsRequired(): boolean;
  setIsRequired(value: boolean): TemplateVariableWithType;

  getFieldType(): pando_api_shared_pb.VariableType;
  setFieldType(value: pando_api_shared_pb.VariableType): TemplateVariableWithType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariableWithType.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariableWithType): TemplateVariableWithType.AsObject;
  static serializeBinaryToWriter(message: TemplateVariableWithType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariableWithType;
  static deserializeBinaryFromReader(message: TemplateVariableWithType, reader: jspb.BinaryReader): TemplateVariableWithType;
}

export namespace TemplateVariableWithType {
  export type AsObject = {
    name: string,
    isRequired: boolean,
    fieldType: pando_api_shared_pb.VariableType,
  }
}

export class TemplateVariablesWithTypeResponse extends jspb.Message {
  getTemplateVariablesList(): Array<TemplateVariableWithType>;
  setTemplateVariablesList(value: Array<TemplateVariableWithType>): TemplateVariablesWithTypeResponse;
  clearTemplateVariablesList(): TemplateVariablesWithTypeResponse;
  addTemplateVariables(value?: TemplateVariableWithType, index?: number): TemplateVariableWithType;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): TemplateVariablesWithTypeResponse;
  hasPagination(): boolean;
  clearPagination(): TemplateVariablesWithTypeResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateVariablesWithTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateVariablesWithTypeResponse): TemplateVariablesWithTypeResponse.AsObject;
  static serializeBinaryToWriter(message: TemplateVariablesWithTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateVariablesWithTypeResponse;
  static deserializeBinaryFromReader(message: TemplateVariablesWithTypeResponse, reader: jspb.BinaryReader): TemplateVariablesWithTypeResponse;
}

export namespace TemplateVariablesWithTypeResponse {
  export type AsObject = {
    templateVariablesList: Array<TemplateVariableWithType.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export enum TemplateType { 
  HTML = 0,
  PDF = 1,
}
