import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_template_v1_beta_admin$beta_pb from '../../../../../pando/api/template/v1/beta/admin-beta_pb';
import * as pando_api_template_v1_admin_pb from '../../../../../pando/api/template/v1/admin_pb';


export class TemplateAdminBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.GetFormFieldRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.FormField) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.FormField>;

  getFormFields(
    request: pando_api_template_v1_beta_admin$beta_pb.GetFormFieldsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.GetFormFieldsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.GetFormFieldsResponse>;

  createOrUpdateFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateOrUpdateFormFieldRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.FormField) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.FormField>;

  deleteFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteFormFieldRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.GetStampRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.Stamp) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.Stamp>;

  getStamps(
    request: pando_api_template_v1_beta_admin$beta_pb.GetStampsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.GetStampsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.GetStampsResponse>;

  createOrUpdateStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateOrUpdateStampRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.Stamp) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.Stamp>;

  deleteStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteStampRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  createStampPageIndexes(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateStampPageIndexesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_beta_admin$beta_pb.CreateStampPageIndexesResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_beta_admin$beta_pb.CreateStampPageIndexesResponse>;

  deleteStampPageIndexes(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteStampPageIndexesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execPublishTemplate(
    request: pando_api_template_v1_beta_admin$beta_pb.ExecPublishTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  updateVariable(
    request: pando_api_template_v1_beta_admin$beta_pb.UpdateVariableItemBetaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_admin_pb.VariableItem) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_admin_pb.VariableItem>;

}

export class TemplateAdminBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.GetFormFieldRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.FormField>;

  getFormFields(
    request: pando_api_template_v1_beta_admin$beta_pb.GetFormFieldsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.GetFormFieldsResponse>;

  createOrUpdateFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateOrUpdateFormFieldRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.FormField>;

  deleteFormField(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteFormFieldRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.GetStampRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.Stamp>;

  getStamps(
    request: pando_api_template_v1_beta_admin$beta_pb.GetStampsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.GetStampsResponse>;

  createOrUpdateStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateOrUpdateStampRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.Stamp>;

  deleteStamp(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteStampRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  createStampPageIndexes(
    request: pando_api_template_v1_beta_admin$beta_pb.CreateStampPageIndexesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_beta_admin$beta_pb.CreateStampPageIndexesResponse>;

  deleteStampPageIndexes(
    request: pando_api_template_v1_beta_admin$beta_pb.DeleteStampPageIndexesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execPublishTemplate(
    request: pando_api_template_v1_beta_admin$beta_pb.ExecPublishTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  updateVariable(
    request: pando_api_template_v1_beta_admin$beta_pb.UpdateVariableItemBetaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_admin_pb.VariableItem>;

}

