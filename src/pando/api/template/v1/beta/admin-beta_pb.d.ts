import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_template_v1_admin_pb from '../../../../../pando/api/template/v1/admin_pb';


export class GetFormFieldsRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): GetFormFieldsRequest;

  getTemplateVersionId(): string;
  setTemplateVersionId(value: string): GetFormFieldsRequest;

  getTemplateCase(): GetFormFieldsRequest.TemplateCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFormFieldsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFormFieldsRequest): GetFormFieldsRequest.AsObject;
  static serializeBinaryToWriter(message: GetFormFieldsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFormFieldsRequest;
  static deserializeBinaryFromReader(message: GetFormFieldsRequest, reader: jspb.BinaryReader): GetFormFieldsRequest;
}

export namespace GetFormFieldsRequest {
  export type AsObject = {
    templateId: string,
    templateVersionId: string,
  }

  export enum TemplateCase { 
    TEMPLATE_NOT_SET = 0,
    TEMPLATE_ID = 1,
    TEMPLATE_VERSION_ID = 2,
  }
}

export class GetFormFieldRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): GetFormFieldRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFormFieldRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFormFieldRequest): GetFormFieldRequest.AsObject;
  static serializeBinaryToWriter(message: GetFormFieldRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFormFieldRequest;
  static deserializeBinaryFromReader(message: GetFormFieldRequest, reader: jspb.BinaryReader): GetFormFieldRequest;
}

export namespace GetFormFieldRequest {
  export type AsObject = {
    guid: string,
  }
}

export class GetTemplateFormFieldRequest extends jspb.Message {
  getTemplateVersionGuid(): string;
  setTemplateVersionGuid(value: string): GetTemplateFormFieldRequest;

  getFieldName(): string;
  setFieldName(value: string): GetTemplateFormFieldRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateFormFieldRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateFormFieldRequest): GetTemplateFormFieldRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplateFormFieldRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateFormFieldRequest;
  static deserializeBinaryFromReader(message: GetTemplateFormFieldRequest, reader: jspb.BinaryReader): GetTemplateFormFieldRequest;
}

export namespace GetTemplateFormFieldRequest {
  export type AsObject = {
    templateVersionGuid: string,
    fieldName: string,
  }
}

export class GetFormFieldsResponse extends jspb.Message {
  getFormFieldsList(): Array<FormField>;
  setFormFieldsList(value: Array<FormField>): GetFormFieldsResponse;
  clearFormFieldsList(): GetFormFieldsResponse;
  addFormFields(value?: FormField, index?: number): FormField;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFormFieldsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFormFieldsResponse): GetFormFieldsResponse.AsObject;
  static serializeBinaryToWriter(message: GetFormFieldsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFormFieldsResponse;
  static deserializeBinaryFromReader(message: GetFormFieldsResponse, reader: jspb.BinaryReader): GetFormFieldsResponse;
}

export namespace GetFormFieldsResponse {
  export type AsObject = {
    formFieldsList: Array<FormField.AsObject>,
  }
}

export class CreateOrUpdateFormFieldRequest extends jspb.Message {
  getFormField(): FormField | undefined;
  setFormField(value?: FormField): CreateOrUpdateFormFieldRequest;
  hasFormField(): boolean;
  clearFormField(): CreateOrUpdateFormFieldRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateFormFieldRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateFormFieldRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateFormFieldRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateFormFieldRequest): CreateOrUpdateFormFieldRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateFormFieldRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateFormFieldRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateFormFieldRequest, reader: jspb.BinaryReader): CreateOrUpdateFormFieldRequest;
}

export namespace CreateOrUpdateFormFieldRequest {
  export type AsObject = {
    formField?: FormField.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteFormFieldRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DeleteFormFieldRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteFormFieldRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteFormFieldRequest): DeleteFormFieldRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteFormFieldRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteFormFieldRequest;
  static deserializeBinaryFromReader(message: DeleteFormFieldRequest, reader: jspb.BinaryReader): DeleteFormFieldRequest;
}

export namespace DeleteFormFieldRequest {
  export type AsObject = {
    guid: string,
  }
}

export class FormField extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): FormField;

  getTemplateVersionGuid(): string;
  setTemplateVersionGuid(value: string): FormField;

  getVariableGuid(): string;
  setVariableGuid(value: string): FormField;

  getFieldName(): string;
  setFieldName(value: string): FormField;

  getFormFieldType(): FormFieldType;
  setFormFieldType(value: FormFieldType): FormField;

  getFontType(): FontType;
  setFontType(value: FontType): FormField;

  getFontSize(): number;
  setFontSize(value: number): FormField;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FormField.AsObject;
  static toObject(includeInstance: boolean, msg: FormField): FormField.AsObject;
  static serializeBinaryToWriter(message: FormField, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FormField;
  static deserializeBinaryFromReader(message: FormField, reader: jspb.BinaryReader): FormField;
}

export namespace FormField {
  export type AsObject = {
    guid: string,
    templateVersionGuid: string,
    variableGuid: string,
    fieldName: string,
    formFieldType: FormFieldType,
    fontType: FontType,
    fontSize: number,
  }
}

export class Stamp extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): Stamp;

  getTemplateVersionGuid(): string;
  setTemplateVersionGuid(value: string): Stamp;

  getHeight(): number;
  setHeight(value: number): Stamp;

  getWidth(): number;
  setWidth(value: number): Stamp;

  getBottom(): number;
  setBottom(value: number): Stamp;

  getTop(): number;
  setTop(value: number): Stamp;

  getLeft(): number;
  setLeft(value: number): Stamp;

  getRight(): number;
  setRight(value: number): Stamp;

  getHtml(): string;
  setHtml(value: string): Stamp;

  getPageIndexesList(): Array<number>;
  setPageIndexesList(value: Array<number>): Stamp;
  clearPageIndexesList(): Stamp;
  addPageIndexes(value: number, index?: number): Stamp;

  getBottomOrTopCase(): Stamp.BottomOrTopCase;

  getLeftOrRightCase(): Stamp.LeftOrRightCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Stamp.AsObject;
  static toObject(includeInstance: boolean, msg: Stamp): Stamp.AsObject;
  static serializeBinaryToWriter(message: Stamp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Stamp;
  static deserializeBinaryFromReader(message: Stamp, reader: jspb.BinaryReader): Stamp;
}

export namespace Stamp {
  export type AsObject = {
    guid: string,
    templateVersionGuid: string,
    height: number,
    width: number,
    bottom: number,
    top: number,
    left: number,
    right: number,
    html: string,
    pageIndexesList: Array<number>,
  }

  export enum BottomOrTopCase { 
    BOTTOM_OR_TOP_NOT_SET = 0,
    BOTTOM = 6,
    TOP = 7,
  }

  export enum LeftOrRightCase { 
    LEFT_OR_RIGHT_NOT_SET = 0,
    LEFT = 8,
    RIGHT = 9,
  }
}

export class GetStampRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): GetStampRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetStampRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetStampRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStampRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetStampRequest): GetStampRequest.AsObject;
  static serializeBinaryToWriter(message: GetStampRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStampRequest;
  static deserializeBinaryFromReader(message: GetStampRequest, reader: jspb.BinaryReader): GetStampRequest;
}

export namespace GetStampRequest {
  export type AsObject = {
    guid: string,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class GetStampsRequest extends jspb.Message {
  getTemplateVersionGuid(): string;
  setTemplateVersionGuid(value: string): GetStampsRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetStampsRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetStampsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStampsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetStampsRequest): GetStampsRequest.AsObject;
  static serializeBinaryToWriter(message: GetStampsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStampsRequest;
  static deserializeBinaryFromReader(message: GetStampsRequest, reader: jspb.BinaryReader): GetStampsRequest;
}

export namespace GetStampsRequest {
  export type AsObject = {
    templateVersionGuid: string,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class GetStampsResponse extends jspb.Message {
  getStampsList(): Array<Stamp>;
  setStampsList(value: Array<Stamp>): GetStampsResponse;
  clearStampsList(): GetStampsResponse;
  addStamps(value?: Stamp, index?: number): Stamp;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStampsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetStampsResponse): GetStampsResponse.AsObject;
  static serializeBinaryToWriter(message: GetStampsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStampsResponse;
  static deserializeBinaryFromReader(message: GetStampsResponse, reader: jspb.BinaryReader): GetStampsResponse;
}

export namespace GetStampsResponse {
  export type AsObject = {
    stampsList: Array<Stamp.AsObject>,
  }
}

export class CreateOrUpdateStampRequest extends jspb.Message {
  getStamp(): Stamp | undefined;
  setStamp(value?: Stamp): CreateOrUpdateStampRequest;
  hasStamp(): boolean;
  clearStamp(): CreateOrUpdateStampRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateStampRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateStampRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateStampRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateStampRequest): CreateOrUpdateStampRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateStampRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateStampRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateStampRequest, reader: jspb.BinaryReader): CreateOrUpdateStampRequest;
}

export namespace CreateOrUpdateStampRequest {
  export type AsObject = {
    stamp?: Stamp.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteStampRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DeleteStampRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteStampRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteStampRequest): DeleteStampRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteStampRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteStampRequest;
  static deserializeBinaryFromReader(message: DeleteStampRequest, reader: jspb.BinaryReader): DeleteStampRequest;
}

export namespace DeleteStampRequest {
  export type AsObject = {
    guid: string,
  }
}

export class CreateStampPageIndexesRequest extends jspb.Message {
  getStampGuid(): string;
  setStampGuid(value: string): CreateStampPageIndexesRequest;

  getPageIndexesList(): Array<number>;
  setPageIndexesList(value: Array<number>): CreateStampPageIndexesRequest;
  clearPageIndexesList(): CreateStampPageIndexesRequest;
  addPageIndexes(value: number, index?: number): CreateStampPageIndexesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateStampPageIndexesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateStampPageIndexesRequest): CreateStampPageIndexesRequest.AsObject;
  static serializeBinaryToWriter(message: CreateStampPageIndexesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateStampPageIndexesRequest;
  static deserializeBinaryFromReader(message: CreateStampPageIndexesRequest, reader: jspb.BinaryReader): CreateStampPageIndexesRequest;
}

export namespace CreateStampPageIndexesRequest {
  export type AsObject = {
    stampGuid: string,
    pageIndexesList: Array<number>,
  }
}

export class CreateStampPageIndexesResponse extends jspb.Message {
  getStampGuid(): string;
  setStampGuid(value: string): CreateStampPageIndexesResponse;

  getPageIndexesList(): Array<number>;
  setPageIndexesList(value: Array<number>): CreateStampPageIndexesResponse;
  clearPageIndexesList(): CreateStampPageIndexesResponse;
  addPageIndexes(value: number, index?: number): CreateStampPageIndexesResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateStampPageIndexesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateStampPageIndexesResponse): CreateStampPageIndexesResponse.AsObject;
  static serializeBinaryToWriter(message: CreateStampPageIndexesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateStampPageIndexesResponse;
  static deserializeBinaryFromReader(message: CreateStampPageIndexesResponse, reader: jspb.BinaryReader): CreateStampPageIndexesResponse;
}

export namespace CreateStampPageIndexesResponse {
  export type AsObject = {
    stampGuid: string,
    pageIndexesList: Array<number>,
  }
}

export class DeleteStampPageIndexesRequest extends jspb.Message {
  getStampGuid(): string;
  setStampGuid(value: string): DeleteStampPageIndexesRequest;

  getPageIndexesList(): Array<number>;
  setPageIndexesList(value: Array<number>): DeleteStampPageIndexesRequest;
  clearPageIndexesList(): DeleteStampPageIndexesRequest;
  addPageIndexes(value: number, index?: number): DeleteStampPageIndexesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteStampPageIndexesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteStampPageIndexesRequest): DeleteStampPageIndexesRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteStampPageIndexesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteStampPageIndexesRequest;
  static deserializeBinaryFromReader(message: DeleteStampPageIndexesRequest, reader: jspb.BinaryReader): DeleteStampPageIndexesRequest;
}

export namespace DeleteStampPageIndexesRequest {
  export type AsObject = {
    stampGuid: string,
    pageIndexesList: Array<number>,
  }
}

export class ExecPublishTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): ExecPublishTemplateRequest;

  getTemplateVersionId(): string;
  setTemplateVersionId(value: string): ExecPublishTemplateRequest;

  getTemplateCase(): ExecPublishTemplateRequest.TemplateCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecPublishTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecPublishTemplateRequest): ExecPublishTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: ExecPublishTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecPublishTemplateRequest;
  static deserializeBinaryFromReader(message: ExecPublishTemplateRequest, reader: jspb.BinaryReader): ExecPublishTemplateRequest;
}

export namespace ExecPublishTemplateRequest {
  export type AsObject = {
    templateId: string,
    templateVersionId: string,
  }

  export enum TemplateCase { 
    TEMPLATE_NOT_SET = 0,
    TEMPLATE_ID = 1,
    TEMPLATE_VERSION_ID = 2,
  }
}

export class UpdateVariableItemBetaRequest extends jspb.Message {
  getVariable(): pando_api_template_v1_admin_pb.VariableItem | undefined;
  setVariable(value?: pando_api_template_v1_admin_pb.VariableItem): UpdateVariableItemBetaRequest;
  hasVariable(): boolean;
  clearVariable(): UpdateVariableItemBetaRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateVariableItemBetaRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateVariableItemBetaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateVariableItemBetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateVariableItemBetaRequest): UpdateVariableItemBetaRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateVariableItemBetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateVariableItemBetaRequest;
  static deserializeBinaryFromReader(message: UpdateVariableItemBetaRequest, reader: jspb.BinaryReader): UpdateVariableItemBetaRequest;
}

export namespace UpdateVariableItemBetaRequest {
  export type AsObject = {
    variable?: pando_api_template_v1_admin_pb.VariableItem.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export enum FontType { 
  UNSPECIFIED_FONT_TYPE = 0,
  TIMES_ROMAN = 1,
  TIMES_BOLD = 2,
  TIMES_ITALIC = 3,
  TIMES_BOLD_ITALIC = 4,
  HELVETICA = 5,
  HELVETICA_BOLD = 6,
  HELVETICA_OBLIQUE = 7,
  HELVETICA_BOLD_OBLIQUE = 8,
  COURIER = 9,
  COURIER_BOLD = 10,
  COURIER_OBLIQUE = 11,
  COURIER_BOLD_OBLIQUE = 12,
  SYMBOL = 13,
  ZAPF_DINGBATS = 14,
}
export enum FormFieldType { 
  UNSPECIFIED_FORM_FIELD_TYPE = 0,
  CHECKBOX = 1,
  COMBO_BOX = 2,
  TEXT_FIELD = 3,
}
