import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class RenderTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): RenderTemplateRequest;

  getTemplateVersionId(): string;
  setTemplateVersionId(value: string): RenderTemplateRequest;

  getVaultRecordId(): string;
  setVaultRecordId(value: string): RenderTemplateRequest;

  getDotNotationDataMap(): jspb.Map<string, string>;
  clearDotNotationDataMap(): RenderTemplateRequest;

  getStoreInVault(): boolean;
  setStoreInVault(value: boolean): RenderTemplateRequest;

  getTemplateCase(): RenderTemplateRequest.TemplateCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RenderTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RenderTemplateRequest): RenderTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: RenderTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RenderTemplateRequest;
  static deserializeBinaryFromReader(message: RenderTemplateRequest, reader: jspb.BinaryReader): RenderTemplateRequest;
}

export namespace RenderTemplateRequest {
  export type AsObject = {
    templateId: string,
    templateVersionId: string,
    vaultRecordId: string,
    dotNotationDataMap: Array<[string, string]>,
    storeInVault: boolean,
  }

  export enum TemplateCase { 
    TEMPLATE_NOT_SET = 0,
    TEMPLATE_ID = 1,
    TEMPLATE_VERSION_ID = 2,
  }
}

export class RenderedHtml extends jspb.Message {
  getHtml(): string;
  setHtml(value: string): RenderedHtml;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RenderedHtml.AsObject;
  static toObject(includeInstance: boolean, msg: RenderedHtml): RenderedHtml.AsObject;
  static serializeBinaryToWriter(message: RenderedHtml, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RenderedHtml;
  static deserializeBinaryFromReader(message: RenderedHtml, reader: jspb.BinaryReader): RenderedHtml;
}

export namespace RenderedHtml {
  export type AsObject = {
    html: string,
  }
}

export class RenderedPdf extends jspb.Message {
  getBytes(): Uint8Array | string;
  getBytes_asU8(): Uint8Array;
  getBytes_asB64(): string;
  setBytes(value: Uint8Array | string): RenderedPdf;

  getVaultId(): string;
  setVaultId(value: string): RenderedPdf;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RenderedPdf.AsObject;
  static toObject(includeInstance: boolean, msg: RenderedPdf): RenderedPdf.AsObject;
  static serializeBinaryToWriter(message: RenderedPdf, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RenderedPdf;
  static deserializeBinaryFromReader(message: RenderedPdf, reader: jspb.BinaryReader): RenderedPdf;
}

export namespace RenderedPdf {
  export type AsObject = {
    bytes: Uint8Array | string,
    vaultId: string,
  }
}

