import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_template_v1_template_pb from '../../../../pando/api/template/v1/template_pb';


export class TemplateServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execValidateData(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execRenderHtml(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_template_pb.RenderedHtml) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_template_pb.RenderedHtml>;

  execRenderPdf(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_template_v1_template_pb.RenderedPdf) => void
  ): grpcWeb.ClientReadableStream<pando_api_template_v1_template_pb.RenderedPdf>;

}

export class TemplateServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execValidateData(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execRenderHtml(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_template_pb.RenderedHtml>;

  execRenderPdf(
    request: pando_api_template_v1_template_pb.RenderTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_template_v1_template_pb.RenderedPdf>;

}

