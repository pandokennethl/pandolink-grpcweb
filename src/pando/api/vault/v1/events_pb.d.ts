import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class DocumentAccessed extends jspb.Message {
  getDocumentGuid(): string;
  setDocumentGuid(value: string): DocumentAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): DocumentAccessed;
  hasUserData(): boolean;
  clearUserData(): DocumentAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentAccessed): DocumentAccessed.AsObject;
  static serializeBinaryToWriter(message: DocumentAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentAccessed;
  static deserializeBinaryFromReader(message: DocumentAccessed, reader: jspb.BinaryReader): DocumentAccessed;
}

export namespace DocumentAccessed {
  export type AsObject = {
    documentGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class HashChecked extends jspb.Message {
  getIsValid(): boolean;
  setIsValid(value: boolean): HashChecked;

  getFailedId(): string;
  setFailedId(value: string): HashChecked;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HashChecked.AsObject;
  static toObject(includeInstance: boolean, msg: HashChecked): HashChecked.AsObject;
  static serializeBinaryToWriter(message: HashChecked, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HashChecked;
  static deserializeBinaryFromReader(message: HashChecked, reader: jspb.BinaryReader): HashChecked;
}

export namespace HashChecked {
  export type AsObject = {
    isValid: boolean,
    failedId: string,
  }
}

export enum Events { 
  DOCUMENT_RETRIEVED = 0,
  DOCUMENT_UPLOADED = 1,
  CHECK_HASH_CHAIN = 2,
  CHECK_DOCUMENT_HASHES = 3,
  HASH_CHAIN_CHECKED = 4,
  DOCUMENT_HASHES_CHECKED = 5,
}
