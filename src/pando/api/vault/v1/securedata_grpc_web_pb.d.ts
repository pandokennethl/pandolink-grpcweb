import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_vault_v1_securedata_pb from '../../../../pando/api/vault/v1/securedata_pb';


export class SecureDataServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  uploadSecureData(
    request: pando_api_vault_v1_securedata_pb.UploadSecureDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_vault_v1_securedata_pb.UploadSecureDataResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_vault_v1_securedata_pb.UploadSecureDataResponse>;

  appendSecureData(
    request: pando_api_vault_v1_securedata_pb.AppendSecureDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_vault_v1_securedata_pb.AppendSecureDataResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_vault_v1_securedata_pb.AppendSecureDataResponse>;

  getSecureData(
    request: pando_api_vault_v1_securedata_pb.GetSecureDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_vault_v1_securedata_pb.SecureDataModel) => void
  ): grpcWeb.ClientReadableStream<pando_api_vault_v1_securedata_pb.SecureDataModel>;

  deleteSecureData(
    request: pando_api_vault_v1_securedata_pb.DeleteSecureDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execAddRecordOwner(
    request: pando_api_vault_v1_securedata_pb.ExecAddRecordOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execRemoveRecordOwnership(
    request: pando_api_vault_v1_securedata_pb.ExecRemoveRecordOwnershipRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class SecureDataServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  uploadSecureData(
    request: pando_api_vault_v1_securedata_pb.UploadSecureDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_vault_v1_securedata_pb.UploadSecureDataResponse>;

  appendSecureData(
    request: pando_api_vault_v1_securedata_pb.AppendSecureDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_vault_v1_securedata_pb.AppendSecureDataResponse>;

  getSecureData(
    request: pando_api_vault_v1_securedata_pb.GetSecureDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_vault_v1_securedata_pb.SecureDataModel>;

  deleteSecureData(
    request: pando_api_vault_v1_securedata_pb.DeleteSecureDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execAddRecordOwner(
    request: pando_api_vault_v1_securedata_pb.ExecAddRecordOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execRemoveRecordOwnership(
    request: pando_api_vault_v1_securedata_pb.ExecRemoveRecordOwnershipRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

