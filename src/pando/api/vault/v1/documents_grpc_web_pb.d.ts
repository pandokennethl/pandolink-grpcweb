import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_vault_v1_documents_pb from '../../../../pando/api/vault/v1/documents_pb';


export class DocumentServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getDocument(
    request: pando_api_vault_v1_documents_pb.GetDocumentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_vault_v1_documents_pb.GetDocumentResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_vault_v1_documents_pb.GetDocumentResponse>;

  uploadDocument(
    request: pando_api_vault_v1_documents_pb.Document,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_vault_v1_documents_pb.Document) => void
  ): grpcWeb.ClientReadableStream<pando_api_vault_v1_documents_pb.Document>;

  execAddDocumentOwner(
    request: pando_api_vault_v1_documents_pb.ExecAddDocumentOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execRemoveDocumentOwnership(
    request: pando_api_vault_v1_documents_pb.ExecRemoveDocumentOwnershipRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class DocumentServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getDocument(
    request: pando_api_vault_v1_documents_pb.GetDocumentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_vault_v1_documents_pb.GetDocumentResponse>;

  uploadDocument(
    request: pando_api_vault_v1_documents_pb.Document,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_vault_v1_documents_pb.Document>;

  execAddDocumentOwner(
    request: pando_api_vault_v1_documents_pb.ExecAddDocumentOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execRemoveDocumentOwnership(
    request: pando_api_vault_v1_documents_pb.ExecRemoveDocumentOwnershipRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

