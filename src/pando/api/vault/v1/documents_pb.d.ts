import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetDocumentRequest extends jspb.Message {
  getDocumentId(): string;
  setDocumentId(value: string): GetDocumentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDocumentRequest): GetDocumentRequest.AsObject;
  static serializeBinaryToWriter(message: GetDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDocumentRequest;
  static deserializeBinaryFromReader(message: GetDocumentRequest, reader: jspb.BinaryReader): GetDocumentRequest;
}

export namespace GetDocumentRequest {
  export type AsObject = {
    documentId: string,
  }
}

export class GetDocumentResponse extends jspb.Message {
  getDocument(): Document | undefined;
  setDocument(value?: Document): GetDocumentResponse;
  hasDocument(): boolean;
  clearDocument(): GetDocumentResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDocumentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDocumentResponse): GetDocumentResponse.AsObject;
  static serializeBinaryToWriter(message: GetDocumentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDocumentResponse;
  static deserializeBinaryFromReader(message: GetDocumentResponse, reader: jspb.BinaryReader): GetDocumentResponse;
}

export namespace GetDocumentResponse {
  export type AsObject = {
    document?: Document.AsObject,
  }
}

export class Document extends jspb.Message {
  getDocumentId(): string;
  setDocumentId(value: string): Document;

  getFile(): Uint8Array | string;
  getFile_asU8(): Uint8Array;
  getFile_asB64(): string;
  setFile(value: Uint8Array | string): Document;

  getMediaType(): string;
  setMediaType(value: string): Document;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Document;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Document.AsObject;
  static toObject(includeInstance: boolean, msg: Document): Document.AsObject;
  static serializeBinaryToWriter(message: Document, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Document;
  static deserializeBinaryFromReader(message: Document, reader: jspb.BinaryReader): Document;
}

export namespace Document {
  export type AsObject = {
    documentId: string,
    file: Uint8Array | string,
    mediaType: string,
    organizationCode: string,
  }
}

export class ExecAddDocumentOwnerRequest extends jspb.Message {
  getDocumentGuid(): string;
  setDocumentGuid(value: string): ExecAddDocumentOwnerRequest;

  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): ExecAddDocumentOwnerRequest;
  clearUserIdsList(): ExecAddDocumentOwnerRequest;
  addUserIds(value: string, index?: number): ExecAddDocumentOwnerRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecAddDocumentOwnerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecAddDocumentOwnerRequest): ExecAddDocumentOwnerRequest.AsObject;
  static serializeBinaryToWriter(message: ExecAddDocumentOwnerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecAddDocumentOwnerRequest;
  static deserializeBinaryFromReader(message: ExecAddDocumentOwnerRequest, reader: jspb.BinaryReader): ExecAddDocumentOwnerRequest;
}

export namespace ExecAddDocumentOwnerRequest {
  export type AsObject = {
    documentGuid: string,
    userIdsList: Array<string>,
  }
}

export class ExecRemoveDocumentOwnershipRequest extends jspb.Message {
  getDocumentGuid(): string;
  setDocumentGuid(value: string): ExecRemoveDocumentOwnershipRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecRemoveDocumentOwnershipRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecRemoveDocumentOwnershipRequest): ExecRemoveDocumentOwnershipRequest.AsObject;
  static serializeBinaryToWriter(message: ExecRemoveDocumentOwnershipRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecRemoveDocumentOwnershipRequest;
  static deserializeBinaryFromReader(message: ExecRemoveDocumentOwnershipRequest, reader: jspb.BinaryReader): ExecRemoveDocumentOwnershipRequest;
}

export namespace ExecRemoveDocumentOwnershipRequest {
  export type AsObject = {
    documentGuid: string,
  }
}

