import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class SecureDataModel extends jspb.Message {
  getRecordId(): string;
  setRecordId(value: string): SecureDataModel;

  getDataList(): Array<Data>;
  setDataList(value: Array<Data>): SecureDataModel;
  clearDataList(): SecureDataModel;
  addData(value?: Data, index?: number): Data;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): SecureDataModel;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SecureDataModel.AsObject;
  static toObject(includeInstance: boolean, msg: SecureDataModel): SecureDataModel.AsObject;
  static serializeBinaryToWriter(message: SecureDataModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SecureDataModel;
  static deserializeBinaryFromReader(message: SecureDataModel, reader: jspb.BinaryReader): SecureDataModel;
}

export namespace SecureDataModel {
  export type AsObject = {
    recordId: string,
    dataList: Array<Data.AsObject>,
    organizationCode: string,
  }
}

export class Data extends jspb.Message {
  getKeyId(): string;
  setKeyId(value: string): Data;

  getValue(): string;
  setValue(value: string): Data;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Data.AsObject;
  static toObject(includeInstance: boolean, msg: Data): Data.AsObject;
  static serializeBinaryToWriter(message: Data, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Data;
  static deserializeBinaryFromReader(message: Data, reader: jspb.BinaryReader): Data;
}

export namespace Data {
  export type AsObject = {
    keyId: string,
    value: string,
  }
}

export class UploadSecureDataRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): UploadSecureDataRequest;

  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): UploadSecureDataRequest;
  clearUserIdsList(): UploadSecureDataRequest;
  addUserIds(value: string, index?: number): UploadSecureDataRequest;

  getDataRecord(): pando_api_shared_pb.DataRecord | undefined;
  setDataRecord(value?: pando_api_shared_pb.DataRecord): UploadSecureDataRequest;
  hasDataRecord(): boolean;
  clearDataRecord(): UploadSecureDataRequest;

  getDataKeyValues(): pando_api_shared_pb.KeyValueMap | undefined;
  setDataKeyValues(value?: pando_api_shared_pb.KeyValueMap): UploadSecureDataRequest;
  hasDataKeyValues(): boolean;
  clearDataKeyValues(): UploadSecureDataRequest;

  getDataCase(): UploadSecureDataRequest.DataCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadSecureDataRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UploadSecureDataRequest): UploadSecureDataRequest.AsObject;
  static serializeBinaryToWriter(message: UploadSecureDataRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadSecureDataRequest;
  static deserializeBinaryFromReader(message: UploadSecureDataRequest, reader: jspb.BinaryReader): UploadSecureDataRequest;
}

export namespace UploadSecureDataRequest {
  export type AsObject = {
    organizationCode: string,
    userIdsList: Array<string>,
    dataRecord?: pando_api_shared_pb.DataRecord.AsObject,
    dataKeyValues?: pando_api_shared_pb.KeyValueMap.AsObject,
  }

  export enum DataCase { 
    DATA_NOT_SET = 0,
    DATA_RECORD = 7,
    DATA_KEY_VALUES = 8,
  }
}

export class GetSecureDataRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): GetSecureDataRequest;

  getKeyIdsList(): Array<string>;
  setKeyIdsList(value: Array<string>): GetSecureDataRequest;
  clearKeyIdsList(): GetSecureDataRequest;
  addKeyIds(value: string, index?: number): GetSecureDataRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSecureDataRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSecureDataRequest): GetSecureDataRequest.AsObject;
  static serializeBinaryToWriter(message: GetSecureDataRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSecureDataRequest;
  static deserializeBinaryFromReader(message: GetSecureDataRequest, reader: jspb.BinaryReader): GetSecureDataRequest;
}

export namespace GetSecureDataRequest {
  export type AsObject = {
    guid: string,
    keyIdsList: Array<string>,
  }
}

export class DeleteSecureDataRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): DeleteSecureDataRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteSecureDataRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteSecureDataRequest): DeleteSecureDataRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteSecureDataRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteSecureDataRequest;
  static deserializeBinaryFromReader(message: DeleteSecureDataRequest, reader: jspb.BinaryReader): DeleteSecureDataRequest;
}

export namespace DeleteSecureDataRequest {
  export type AsObject = {
    guid: string,
  }
}

export class ExecAddRecordOwnerRequest extends jspb.Message {
  getRecordGuid(): string;
  setRecordGuid(value: string): ExecAddRecordOwnerRequest;

  getUserIdsList(): Array<string>;
  setUserIdsList(value: Array<string>): ExecAddRecordOwnerRequest;
  clearUserIdsList(): ExecAddRecordOwnerRequest;
  addUserIds(value: string, index?: number): ExecAddRecordOwnerRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecAddRecordOwnerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecAddRecordOwnerRequest): ExecAddRecordOwnerRequest.AsObject;
  static serializeBinaryToWriter(message: ExecAddRecordOwnerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecAddRecordOwnerRequest;
  static deserializeBinaryFromReader(message: ExecAddRecordOwnerRequest, reader: jspb.BinaryReader): ExecAddRecordOwnerRequest;
}

export namespace ExecAddRecordOwnerRequest {
  export type AsObject = {
    recordGuid: string,
    userIdsList: Array<string>,
  }
}

export class ExecRemoveRecordOwnershipRequest extends jspb.Message {
  getRecordGuid(): string;
  setRecordGuid(value: string): ExecRemoveRecordOwnershipRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecRemoveRecordOwnershipRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecRemoveRecordOwnershipRequest): ExecRemoveRecordOwnershipRequest.AsObject;
  static serializeBinaryToWriter(message: ExecRemoveRecordOwnershipRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecRemoveRecordOwnershipRequest;
  static deserializeBinaryFromReader(message: ExecRemoveRecordOwnershipRequest, reader: jspb.BinaryReader): ExecRemoveRecordOwnershipRequest;
}

export namespace ExecRemoveRecordOwnershipRequest {
  export type AsObject = {
    recordGuid: string,
  }
}

export class UploadSecureDataResponse extends jspb.Message {
  getRecordId(): string;
  setRecordId(value: string): UploadSecureDataResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadSecureDataResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UploadSecureDataResponse): UploadSecureDataResponse.AsObject;
  static serializeBinaryToWriter(message: UploadSecureDataResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadSecureDataResponse;
  static deserializeBinaryFromReader(message: UploadSecureDataResponse, reader: jspb.BinaryReader): UploadSecureDataResponse;
}

export namespace UploadSecureDataResponse {
  export type AsObject = {
    recordId: string,
  }
}

export class AppendSecureDataRequest extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): AppendSecureDataRequest;

  getKeyValuesMap(): jspb.Map<string, string>;
  clearKeyValuesMap(): AppendSecureDataRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AppendSecureDataRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AppendSecureDataRequest): AppendSecureDataRequest.AsObject;
  static serializeBinaryToWriter(message: AppendSecureDataRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AppendSecureDataRequest;
  static deserializeBinaryFromReader(message: AppendSecureDataRequest, reader: jspb.BinaryReader): AppendSecureDataRequest;
}

export namespace AppendSecureDataRequest {
  export type AsObject = {
    guid: string,
    keyValuesMap: Array<[string, string]>,
  }
}

export class AppendSecureDataResponse extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): AppendSecureDataResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AppendSecureDataResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AppendSecureDataResponse): AppendSecureDataResponse.AsObject;
  static serializeBinaryToWriter(message: AppendSecureDataResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AppendSecureDataResponse;
  static deserializeBinaryFromReader(message: AppendSecureDataResponse, reader: jspb.BinaryReader): AppendSecureDataResponse;
}

export namespace AppendSecureDataResponse {
  export type AsObject = {
    guid: string,
  }
}

