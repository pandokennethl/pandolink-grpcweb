import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class ValidateLoginCodeRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): ValidateLoginCodeRequest;

  getLoginCode(): string;
  setLoginCode(value: string): ValidateLoginCodeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateLoginCodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateLoginCodeRequest): ValidateLoginCodeRequest.AsObject;
  static serializeBinaryToWriter(message: ValidateLoginCodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateLoginCodeRequest;
  static deserializeBinaryFromReader(message: ValidateLoginCodeRequest, reader: jspb.BinaryReader): ValidateLoginCodeRequest;
}

export namespace ValidateLoginCodeRequest {
  export type AsObject = {
    userId: string,
    loginCode: string,
  }
}

export class ValidateLoginCodeResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): ValidateLoginCodeResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateLoginCodeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateLoginCodeResponse): ValidateLoginCodeResponse.AsObject;
  static serializeBinaryToWriter(message: ValidateLoginCodeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateLoginCodeResponse;
  static deserializeBinaryFromReader(message: ValidateLoginCodeResponse, reader: jspb.BinaryReader): ValidateLoginCodeResponse;
}

export namespace ValidateLoginCodeResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class SendEmailLoginLinkRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): SendEmailLoginLinkRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendEmailLoginLinkRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendEmailLoginLinkRequest): SendEmailLoginLinkRequest.AsObject;
  static serializeBinaryToWriter(message: SendEmailLoginLinkRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendEmailLoginLinkRequest;
  static deserializeBinaryFromReader(message: SendEmailLoginLinkRequest, reader: jspb.BinaryReader): SendEmailLoginLinkRequest;
}

export namespace SendEmailLoginLinkRequest {
  export type AsObject = {
    userId: string,
  }
}

export class Claim extends jspb.Message {
  getType(): string;
  setType(value: string): Claim;

  getValue(): string;
  setValue(value: string): Claim;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Claim.AsObject;
  static toObject(includeInstance: boolean, msg: Claim): Claim.AsObject;
  static serializeBinaryToWriter(message: Claim, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Claim;
  static deserializeBinaryFromReader(message: Claim, reader: jspb.BinaryReader): Claim;
}

export namespace Claim {
  export type AsObject = {
    type: string,
    value: string,
  }
}

export class ClaimRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): ClaimRequest;

  getClaim(): Claim | undefined;
  setClaim(value?: Claim): ClaimRequest;
  hasClaim(): boolean;
  clearClaim(): ClaimRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ClaimRequest): ClaimRequest.AsObject;
  static serializeBinaryToWriter(message: ClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClaimRequest;
  static deserializeBinaryFromReader(message: ClaimRequest, reader: jspb.BinaryReader): ClaimRequest;
}

export namespace ClaimRequest {
  export type AsObject = {
    userId: string,
    claim?: Claim.AsObject,
  }
}

export class UserInfo extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): UserInfo;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): UserInfo;

  getGivenName(): string;
  setGivenName(value: string): UserInfo;

  getFamilyName(): string;
  setFamilyName(value: string): UserInfo;

  getNickname(): string;
  setNickname(value: string): UserInfo;

  getUserId(): string;
  setUserId(value: string): UserInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserInfo.AsObject;
  static toObject(includeInstance: boolean, msg: UserInfo): UserInfo.AsObject;
  static serializeBinaryToWriter(message: UserInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserInfo;
  static deserializeBinaryFromReader(message: UserInfo, reader: jspb.BinaryReader): UserInfo;
}

export namespace UserInfo {
  export type AsObject = {
    email: string,
    phoneNumber: string,
    givenName: string,
    familyName: string,
    nickname: string,
    userId: string,
  }
}

export class UserInfoRequest extends jspb.Message {
  getUserInfo(): UserInfo | undefined;
  setUserInfo(value?: UserInfo): UserInfoRequest;
  hasUserInfo(): boolean;
  clearUserInfo(): UserInfoRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UserInfoRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UserInfoRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserInfoRequest): UserInfoRequest.AsObject;
  static serializeBinaryToWriter(message: UserInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserInfoRequest;
  static deserializeBinaryFromReader(message: UserInfoRequest, reader: jspb.BinaryReader): UserInfoRequest;
}

export namespace UserInfoRequest {
  export type AsObject = {
    userInfo?: UserInfo.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class CreateUserRequest extends jspb.Message {
  getUser(): UserInfo | undefined;
  setUser(value?: UserInfo): CreateUserRequest;
  hasUser(): boolean;
  clearUser(): CreateUserRequest;

  getClaimsList(): Array<Claim>;
  setClaimsList(value: Array<Claim>): CreateUserRequest;
  clearClaimsList(): CreateUserRequest;
  addClaims(value?: Claim, index?: number): Claim;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateUserRequest): CreateUserRequest.AsObject;
  static serializeBinaryToWriter(message: CreateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateUserRequest;
  static deserializeBinaryFromReader(message: CreateUserRequest, reader: jspb.BinaryReader): CreateUserRequest;
}

export namespace CreateUserRequest {
  export type AsObject = {
    user?: UserInfo.AsObject,
    claimsList: Array<Claim.AsObject>,
  }
}

