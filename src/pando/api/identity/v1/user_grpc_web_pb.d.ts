import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_identity_v1_user_pb from '../../../../pando/api/identity/v1/user_pb';


export class UserServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createUserClaim(
    request: pando_api_identity_v1_user_pb.ClaimRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_identity_v1_user_pb.Claim) => void
  ): grpcWeb.ClientReadableStream<pando_api_identity_v1_user_pb.Claim>;

  deleteUserClaim(
    request: pando_api_identity_v1_user_pb.ClaimRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  updateUserInfo(
    request: pando_api_identity_v1_user_pb.UserInfoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_identity_v1_user_pb.UserInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_identity_v1_user_pb.UserInfo>;

  createUser(
    request: pando_api_identity_v1_user_pb.CreateUserRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_identity_v1_user_pb.CreateUserRequest) => void
  ): grpcWeb.ClientReadableStream<pando_api_identity_v1_user_pb.CreateUserRequest>;

  execSendEmailLoginLink(
    request: pando_api_identity_v1_user_pb.SendEmailLoginLinkRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execValidateLoginCode(
    request: pando_api_identity_v1_user_pb.ValidateLoginCodeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_identity_v1_user_pb.ValidateLoginCodeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_identity_v1_user_pb.ValidateLoginCodeResponse>;

}

export class UserServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createUserClaim(
    request: pando_api_identity_v1_user_pb.ClaimRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_identity_v1_user_pb.Claim>;

  deleteUserClaim(
    request: pando_api_identity_v1_user_pb.ClaimRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  updateUserInfo(
    request: pando_api_identity_v1_user_pb.UserInfoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_identity_v1_user_pb.UserInfo>;

  createUser(
    request: pando_api_identity_v1_user_pb.CreateUserRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_identity_v1_user_pb.CreateUserRequest>;

  execSendEmailLoginLink(
    request: pando_api_identity_v1_user_pb.SendEmailLoginLinkRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execValidateLoginCode(
    request: pando_api_identity_v1_user_pb.ValidateLoginCodeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_identity_v1_user_pb.ValidateLoginCodeResponse>;

}

