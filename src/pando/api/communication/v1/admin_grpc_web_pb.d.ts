import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_communication_v1_admin_pb from '../../../../pando/api/communication/v1/admin_pb';


export class CommunicationAdminServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.OrganizationContactInfo,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.OrganizationContactInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  createOrUpdateDefaultOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.CreateOrUpdateDefaultOrganizationContactInfoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo>;

  createOrUpdateTemplate(
    request: pando_api_communication_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.Template>;

  getOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.OrganizationContactInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  getOrganizationContactInfoList(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.OrganizationContactInfoList) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.OrganizationContactInfoList>;

  getDefaultOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetDefaultOrganizationContactInfoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo>;

  getTemplate(
    request: pando_api_communication_v1_admin_pb.GetTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.Template) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.Template>;

  getTemplates(
    request: pando_api_communication_v1_admin_pb.GetTemplatesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.TemplateList) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.TemplateList>;

  deleteOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_admin_pb.OrganizationContactInfo) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  deleteTemplate(
    request: pando_api_communication_v1_admin_pb.DeleteTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class CommunicationAdminServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.OrganizationContactInfo,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  createOrUpdateDefaultOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.CreateOrUpdateDefaultOrganizationContactInfoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo>;

  createOrUpdateTemplate(
    request: pando_api_communication_v1_admin_pb.CreateOrUpdateTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.Template>;

  getOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  getOrganizationContactInfoList(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.OrganizationContactInfoList>;

  getDefaultOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetDefaultOrganizationContactInfoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.DefaultOrganizationContactInfo>;

  getTemplate(
    request: pando_api_communication_v1_admin_pb.GetTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.Template>;

  getTemplates(
    request: pando_api_communication_v1_admin_pb.GetTemplatesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.TemplateList>;

  deleteOrganizationContactInfo(
    request: pando_api_communication_v1_admin_pb.GetOrganizationContactInfoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_admin_pb.OrganizationContactInfo>;

  deleteTemplate(
    request: pando_api_communication_v1_admin_pb.DeleteTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

