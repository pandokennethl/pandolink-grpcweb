import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class MessageCreated extends jspb.Message {
  getMessageId(): string;
  setMessageId(value: string): MessageCreated;

  getTemplateId(): string;
  setTemplateId(value: string): MessageCreated;

  getContactInfoListList(): Array<pando_api_shared_pb.ContactInfo>;
  setContactInfoListList(value: Array<pando_api_shared_pb.ContactInfo>): MessageCreated;
  clearContactInfoListList(): MessageCreated;
  addContactInfoList(value?: pando_api_shared_pb.ContactInfo, index?: number): pando_api_shared_pb.ContactInfo;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): MessageCreated;
  hasUserData(): boolean;
  clearUserData(): MessageCreated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageCreated.AsObject;
  static toObject(includeInstance: boolean, msg: MessageCreated): MessageCreated.AsObject;
  static serializeBinaryToWriter(message: MessageCreated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageCreated;
  static deserializeBinaryFromReader(message: MessageCreated, reader: jspb.BinaryReader): MessageCreated;
}

export namespace MessageCreated {
  export type AsObject = {
    messageId: string,
    templateId: string,
    contactInfoListList: Array<pando_api_shared_pb.ContactInfo.AsObject>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class MessageSent extends jspb.Message {
  getMessageId(): string;
  setMessageId(value: string): MessageSent;

  getTemplateId(): string;
  setTemplateId(value: string): MessageSent;

  getContactInfoListList(): Array<pando_api_shared_pb.ContactInfo>;
  setContactInfoListList(value: Array<pando_api_shared_pb.ContactInfo>): MessageSent;
  clearContactInfoListList(): MessageSent;
  addContactInfoList(value?: pando_api_shared_pb.ContactInfo, index?: number): pando_api_shared_pb.ContactInfo;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): MessageSent;
  hasUserData(): boolean;
  clearUserData(): MessageSent;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageSent.AsObject;
  static toObject(includeInstance: boolean, msg: MessageSent): MessageSent.AsObject;
  static serializeBinaryToWriter(message: MessageSent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageSent;
  static deserializeBinaryFromReader(message: MessageSent, reader: jspb.BinaryReader): MessageSent;
}

export namespace MessageSent {
  export type AsObject = {
    messageId: string,
    templateId: string,
    contactInfoListList: Array<pando_api_shared_pb.ContactInfo.AsObject>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class MessageUndeliverable extends jspb.Message {
  getMessageId(): string;
  setMessageId(value: string): MessageUndeliverable;

  getErrorMessage(): string;
  setErrorMessage(value: string): MessageUndeliverable;

  getTemplateId(): string;
  setTemplateId(value: string): MessageUndeliverable;

  getContactInfoListList(): Array<pando_api_shared_pb.ContactInfo>;
  setContactInfoListList(value: Array<pando_api_shared_pb.ContactInfo>): MessageUndeliverable;
  clearContactInfoListList(): MessageUndeliverable;
  addContactInfoList(value?: pando_api_shared_pb.ContactInfo, index?: number): pando_api_shared_pb.ContactInfo;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): MessageUndeliverable;
  hasUserData(): boolean;
  clearUserData(): MessageUndeliverable;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageUndeliverable.AsObject;
  static toObject(includeInstance: boolean, msg: MessageUndeliverable): MessageUndeliverable.AsObject;
  static serializeBinaryToWriter(message: MessageUndeliverable, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageUndeliverable;
  static deserializeBinaryFromReader(message: MessageUndeliverable, reader: jspb.BinaryReader): MessageUndeliverable;
}

export namespace MessageUndeliverable {
  export type AsObject = {
    messageId: string,
    errorMessage: string,
    templateId: string,
    contactInfoListList: Array<pando_api_shared_pb.ContactInfo.AsObject>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class MessageError extends jspb.Message {
  getMessageId(): string;
  setMessageId(value: string): MessageError;

  getErrorMessage(): string;
  setErrorMessage(value: string): MessageError;

  getTemplateId(): string;
  setTemplateId(value: string): MessageError;

  getContactInfoListList(): Array<pando_api_shared_pb.ContactInfo>;
  setContactInfoListList(value: Array<pando_api_shared_pb.ContactInfo>): MessageError;
  clearContactInfoListList(): MessageError;
  addContactInfoList(value?: pando_api_shared_pb.ContactInfo, index?: number): pando_api_shared_pb.ContactInfo;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): MessageError;
  hasUserData(): boolean;
  clearUserData(): MessageError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MessageError.AsObject;
  static toObject(includeInstance: boolean, msg: MessageError): MessageError.AsObject;
  static serializeBinaryToWriter(message: MessageError, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MessageError;
  static deserializeBinaryFromReader(message: MessageError, reader: jspb.BinaryReader): MessageError;
}

export namespace MessageError {
  export type AsObject = {
    messageId: string,
    errorMessage: string,
    templateId: string,
    contactInfoListList: Array<pando_api_shared_pb.ContactInfo.AsObject>,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class OrganizationContactInfoCreated extends jspb.Message {
  getContactInfoId(): string;
  setContactInfoId(value: string): OrganizationContactInfoCreated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): OrganizationContactInfoCreated;
  hasUserData(): boolean;
  clearUserData(): OrganizationContactInfoCreated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrganizationContactInfoCreated.AsObject;
  static toObject(includeInstance: boolean, msg: OrganizationContactInfoCreated): OrganizationContactInfoCreated.AsObject;
  static serializeBinaryToWriter(message: OrganizationContactInfoCreated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrganizationContactInfoCreated;
  static deserializeBinaryFromReader(message: OrganizationContactInfoCreated, reader: jspb.BinaryReader): OrganizationContactInfoCreated;
}

export namespace OrganizationContactInfoCreated {
  export type AsObject = {
    contactInfoId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class OrganizationContactInfoRetrieved extends jspb.Message {
  getContactInfoId(): string;
  setContactInfoId(value: string): OrganizationContactInfoRetrieved;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): OrganizationContactInfoRetrieved;
  hasUserData(): boolean;
  clearUserData(): OrganizationContactInfoRetrieved;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrganizationContactInfoRetrieved.AsObject;
  static toObject(includeInstance: boolean, msg: OrganizationContactInfoRetrieved): OrganizationContactInfoRetrieved.AsObject;
  static serializeBinaryToWriter(message: OrganizationContactInfoRetrieved, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrganizationContactInfoRetrieved;
  static deserializeBinaryFromReader(message: OrganizationContactInfoRetrieved, reader: jspb.BinaryReader): OrganizationContactInfoRetrieved;
}

export namespace OrganizationContactInfoRetrieved {
  export type AsObject = {
    contactInfoId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class OrganizationContactInfoDeleted extends jspb.Message {
  getContactInfoId(): string;
  setContactInfoId(value: string): OrganizationContactInfoDeleted;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): OrganizationContactInfoDeleted;
  hasUserData(): boolean;
  clearUserData(): OrganizationContactInfoDeleted;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrganizationContactInfoDeleted.AsObject;
  static toObject(includeInstance: boolean, msg: OrganizationContactInfoDeleted): OrganizationContactInfoDeleted.AsObject;
  static serializeBinaryToWriter(message: OrganizationContactInfoDeleted, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrganizationContactInfoDeleted;
  static deserializeBinaryFromReader(message: OrganizationContactInfoDeleted, reader: jspb.BinaryReader): OrganizationContactInfoDeleted;
}

export namespace OrganizationContactInfoDeleted {
  export type AsObject = {
    contactInfoId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class DefaultOrganizationContactInfoCreated extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): DefaultOrganizationContactInfoCreated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): DefaultOrganizationContactInfoCreated;
  hasUserData(): boolean;
  clearUserData(): DefaultOrganizationContactInfoCreated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DefaultOrganizationContactInfoCreated.AsObject;
  static toObject(includeInstance: boolean, msg: DefaultOrganizationContactInfoCreated): DefaultOrganizationContactInfoCreated.AsObject;
  static serializeBinaryToWriter(message: DefaultOrganizationContactInfoCreated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DefaultOrganizationContactInfoCreated;
  static deserializeBinaryFromReader(message: DefaultOrganizationContactInfoCreated, reader: jspb.BinaryReader): DefaultOrganizationContactInfoCreated;
}

export namespace DefaultOrganizationContactInfoCreated {
  export type AsObject = {
    organizationCode: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class DefaultOrganizationContactInfoRetrieved extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): DefaultOrganizationContactInfoRetrieved;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): DefaultOrganizationContactInfoRetrieved;
  hasUserData(): boolean;
  clearUserData(): DefaultOrganizationContactInfoRetrieved;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DefaultOrganizationContactInfoRetrieved.AsObject;
  static toObject(includeInstance: boolean, msg: DefaultOrganizationContactInfoRetrieved): DefaultOrganizationContactInfoRetrieved.AsObject;
  static serializeBinaryToWriter(message: DefaultOrganizationContactInfoRetrieved, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DefaultOrganizationContactInfoRetrieved;
  static deserializeBinaryFromReader(message: DefaultOrganizationContactInfoRetrieved, reader: jspb.BinaryReader): DefaultOrganizationContactInfoRetrieved;
}

export namespace DefaultOrganizationContactInfoRetrieved {
  export type AsObject = {
    organizationCode: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class DefaultOrganizationContactInfoUpdated extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): DefaultOrganizationContactInfoUpdated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): DefaultOrganizationContactInfoUpdated;
  hasUserData(): boolean;
  clearUserData(): DefaultOrganizationContactInfoUpdated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DefaultOrganizationContactInfoUpdated.AsObject;
  static toObject(includeInstance: boolean, msg: DefaultOrganizationContactInfoUpdated): DefaultOrganizationContactInfoUpdated.AsObject;
  static serializeBinaryToWriter(message: DefaultOrganizationContactInfoUpdated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DefaultOrganizationContactInfoUpdated;
  static deserializeBinaryFromReader(message: DefaultOrganizationContactInfoUpdated, reader: jspb.BinaryReader): DefaultOrganizationContactInfoUpdated;
}

export namespace DefaultOrganizationContactInfoUpdated {
  export type AsObject = {
    organizationCode: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class TemplateCreated extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateCreated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): TemplateCreated;
  hasUserData(): boolean;
  clearUserData(): TemplateCreated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateCreated.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateCreated): TemplateCreated.AsObject;
  static serializeBinaryToWriter(message: TemplateCreated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateCreated;
  static deserializeBinaryFromReader(message: TemplateCreated, reader: jspb.BinaryReader): TemplateCreated;
}

export namespace TemplateCreated {
  export type AsObject = {
    templateId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class TemplateRetrieved extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateRetrieved;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): TemplateRetrieved;
  hasUserData(): boolean;
  clearUserData(): TemplateRetrieved;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateRetrieved.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateRetrieved): TemplateRetrieved.AsObject;
  static serializeBinaryToWriter(message: TemplateRetrieved, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateRetrieved;
  static deserializeBinaryFromReader(message: TemplateRetrieved, reader: jspb.BinaryReader): TemplateRetrieved;
}

export namespace TemplateRetrieved {
  export type AsObject = {
    templateId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class TemplateUpdated extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateUpdated;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): TemplateUpdated;
  hasUserData(): boolean;
  clearUserData(): TemplateUpdated;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateUpdated.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateUpdated): TemplateUpdated.AsObject;
  static serializeBinaryToWriter(message: TemplateUpdated, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateUpdated;
  static deserializeBinaryFromReader(message: TemplateUpdated, reader: jspb.BinaryReader): TemplateUpdated;
}

export namespace TemplateUpdated {
  export type AsObject = {
    templateId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class TemplateDeleted extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): TemplateDeleted;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): TemplateDeleted;
  hasUserData(): boolean;
  clearUserData(): TemplateDeleted;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateDeleted.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateDeleted): TemplateDeleted.AsObject;
  static serializeBinaryToWriter(message: TemplateDeleted, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateDeleted;
  static deserializeBinaryFromReader(message: TemplateDeleted, reader: jspb.BinaryReader): TemplateDeleted;
}

export namespace TemplateDeleted {
  export type AsObject = {
    templateId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export enum EventTypes { 
  MESSAGE_CREATED = 0,
  MESSAGE_SENT = 1,
  MESSAGE_UNDELIVERABLE = 2,
  MESSAGE_ERROR = 3,
  ORGANIZATION_CONTACT_INFO_CREATED = 10,
  ORGANIZATION_CONTACT_INFO_RETRIEVED = 11,
  ORGANIZATION_CONTACT_INFO_DELETED = 12,
  DEFAULT_ORGANIZATION_CONTACT_INFO_CREATED = 20,
  DEFAULT_ORGANIZATION_CONTACT_INFO_RETRIEVED = 21,
  DEFAULT_ORGANIZATION_CONTACT_INFO_UPDATED = 22,
  TEMPLATE_CREATED = 30,
  TEMPLATE_RETRIEVED = 31,
  TEMPLATE_UPDATED = 32,
  TEMPLATE_DELETED = 33,
}
