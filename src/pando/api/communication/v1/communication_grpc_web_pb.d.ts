import * as grpcWeb from 'grpc-web';

import * as pando_api_communication_v1_communication_pb from '../../../../pando/api/communication/v1/communication_pb';


export class CommunicationServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execSendMessage(
    request: pando_api_communication_v1_communication_pb.SendMessageRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_communication_v1_communication_pb.SendMessageResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_communication_v1_communication_pb.SendMessageResponse>;

}

export class CommunicationServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execSendMessage(
    request: pando_api_communication_v1_communication_pb.SendMessageRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_communication_v1_communication_pb.SendMessageResponse>;

}

