import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class OrganizationContactInfo extends jspb.Message {
  getId(): string;
  setId(value: string): OrganizationContactInfo;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): OrganizationContactInfo;

  getContactInfo(): pando_api_shared_pb.ContactInfo | undefined;
  setContactInfo(value?: pando_api_shared_pb.ContactInfo): OrganizationContactInfo;
  hasContactInfo(): boolean;
  clearContactInfo(): OrganizationContactInfo;

  getIsDefault(): boolean;
  setIsDefault(value: boolean): OrganizationContactInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrganizationContactInfo.AsObject;
  static toObject(includeInstance: boolean, msg: OrganizationContactInfo): OrganizationContactInfo.AsObject;
  static serializeBinaryToWriter(message: OrganizationContactInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrganizationContactInfo;
  static deserializeBinaryFromReader(message: OrganizationContactInfo, reader: jspb.BinaryReader): OrganizationContactInfo;
}

export namespace OrganizationContactInfo {
  export type AsObject = {
    id: string,
    organizationCode: string,
    contactInfo?: pando_api_shared_pb.ContactInfo.AsObject,
    isDefault: boolean,
  }
}

export class DefaultOrganizationContactInfo extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): DefaultOrganizationContactInfo;

  getEmailAddress(): string;
  setEmailAddress(value: string): DefaultOrganizationContactInfo;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): DefaultOrganizationContactInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DefaultOrganizationContactInfo.AsObject;
  static toObject(includeInstance: boolean, msg: DefaultOrganizationContactInfo): DefaultOrganizationContactInfo.AsObject;
  static serializeBinaryToWriter(message: DefaultOrganizationContactInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DefaultOrganizationContactInfo;
  static deserializeBinaryFromReader(message: DefaultOrganizationContactInfo, reader: jspb.BinaryReader): DefaultOrganizationContactInfo;
}

export namespace DefaultOrganizationContactInfo {
  export type AsObject = {
    organizationCode: string,
    emailAddress: string,
    phoneNumber: string,
  }
}

export class GetOrganizationContactInfoRequest extends jspb.Message {
  getContactInfoId(): string;
  setContactInfoId(value: string): GetOrganizationContactInfoRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOrganizationContactInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOrganizationContactInfoRequest): GetOrganizationContactInfoRequest.AsObject;
  static serializeBinaryToWriter(message: GetOrganizationContactInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOrganizationContactInfoRequest;
  static deserializeBinaryFromReader(message: GetOrganizationContactInfoRequest, reader: jspb.BinaryReader): GetOrganizationContactInfoRequest;
}

export namespace GetOrganizationContactInfoRequest {
  export type AsObject = {
    contactInfoId: string,
  }
}

export class GetOrganizationContactInfoListRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetOrganizationContactInfoListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOrganizationContactInfoListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOrganizationContactInfoListRequest): GetOrganizationContactInfoListRequest.AsObject;
  static serializeBinaryToWriter(message: GetOrganizationContactInfoListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOrganizationContactInfoListRequest;
  static deserializeBinaryFromReader(message: GetOrganizationContactInfoListRequest, reader: jspb.BinaryReader): GetOrganizationContactInfoListRequest;
}

export namespace GetOrganizationContactInfoListRequest {
  export type AsObject = {
    organizationCode: string,
  }
}

export class DeleteOrganizationContactInfoRequest extends jspb.Message {
  getContactInfoId(): string;
  setContactInfoId(value: string): DeleteOrganizationContactInfoRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteOrganizationContactInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteOrganizationContactInfoRequest): DeleteOrganizationContactInfoRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteOrganizationContactInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteOrganizationContactInfoRequest;
  static deserializeBinaryFromReader(message: DeleteOrganizationContactInfoRequest, reader: jspb.BinaryReader): DeleteOrganizationContactInfoRequest;
}

export namespace DeleteOrganizationContactInfoRequest {
  export type AsObject = {
    contactInfoId: string,
  }
}

export class GetDefaultOrganizationContactInfoRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetDefaultOrganizationContactInfoRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDefaultOrganizationContactInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDefaultOrganizationContactInfoRequest): GetDefaultOrganizationContactInfoRequest.AsObject;
  static serializeBinaryToWriter(message: GetDefaultOrganizationContactInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDefaultOrganizationContactInfoRequest;
  static deserializeBinaryFromReader(message: GetDefaultOrganizationContactInfoRequest, reader: jspb.BinaryReader): GetDefaultOrganizationContactInfoRequest;
}

export namespace GetDefaultOrganizationContactInfoRequest {
  export type AsObject = {
    organizationCode: string,
  }
}

export class CreateOrUpdateDefaultOrganizationContactInfoRequest extends jspb.Message {
  getDefaultOrganizationContactInfo(): DefaultOrganizationContactInfo | undefined;
  setDefaultOrganizationContactInfo(value?: DefaultOrganizationContactInfo): CreateOrUpdateDefaultOrganizationContactInfoRequest;
  hasDefaultOrganizationContactInfo(): boolean;
  clearDefaultOrganizationContactInfo(): CreateOrUpdateDefaultOrganizationContactInfoRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateDefaultOrganizationContactInfoRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateDefaultOrganizationContactInfoRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateDefaultOrganizationContactInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateDefaultOrganizationContactInfoRequest): CreateOrUpdateDefaultOrganizationContactInfoRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateDefaultOrganizationContactInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateDefaultOrganizationContactInfoRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateDefaultOrganizationContactInfoRequest, reader: jspb.BinaryReader): CreateOrUpdateDefaultOrganizationContactInfoRequest;
}

export namespace CreateOrUpdateDefaultOrganizationContactInfoRequest {
  export type AsObject = {
    defaultOrganizationContactInfo?: DefaultOrganizationContactInfo.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class OrganizationContactInfoList extends jspb.Message {
  getOrganizationContactInfoListList(): Array<OrganizationContactInfo>;
  setOrganizationContactInfoListList(value: Array<OrganizationContactInfo>): OrganizationContactInfoList;
  clearOrganizationContactInfoListList(): OrganizationContactInfoList;
  addOrganizationContactInfoList(value?: OrganizationContactInfo, index?: number): OrganizationContactInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrganizationContactInfoList.AsObject;
  static toObject(includeInstance: boolean, msg: OrganizationContactInfoList): OrganizationContactInfoList.AsObject;
  static serializeBinaryToWriter(message: OrganizationContactInfoList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrganizationContactInfoList;
  static deserializeBinaryFromReader(message: OrganizationContactInfoList, reader: jspb.BinaryReader): OrganizationContactInfoList;
}

export namespace OrganizationContactInfoList {
  export type AsObject = {
    organizationContactInfoListList: Array<OrganizationContactInfo.AsObject>,
  }
}

export class Template extends jspb.Message {
  getId(): string;
  setId(value: string): Template;

  getName(): string;
  setName(value: string): Template;

  getDesciption(): string;
  setDesciption(value: string): Template;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Template;

  getTextMessage(): string;
  setTextMessage(value: string): Template;

  getEmailId(): string;
  setEmailId(value: string): Template;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Template.AsObject;
  static toObject(includeInstance: boolean, msg: Template): Template.AsObject;
  static serializeBinaryToWriter(message: Template, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Template;
  static deserializeBinaryFromReader(message: Template, reader: jspb.BinaryReader): Template;
}

export namespace Template {
  export type AsObject = {
    id: string,
    name: string,
    desciption: string,
    organizationCode: string,
    textMessage: string,
    emailId: string,
  }
}

export class GetTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): GetTemplateRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetTemplateRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplateRequest): GetTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplateRequest;
  static deserializeBinaryFromReader(message: GetTemplateRequest, reader: jspb.BinaryReader): GetTemplateRequest;
}

export namespace GetTemplateRequest {
  export type AsObject = {
    templateId: string,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class GetTemplatesRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetTemplatesRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetTemplatesRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetTemplatesRequest;

  getPage(): number;
  setPage(value: number): GetTemplatesRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetTemplatesRequest;

  getFilter(): string;
  setFilter(value: string): GetTemplatesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTemplatesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTemplatesRequest): GetTemplatesRequest.AsObject;
  static serializeBinaryToWriter(message: GetTemplatesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTemplatesRequest;
  static deserializeBinaryFromReader(message: GetTemplatesRequest, reader: jspb.BinaryReader): GetTemplatesRequest;
}

export namespace GetTemplatesRequest {
  export type AsObject = {
    organizationCode: string,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
    page: number,
    resultsPerPage: number,
    filter: string,
  }
}

export class CreateOrUpdateTemplateRequest extends jspb.Message {
  getTemplate(): Template | undefined;
  setTemplate(value?: Template): CreateOrUpdateTemplateRequest;
  hasTemplate(): boolean;
  clearTemplate(): CreateOrUpdateTemplateRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): CreateOrUpdateTemplateRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): CreateOrUpdateTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateOrUpdateTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateOrUpdateTemplateRequest): CreateOrUpdateTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: CreateOrUpdateTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateOrUpdateTemplateRequest;
  static deserializeBinaryFromReader(message: CreateOrUpdateTemplateRequest, reader: jspb.BinaryReader): CreateOrUpdateTemplateRequest;
}

export namespace CreateOrUpdateTemplateRequest {
  export type AsObject = {
    template?: Template.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteTemplateRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): DeleteTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteTemplateRequest): DeleteTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteTemplateRequest;
  static deserializeBinaryFromReader(message: DeleteTemplateRequest, reader: jspb.BinaryReader): DeleteTemplateRequest;
}

export namespace DeleteTemplateRequest {
  export type AsObject = {
    templateId: string,
  }
}

export class TemplateList extends jspb.Message {
  getTemplatesList(): Array<Template>;
  setTemplatesList(value: Array<Template>): TemplateList;
  clearTemplatesList(): TemplateList;
  addTemplates(value?: Template, index?: number): Template;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TemplateList.AsObject;
  static toObject(includeInstance: boolean, msg: TemplateList): TemplateList.AsObject;
  static serializeBinaryToWriter(message: TemplateList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TemplateList;
  static deserializeBinaryFromReader(message: TemplateList, reader: jspb.BinaryReader): TemplateList;
}

export namespace TemplateList {
  export type AsObject = {
    templatesList: Array<Template.AsObject>,
  }
}

