import * as jspb from 'google-protobuf'

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class SendMessageRequest extends jspb.Message {
  getTemplateId(): string;
  setTemplateId(value: string): SendMessageRequest;

  getDateToSend(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateToSend(value?: google_protobuf_timestamp_pb.Timestamp): SendMessageRequest;
  hasDateToSend(): boolean;
  clearDateToSend(): SendMessageRequest;

  getFromPhoneId(): string;
  setFromPhoneId(value: string): SendMessageRequest;

  getFromPhone(): string;
  setFromPhone(value: string): SendMessageRequest;

  getFromEmailId(): string;
  setFromEmailId(value: string): SendMessageRequest;

  getFromEmail(): string;
  setFromEmail(value: string): SendMessageRequest;

  getVaultRecordId(): string;
  setVaultRecordId(value: string): SendMessageRequest;

  getDataRecord(): pando_api_shared_pb.DataRecord | undefined;
  setDataRecord(value?: pando_api_shared_pb.DataRecord): SendMessageRequest;
  hasDataRecord(): boolean;
  clearDataRecord(): SendMessageRequest;

  getKeyValues(): KeyValueMap | undefined;
  setKeyValues(value?: KeyValueMap): SendMessageRequest;
  hasKeyValues(): boolean;
  clearKeyValues(): SendMessageRequest;

  getRecipientGroupId(): string;
  setRecipientGroupId(value: string): SendMessageRequest;

  getRecipientList(): RecipientList | undefined;
  setRecipientList(value?: RecipientList): SendMessageRequest;
  hasRecipientList(): boolean;
  clearRecipientList(): SendMessageRequest;

  getFromPhoneOptionCase(): SendMessageRequest.FromPhoneOptionCase;

  getFromEmailOptionCase(): SendMessageRequest.FromEmailOptionCase;

  getDataSourceCase(): SendMessageRequest.DataSourceCase;

  getRecipientsCase(): SendMessageRequest.RecipientsCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendMessageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendMessageRequest): SendMessageRequest.AsObject;
  static serializeBinaryToWriter(message: SendMessageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendMessageRequest;
  static deserializeBinaryFromReader(message: SendMessageRequest, reader: jspb.BinaryReader): SendMessageRequest;
}

export namespace SendMessageRequest {
  export type AsObject = {
    templateId: string,
    dateToSend?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    fromPhoneId: string,
    fromPhone: string,
    fromEmailId: string,
    fromEmail: string,
    vaultRecordId: string,
    dataRecord?: pando_api_shared_pb.DataRecord.AsObject,
    keyValues?: KeyValueMap.AsObject,
    recipientGroupId: string,
    recipientList?: RecipientList.AsObject,
  }

  export enum FromPhoneOptionCase { 
    FROM_PHONE_OPTION_NOT_SET = 0,
    FROM_PHONE_ID = 3,
    FROM_PHONE = 4,
  }

  export enum FromEmailOptionCase { 
    FROM_EMAIL_OPTION_NOT_SET = 0,
    FROM_EMAIL_ID = 5,
    FROM_EMAIL = 6,
  }

  export enum DataSourceCase { 
    DATA_SOURCE_NOT_SET = 0,
    VAULT_RECORD_ID = 7,
    DATA_RECORD = 8,
    KEY_VALUES = 9,
  }

  export enum RecipientsCase { 
    RECIPIENTS_NOT_SET = 0,
    RECIPIENT_GROUP_ID = 10,
    RECIPIENT_LIST = 11,
  }
}

export class RecipientList extends jspb.Message {
  getRecipientsList(): Array<Recipient>;
  setRecipientsList(value: Array<Recipient>): RecipientList;
  clearRecipientsList(): RecipientList;
  addRecipients(value?: Recipient, index?: number): Recipient;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecipientList.AsObject;
  static toObject(includeInstance: boolean, msg: RecipientList): RecipientList.AsObject;
  static serializeBinaryToWriter(message: RecipientList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecipientList;
  static deserializeBinaryFromReader(message: RecipientList, reader: jspb.BinaryReader): RecipientList;
}

export namespace RecipientList {
  export type AsObject = {
    recipientsList: Array<Recipient.AsObject>,
  }
}

export class Recipient extends jspb.Message {
  getContactInfo(): pando_api_shared_pb.ContactInfo | undefined;
  setContactInfo(value?: pando_api_shared_pb.ContactInfo): Recipient;
  hasContactInfo(): boolean;
  clearContactInfo(): Recipient;

  getEmailType(): EmailType;
  setEmailType(value: EmailType): Recipient;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Recipient.AsObject;
  static toObject(includeInstance: boolean, msg: Recipient): Recipient.AsObject;
  static serializeBinaryToWriter(message: Recipient, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Recipient;
  static deserializeBinaryFromReader(message: Recipient, reader: jspb.BinaryReader): Recipient;
}

export namespace Recipient {
  export type AsObject = {
    contactInfo?: pando_api_shared_pb.ContactInfo.AsObject,
    emailType: EmailType,
  }
}

export class SendMessageResponse extends jspb.Message {
  getMessageId(): string;
  setMessageId(value: string): SendMessageResponse;

  getRecipientGroupId(): string;
  setRecipientGroupId(value: string): SendMessageResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendMessageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SendMessageResponse): SendMessageResponse.AsObject;
  static serializeBinaryToWriter(message: SendMessageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendMessageResponse;
  static deserializeBinaryFromReader(message: SendMessageResponse, reader: jspb.BinaryReader): SendMessageResponse;
}

export namespace SendMessageResponse {
  export type AsObject = {
    messageId: string,
    recipientGroupId: string,
  }
}

export class KeyValueMap extends jspb.Message {
  getKeyValuesMap(): jspb.Map<string, string>;
  clearKeyValuesMap(): KeyValueMap;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): KeyValueMap.AsObject;
  static toObject(includeInstance: boolean, msg: KeyValueMap): KeyValueMap.AsObject;
  static serializeBinaryToWriter(message: KeyValueMap, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): KeyValueMap;
  static deserializeBinaryFromReader(message: KeyValueMap, reader: jspb.BinaryReader): KeyValueMap;
}

export namespace KeyValueMap {
  export type AsObject = {
    keyValuesMap: Array<[string, string]>,
  }
}

export enum EmailType { 
  UNSET_EMAIL_TYPE = 0,
  TO = 1,
  CC = 2,
  BCC = 3,
}
