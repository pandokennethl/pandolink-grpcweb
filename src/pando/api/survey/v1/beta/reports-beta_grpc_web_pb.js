/**
 * @fileoverview gRPC-Web generated client stub for pando.api.survey.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('../../../../../google/api/annotations_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')

var pando_api_extensions_pb = require('../../../../../pando/api/extensions_pb.js')

var pando_api_shared_pb = require('../../../../../pando/api/shared_pb.js')
const proto = {};
proto.pando = {};
proto.pando.api = {};
proto.pando.api.survey = {};
proto.pando.api.survey.v1 = require('./reports-beta_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.survey.v1.SurveyReportBetaServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.survey.v1.SurveyReportBetaServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetReportInstancesRequest,
 *   !proto.pando.api.survey.v1.ReportInstanceList>}
 */
const methodDescriptor_SurveyReportBetaService_GetSurveyInstances = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyInstances',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.GetReportInstancesRequest,
  proto.pando.api.survey.v1.ReportInstanceList,
  /**
   * @param {!proto.pando.api.survey.v1.GetReportInstancesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.ReportInstanceList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetReportInstancesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.ReportInstanceList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.ReportInstanceList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyReportBetaServiceClient.prototype.getSurveyInstances =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyInstances',
      request,
      metadata || {},
      methodDescriptor_SurveyReportBetaService_GetSurveyInstances,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetReportInstancesRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.ReportInstanceList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyReportBetaServicePromiseClient.prototype.getSurveyInstances =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyInstances',
      request,
      metadata || {},
      methodDescriptor_SurveyReportBetaService_GetSurveyInstances);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetSurveyCompletionStatusesRequest,
 *   !proto.pando.api.survey.v1.SurveyCompletionStatusList>}
 */
const methodDescriptor_SurveyReportBetaService_GetSurveyCompletionStatuses = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyCompletionStatuses',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.GetSurveyCompletionStatusesRequest,
  proto.pando.api.survey.v1.SurveyCompletionStatusList,
  /**
   * @param {!proto.pando.api.survey.v1.GetSurveyCompletionStatusesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.SurveyCompletionStatusList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetSurveyCompletionStatusesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyCompletionStatusList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyCompletionStatusList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyReportBetaServiceClient.prototype.getSurveyCompletionStatuses =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyCompletionStatuses',
      request,
      metadata || {},
      methodDescriptor_SurveyReportBetaService_GetSurveyCompletionStatuses,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetSurveyCompletionStatusesRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyCompletionStatusList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyReportBetaServicePromiseClient.prototype.getSurveyCompletionStatuses =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyReportBetaService/GetSurveyCompletionStatuses',
      request,
      metadata || {},
      methodDescriptor_SurveyReportBetaService_GetSurveyCompletionStatuses);
};


module.exports = proto.pando.api.survey.v1;

