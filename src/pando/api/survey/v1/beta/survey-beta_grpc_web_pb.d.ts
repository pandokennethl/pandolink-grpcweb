import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_survey_v1_beta_survey$beta_pb from '../../../../../pando/api/survey/v1/beta/survey-beta_pb';


export class SurveyBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execClaimInstance(
    request: pando_api_survey_v1_beta_survey$beta_pb.ExecClaimInstanceRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetInstanceShortListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetInstanceShortListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetRecentlyCompletedSurveyInstancesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getSurveyInstancesToDo(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetSurveyInstancesToDoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

}

export class SurveyBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  execClaimInstance(
    request: pando_api_survey_v1_beta_survey$beta_pb.ExecClaimInstanceRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetInstanceShortListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetInstanceShortListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetRecentlyCompletedSurveyInstancesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

  getSurveyInstancesToDo(
    request: pando_api_survey_v1_beta_survey$beta_pb.GetSurveyInstancesToDoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_survey$beta_pb.InstanceShortList>;

}

