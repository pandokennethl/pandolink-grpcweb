import * as grpcWeb from 'grpc-web';

import * as pando_api_survey_v1_beta_admin$beta_pb from '../../../../../pando/api/survey/v1/beta/admin-beta_pb';
import * as pando_api_survey_v1_admin_pb from '../../../../../pando/api/survey/v1/admin_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../../pando/api/survey/v1/shared_pb';


export class SurveyAdminBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getSurveysByESignTemplate(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysByESignTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList>;

  execPublishSurvey(
    request: pando_api_survey_v1_beta_admin$beta_pb.PublishSurveyRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Survey) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Survey>;

  createInstance(
    request: pando_api_survey_v1_beta_admin$beta_pb.CreateInstanceBetaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_admin$beta_pb.CreateInstanceResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_admin$beta_pb.CreateInstanceResponse>;

  getSurveysByVariable(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysByVariableRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList>;

  updateSurveyVariable(
    request: pando_api_survey_v1_admin_pb.SurveyVariable,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.SurveyVariable) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.SurveyVariable>;

  getSurveyVariables(
    request: pando_api_survey_v1_admin_pb.GetSurveyVariablesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_admin$beta_pb.SurveyVariableDetailList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_admin$beta_pb.SurveyVariableDetailList>;

  getSurveys(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysBetaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_admin$beta_pb.SurveyBasicList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_admin$beta_pb.SurveyBasicList>;

}

export class SurveyAdminBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getSurveysByESignTemplate(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysByESignTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList>;

  execPublishSurvey(
    request: pando_api_survey_v1_beta_admin$beta_pb.PublishSurveyRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Survey>;

  createInstance(
    request: pando_api_survey_v1_beta_admin$beta_pb.CreateInstanceBetaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_admin$beta_pb.CreateInstanceResponse>;

  getSurveysByVariable(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysByVariableRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_admin$beta_pb.SurveyShortList>;

  updateSurveyVariable(
    request: pando_api_survey_v1_admin_pb.SurveyVariable,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.SurveyVariable>;

  getSurveyVariables(
    request: pando_api_survey_v1_admin_pb.GetSurveyVariablesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_admin$beta_pb.SurveyVariableDetailList>;

  getSurveys(
    request: pando_api_survey_v1_beta_admin$beta_pb.GetSurveysBetaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_admin$beta_pb.SurveyBasicList>;

}

