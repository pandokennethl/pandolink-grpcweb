import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../../pando/api/survey/v1/shared_pb';
import * as pando_api_survey_v1_admin_pb from '../../../../../pando/api/survey/v1/admin_pb';


export class PublishSurveyRequest extends jspb.Message {
  getSurveyGuid(): string;
  setSurveyGuid(value: string): PublishSurveyRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PublishSurveyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: PublishSurveyRequest): PublishSurveyRequest.AsObject;
  static serializeBinaryToWriter(message: PublishSurveyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PublishSurveyRequest;
  static deserializeBinaryFromReader(message: PublishSurveyRequest, reader: jspb.BinaryReader): PublishSurveyRequest;
}

export namespace PublishSurveyRequest {
  export type AsObject = {
    surveyGuid: string,
  }
}

export class CreateInstanceBetaRequest extends jspb.Message {
  getSurveyGuid(): string;
  setSurveyGuid(value: string): CreateInstanceBetaRequest;

  getData(): pando_api_shared_pb.DataRecord | undefined;
  setData(value?: pando_api_shared_pb.DataRecord): CreateInstanceBetaRequest;
  hasData(): boolean;
  clearData(): CreateInstanceBetaRequest;

  getRecordGuid(): string;
  setRecordGuid(value: string): CreateInstanceBetaRequest;

  getContactInfo(): pando_api_shared_pb.ContactInfo | undefined;
  setContactInfo(value?: pando_api_shared_pb.ContactInfo): CreateInstanceBetaRequest;
  hasContactInfo(): boolean;
  clearContactInfo(): CreateInstanceBetaRequest;

  getRespondentName(): string;
  setRespondentName(value: string): CreateInstanceBetaRequest;

  getSendLink(): boolean;
  setSendLink(value: boolean): CreateInstanceBetaRequest;

  getUserId(): string;
  setUserId(value: string): CreateInstanceBetaRequest;

  getClaimCode(): string;
  setClaimCode(value: string): CreateInstanceBetaRequest;

  getDataSourceCase(): CreateInstanceBetaRequest.DataSourceCase;

  getInstanceOwnerCase(): CreateInstanceBetaRequest.InstanceOwnerCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateInstanceBetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateInstanceBetaRequest): CreateInstanceBetaRequest.AsObject;
  static serializeBinaryToWriter(message: CreateInstanceBetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateInstanceBetaRequest;
  static deserializeBinaryFromReader(message: CreateInstanceBetaRequest, reader: jspb.BinaryReader): CreateInstanceBetaRequest;
}

export namespace CreateInstanceBetaRequest {
  export type AsObject = {
    surveyGuid: string,
    data?: pando_api_shared_pb.DataRecord.AsObject,
    recordGuid: string,
    contactInfo?: pando_api_shared_pb.ContactInfo.AsObject,
    respondentName: string,
    sendLink: boolean,
    userId: string,
    claimCode: string,
  }

  export enum DataSourceCase { 
    DATA_SOURCE_NOT_SET = 0,
    DATA = 2,
    RECORD_GUID = 3,
  }

  export enum InstanceOwnerCase { 
    INSTANCE_OWNER_NOT_SET = 0,
    USER_ID = 8,
    CLAIM_CODE = 9,
  }
}

export class CreateInstanceResponse extends jspb.Message {
  getSurveyInstanceGuid(): string;
  setSurveyInstanceGuid(value: string): CreateInstanceResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateInstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateInstanceResponse): CreateInstanceResponse.AsObject;
  static serializeBinaryToWriter(message: CreateInstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateInstanceResponse;
  static deserializeBinaryFromReader(message: CreateInstanceResponse, reader: jspb.BinaryReader): CreateInstanceResponse;
}

export namespace CreateInstanceResponse {
  export type AsObject = {
    surveyInstanceGuid: string,
  }
}

export class GetSurveysByESignTemplateRequest extends jspb.Message {
  getTemplateGuid(): string;
  setTemplateGuid(value: string): GetSurveysByESignTemplateRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetSurveysByESignTemplateRequest;

  getPage(): number;
  setPage(value: number): GetSurveysByESignTemplateRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetSurveysByESignTemplateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSurveysByESignTemplateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSurveysByESignTemplateRequest): GetSurveysByESignTemplateRequest.AsObject;
  static serializeBinaryToWriter(message: GetSurveysByESignTemplateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSurveysByESignTemplateRequest;
  static deserializeBinaryFromReader(message: GetSurveysByESignTemplateRequest, reader: jspb.BinaryReader): GetSurveysByESignTemplateRequest;
}

export namespace GetSurveysByESignTemplateRequest {
  export type AsObject = {
    templateGuid: string,
    organizationCode: string,
    page: number,
    resultsPerPage: number,
  }
}

export class SurveyShort extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): SurveyShort;

  getName(): string;
  setName(value: string): SurveyShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyShort.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyShort): SurveyShort.AsObject;
  static serializeBinaryToWriter(message: SurveyShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyShort;
  static deserializeBinaryFromReader(message: SurveyShort, reader: jspb.BinaryReader): SurveyShort;
}

export namespace SurveyShort {
  export type AsObject = {
    guid: string,
    name: string,
  }
}

export class SurveyShortList extends jspb.Message {
  getSurveysList(): Array<SurveyShort>;
  setSurveysList(value: Array<SurveyShort>): SurveyShortList;
  clearSurveysList(): SurveyShortList;
  addSurveys(value?: SurveyShort, index?: number): SurveyShort;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): SurveyShortList;
  hasPagination(): boolean;
  clearPagination(): SurveyShortList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyShortList.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyShortList): SurveyShortList.AsObject;
  static serializeBinaryToWriter(message: SurveyShortList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyShortList;
  static deserializeBinaryFromReader(message: SurveyShortList, reader: jspb.BinaryReader): SurveyShortList;
}

export namespace SurveyShortList {
  export type AsObject = {
    surveysList: Array<SurveyShort.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class GetSurveysByVariableRequest extends jspb.Message {
  getVariableGuid(): string;
  setVariableGuid(value: string): GetSurveysByVariableRequest;

  getPublished(): boolean;
  setPublished(value: boolean): GetSurveysByVariableRequest;

  getPage(): number;
  setPage(value: number): GetSurveysByVariableRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetSurveysByVariableRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSurveysByVariableRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSurveysByVariableRequest): GetSurveysByVariableRequest.AsObject;
  static serializeBinaryToWriter(message: GetSurveysByVariableRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSurveysByVariableRequest;
  static deserializeBinaryFromReader(message: GetSurveysByVariableRequest, reader: jspb.BinaryReader): GetSurveysByVariableRequest;
}

export namespace GetSurveysByVariableRequest {
  export type AsObject = {
    variableGuid: string,
    published: boolean,
    page: number,
    resultsPerPage: number,
  }
}

export class SurveyVariableDetail extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): SurveyVariableDetail;

  getName(): string;
  setName(value: string): SurveyVariableDetail;

  getVariableType(): pando_api_shared_pb.VariableType;
  setVariableType(value: pando_api_shared_pb.VariableType): SurveyVariableDetail;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): SurveyVariableDetail;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): SurveyVariableDetail;

  getRequired(): boolean;
  setRequired(value: boolean): SurveyVariableDetail;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyVariableDetail.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyVariableDetail): SurveyVariableDetail.AsObject;
  static serializeBinaryToWriter(message: SurveyVariableDetail, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyVariableDetail;
  static deserializeBinaryFromReader(message: SurveyVariableDetail, reader: jspb.BinaryReader): SurveyVariableDetail;
}

export namespace SurveyVariableDetail {
  export type AsObject = {
    guid: string,
    name: string,
    variableType: pando_api_shared_pb.VariableType,
    organizationCode: string,
    isDeleted: boolean,
    required: boolean,
  }
}

export class SurveyVariableDetailList extends jspb.Message {
  getSurveyVariablesList(): Array<SurveyVariableDetail>;
  setSurveyVariablesList(value: Array<SurveyVariableDetail>): SurveyVariableDetailList;
  clearSurveyVariablesList(): SurveyVariableDetailList;
  addSurveyVariables(value?: SurveyVariableDetail, index?: number): SurveyVariableDetail;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): SurveyVariableDetailList;
  hasPagination(): boolean;
  clearPagination(): SurveyVariableDetailList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyVariableDetailList.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyVariableDetailList): SurveyVariableDetailList.AsObject;
  static serializeBinaryToWriter(message: SurveyVariableDetailList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyVariableDetailList;
  static deserializeBinaryFromReader(message: SurveyVariableDetailList, reader: jspb.BinaryReader): SurveyVariableDetailList;
}

export namespace SurveyVariableDetailList {
  export type AsObject = {
    surveyVariablesList: Array<SurveyVariableDetail.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class GetSurveysBetaRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetSurveysBetaRequest;

  getFilter(): string;
  setFilter(value: string): GetSurveysBetaRequest;

  getIncludeDeleted(): boolean;
  setIncludeDeleted(value: boolean): GetSurveysBetaRequest;

  getIncludeUnpublishedVersions(): boolean;
  setIncludeUnpublishedVersions(value: boolean): GetSurveysBetaRequest;

  getPage(): number;
  setPage(value: number): GetSurveysBetaRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetSurveysBetaRequest;

  getReadMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setReadMask(value?: google_protobuf_field_mask_pb.FieldMask): GetSurveysBetaRequest;
  hasReadMask(): boolean;
  clearReadMask(): GetSurveysBetaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSurveysBetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSurveysBetaRequest): GetSurveysBetaRequest.AsObject;
  static serializeBinaryToWriter(message: GetSurveysBetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSurveysBetaRequest;
  static deserializeBinaryFromReader(message: GetSurveysBetaRequest, reader: jspb.BinaryReader): GetSurveysBetaRequest;
}

export namespace GetSurveysBetaRequest {
  export type AsObject = {
    organizationCode: string,
    filter: string,
    includeDeleted: boolean,
    includeUnpublishedVersions: boolean,
    page: number,
    resultsPerPage: number,
    readMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class SurveyBasic extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): SurveyBasic;

  getName(): string;
  setName(value: string): SurveyBasic;

  getDescription(): string;
  setDescription(value: string): SurveyBasic;

  getPreSurveyText(): string;
  setPreSurveyText(value: string): SurveyBasic;

  getFirstQuestionGuid(): string;
  setFirstQuestionGuid(value: string): SurveyBasic;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): SurveyBasic;

  getOrganizationName(): string;
  setOrganizationName(value: string): SurveyBasic;

  getCurrentVersionGuid(): string;
  setCurrentVersionGuid(value: string): SurveyBasic;

  getUnpublishedVersionGuid(): string;
  setUnpublishedVersionGuid(value: string): SurveyBasic;

  getDatePublished(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDatePublished(value?: google_protobuf_timestamp_pb.Timestamp): SurveyBasic;
  hasDatePublished(): boolean;
  clearDatePublished(): SurveyBasic;

  getIsDeleted(): boolean;
  setIsDeleted(value: boolean): SurveyBasic;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyBasic.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyBasic): SurveyBasic.AsObject;
  static serializeBinaryToWriter(message: SurveyBasic, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyBasic;
  static deserializeBinaryFromReader(message: SurveyBasic, reader: jspb.BinaryReader): SurveyBasic;
}

export namespace SurveyBasic {
  export type AsObject = {
    guid: string,
    name: string,
    description: string,
    preSurveyText: string,
    firstQuestionGuid: string,
    organizationCode: string,
    organizationName: string,
    currentVersionGuid: string,
    unpublishedVersionGuid: string,
    datePublished?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    isDeleted: boolean,
  }
}

export class SurveyBasicList extends jspb.Message {
  getSurveysList(): Array<SurveyBasic>;
  setSurveysList(value: Array<SurveyBasic>): SurveyBasicList;
  clearSurveysList(): SurveyBasicList;
  addSurveys(value?: SurveyBasic, index?: number): SurveyBasic;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): SurveyBasicList;
  hasPagination(): boolean;
  clearPagination(): SurveyBasicList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyBasicList.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyBasicList): SurveyBasicList.AsObject;
  static serializeBinaryToWriter(message: SurveyBasicList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyBasicList;
  static deserializeBinaryFromReader(message: SurveyBasicList, reader: jspb.BinaryReader): SurveyBasicList;
}

export namespace SurveyBasicList {
  export type AsObject = {
    surveysList: Array<SurveyBasic.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

