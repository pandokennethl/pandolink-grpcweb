import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../../pando/api/survey/v1/shared_pb';


export class ExecClaimInstanceRequest extends jspb.Message {
  getInstanceGuid(): string;
  setInstanceGuid(value: string): ExecClaimInstanceRequest;

  getClaimCode(): string;
  setClaimCode(value: string): ExecClaimInstanceRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecClaimInstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecClaimInstanceRequest): ExecClaimInstanceRequest.AsObject;
  static serializeBinaryToWriter(message: ExecClaimInstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecClaimInstanceRequest;
  static deserializeBinaryFromReader(message: ExecClaimInstanceRequest, reader: jspb.BinaryReader): ExecClaimInstanceRequest;
}

export namespace ExecClaimInstanceRequest {
  export type AsObject = {
    instanceGuid: string,
    claimCode: string,
  }
}

export class GetInstanceShortListRequest extends jspb.Message {
  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetInstanceShortListRequest;
  hasStart(): boolean;
  clearStart(): GetInstanceShortListRequest;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetInstanceShortListRequest;
  hasEnd(): boolean;
  clearEnd(): GetInstanceShortListRequest;

  getPage(): number;
  setPage(value: number): GetInstanceShortListRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetInstanceShortListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInstanceShortListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetInstanceShortListRequest): GetInstanceShortListRequest.AsObject;
  static serializeBinaryToWriter(message: GetInstanceShortListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInstanceShortListRequest;
  static deserializeBinaryFromReader(message: GetInstanceShortListRequest, reader: jspb.BinaryReader): GetInstanceShortListRequest;
}

export namespace GetInstanceShortListRequest {
  export type AsObject = {
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    page: number,
    resultsPerPage: number,
  }
}

export class GetRecentlyCompletedSurveyInstancesRequest extends jspb.Message {
  getPage(): number;
  setPage(value: number): GetRecentlyCompletedSurveyInstancesRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetRecentlyCompletedSurveyInstancesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRecentlyCompletedSurveyInstancesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRecentlyCompletedSurveyInstancesRequest): GetRecentlyCompletedSurveyInstancesRequest.AsObject;
  static serializeBinaryToWriter(message: GetRecentlyCompletedSurveyInstancesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRecentlyCompletedSurveyInstancesRequest;
  static deserializeBinaryFromReader(message: GetRecentlyCompletedSurveyInstancesRequest, reader: jspb.BinaryReader): GetRecentlyCompletedSurveyInstancesRequest;
}

export namespace GetRecentlyCompletedSurveyInstancesRequest {
  export type AsObject = {
    page: number,
    resultsPerPage: number,
  }
}

export class GetSurveyInstancesToDoRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSurveyInstancesToDoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSurveyInstancesToDoRequest): GetSurveyInstancesToDoRequest.AsObject;
  static serializeBinaryToWriter(message: GetSurveyInstancesToDoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSurveyInstancesToDoRequest;
  static deserializeBinaryFromReader(message: GetSurveyInstancesToDoRequest, reader: jspb.BinaryReader): GetSurveyInstancesToDoRequest;
}

export namespace GetSurveyInstancesToDoRequest {
  export type AsObject = {
  }
}

export class InstanceShort extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): InstanceShort;

  getSurveyName(): string;
  setSurveyName(value: string): InstanceShort;

  getSurveyDescription(): string;
  setSurveyDescription(value: string): InstanceShort;

  getOrganizationName(): string;
  setOrganizationName(value: string): InstanceShort;

  getCompletionType(): pando_api_survey_v1_shared_pb.CompletionType;
  setCompletionType(value: pando_api_survey_v1_shared_pb.CompletionType): InstanceShort;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): InstanceShort;
  hasDateCreated(): boolean;
  clearDateCreated(): InstanceShort;

  getDateCompleted(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCompleted(value?: google_protobuf_timestamp_pb.Timestamp): InstanceShort;
  hasDateCompleted(): boolean;
  clearDateCompleted(): InstanceShort;

  getTotalAnswered(): number;
  setTotalAnswered(value: number): InstanceShort;

  getTotalQuestions(): number;
  setTotalQuestions(value: number): InstanceShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InstanceShort.AsObject;
  static toObject(includeInstance: boolean, msg: InstanceShort): InstanceShort.AsObject;
  static serializeBinaryToWriter(message: InstanceShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InstanceShort;
  static deserializeBinaryFromReader(message: InstanceShort, reader: jspb.BinaryReader): InstanceShort;
}

export namespace InstanceShort {
  export type AsObject = {
    guid: string,
    surveyName: string,
    surveyDescription: string,
    organizationName: string,
    completionType: pando_api_survey_v1_shared_pb.CompletionType,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateCompleted?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    totalAnswered: number,
    totalQuestions: number,
  }
}

export class InstanceShortList extends jspb.Message {
  getInstancesList(): Array<InstanceShort>;
  setInstancesList(value: Array<InstanceShort>): InstanceShortList;
  clearInstancesList(): InstanceShortList;
  addInstances(value?: InstanceShort, index?: number): InstanceShort;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): InstanceShortList;
  hasPagination(): boolean;
  clearPagination(): InstanceShortList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InstanceShortList.AsObject;
  static toObject(includeInstance: boolean, msg: InstanceShortList): InstanceShortList.AsObject;
  static serializeBinaryToWriter(message: InstanceShortList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InstanceShortList;
  static deserializeBinaryFromReader(message: InstanceShortList, reader: jspb.BinaryReader): InstanceShortList;
}

export namespace InstanceShortList {
  export type AsObject = {
    instancesList: Array<InstanceShort.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

