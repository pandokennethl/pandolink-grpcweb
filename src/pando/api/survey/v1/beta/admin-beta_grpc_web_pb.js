/**
 * @fileoverview gRPC-Web generated client stub for pando.api.survey.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('../../../../../google/api/annotations_pb.js')

var google_protobuf_field_mask_pb = require('google-protobuf/google/protobuf/field_mask_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')

var pando_api_extensions_pb = require('../../../../../pando/api/extensions_pb.js')

var pando_api_shared_pb = require('../../../../../pando/api/shared_pb.js')

var pando_api_survey_v1_shared_pb = require('../../../../../pando/api/survey/v1/shared_pb.js')

var pando_api_survey_v1_admin_pb = require('../../../../../pando/api/survey/v1/admin_pb.js')
const proto = {};
proto.pando = {};
proto.pando.api = {};
proto.pando.api.survey = {};
proto.pando.api.survey.v1 = require('./admin-beta_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetSurveysByESignTemplateRequest,
 *   !proto.pando.api.survey.v1.SurveyShortList>}
 */
const methodDescriptor_SurveyAdminBetaService_GetSurveysByESignTemplate = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByESignTemplate',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.GetSurveysByESignTemplateRequest,
  proto.pando.api.survey.v1.SurveyShortList,
  /**
   * @param {!proto.pando.api.survey.v1.GetSurveysByESignTemplateRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.SurveyShortList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysByESignTemplateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyShortList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyShortList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.getSurveysByESignTemplate =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByESignTemplate',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveysByESignTemplate,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysByESignTemplateRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyShortList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.getSurveysByESignTemplate =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByESignTemplate',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveysByESignTemplate);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.PublishSurveyRequest,
 *   !proto.pando.api.survey.v1.Survey>}
 */
const methodDescriptor_SurveyAdminBetaService_ExecPublishSurvey = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/ExecPublishSurvey',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.PublishSurveyRequest,
  pando_api_survey_v1_shared_pb.Survey,
  /**
   * @param {!proto.pando.api.survey.v1.PublishSurveyRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  pando_api_survey_v1_shared_pb.Survey.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.PublishSurveyRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.Survey)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.Survey>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.execPublishSurvey =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/ExecPublishSurvey',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_ExecPublishSurvey,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.PublishSurveyRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.Survey>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.execPublishSurvey =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/ExecPublishSurvey',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_ExecPublishSurvey);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.CreateInstanceBetaRequest,
 *   !proto.pando.api.survey.v1.CreateInstanceResponse>}
 */
const methodDescriptor_SurveyAdminBetaService_CreateInstance = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/CreateInstance',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.CreateInstanceBetaRequest,
  proto.pando.api.survey.v1.CreateInstanceResponse,
  /**
   * @param {!proto.pando.api.survey.v1.CreateInstanceBetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.CreateInstanceResponse.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.CreateInstanceBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.CreateInstanceResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.CreateInstanceResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.createInstance =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/CreateInstance',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_CreateInstance,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.CreateInstanceBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.CreateInstanceResponse>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.createInstance =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/CreateInstance',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_CreateInstance);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetSurveysByVariableRequest,
 *   !proto.pando.api.survey.v1.SurveyShortList>}
 */
const methodDescriptor_SurveyAdminBetaService_GetSurveysByVariable = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByVariable',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.GetSurveysByVariableRequest,
  proto.pando.api.survey.v1.SurveyShortList,
  /**
   * @param {!proto.pando.api.survey.v1.GetSurveysByVariableRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.SurveyShortList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysByVariableRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyShortList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyShortList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.getSurveysByVariable =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByVariable',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveysByVariable,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysByVariableRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyShortList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.getSurveysByVariable =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveysByVariable',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveysByVariable);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.SurveyVariable,
 *   !proto.pando.api.survey.v1.SurveyVariable>}
 */
const methodDescriptor_SurveyAdminBetaService_UpdateSurveyVariable = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/UpdateSurveyVariable',
  grpc.web.MethodType.UNARY,
  pando_api_survey_v1_admin_pb.SurveyVariable,
  pando_api_survey_v1_admin_pb.SurveyVariable,
  /**
   * @param {!proto.pando.api.survey.v1.SurveyVariable} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  pando_api_survey_v1_admin_pb.SurveyVariable.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.SurveyVariable} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyVariable)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyVariable>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.updateSurveyVariable =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/UpdateSurveyVariable',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_UpdateSurveyVariable,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.SurveyVariable} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyVariable>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.updateSurveyVariable =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/UpdateSurveyVariable',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_UpdateSurveyVariable);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetSurveyVariablesRequest,
 *   !proto.pando.api.survey.v1.SurveyVariableDetailList>}
 */
const methodDescriptor_SurveyAdminBetaService_GetSurveyVariables = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveyVariables',
  grpc.web.MethodType.UNARY,
  pando_api_survey_v1_admin_pb.GetSurveyVariablesRequest,
  proto.pando.api.survey.v1.SurveyVariableDetailList,
  /**
   * @param {!proto.pando.api.survey.v1.GetSurveyVariablesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.SurveyVariableDetailList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetSurveyVariablesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyVariableDetailList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyVariableDetailList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.getSurveyVariables =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveyVariables',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveyVariables,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetSurveyVariablesRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyVariableDetailList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.getSurveyVariables =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveyVariables',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveyVariables);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pando.api.survey.v1.GetSurveysBetaRequest,
 *   !proto.pando.api.survey.v1.SurveyBasicList>}
 */
const methodDescriptor_SurveyAdminBetaService_GetSurveys = new grpc.web.MethodDescriptor(
  '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveys',
  grpc.web.MethodType.UNARY,
  proto.pando.api.survey.v1.GetSurveysBetaRequest,
  proto.pando.api.survey.v1.SurveyBasicList,
  /**
   * @param {!proto.pando.api.survey.v1.GetSurveysBetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pando.api.survey.v1.SurveyBasicList.deserializeBinary
);


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.pando.api.survey.v1.SurveyBasicList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pando.api.survey.v1.SurveyBasicList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pando.api.survey.v1.SurveyAdminBetaServiceClient.prototype.getSurveys =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveys',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveys,
      callback);
};


/**
 * @param {!proto.pando.api.survey.v1.GetSurveysBetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pando.api.survey.v1.SurveyBasicList>}
 *     Promise that resolves to the response
 */
proto.pando.api.survey.v1.SurveyAdminBetaServicePromiseClient.prototype.getSurveys =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pando.api.survey.v1.SurveyAdminBetaService/GetSurveys',
      request,
      metadata || {},
      methodDescriptor_SurveyAdminBetaService_GetSurveys);
};


module.exports = proto.pando.api.survey.v1;

