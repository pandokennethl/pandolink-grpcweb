import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../../google/api/annotations_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../../pando/api/shared_pb';


export class GetReportInstancesRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetReportInstancesRequest;

  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetReportInstancesRequest;
  hasStart(): boolean;
  clearStart(): GetReportInstancesRequest;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetReportInstancesRequest;
  hasEnd(): boolean;
  clearEnd(): GetReportInstancesRequest;

  getIsCompletedOnly(): boolean;
  setIsCompletedOnly(value: boolean): GetReportInstancesRequest;

  getPage(): number;
  setPage(value: number): GetReportInstancesRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetReportInstancesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetReportInstancesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetReportInstancesRequest): GetReportInstancesRequest.AsObject;
  static serializeBinaryToWriter(message: GetReportInstancesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetReportInstancesRequest;
  static deserializeBinaryFromReader(message: GetReportInstancesRequest, reader: jspb.BinaryReader): GetReportInstancesRequest;
}

export namespace GetReportInstancesRequest {
  export type AsObject = {
    organizationCode: string,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    isCompletedOnly: boolean,
    page: number,
    resultsPerPage: number,
  }
}

export class ReportInstance extends jspb.Message {
  getGuid(): string;
  setGuid(value: string): ReportInstance;

  getOrganizationName(): string;
  setOrganizationName(value: string): ReportInstance;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): ReportInstance;
  hasDateCreated(): boolean;
  clearDateCreated(): ReportInstance;

  getContactInfo(): string;
  setContactInfo(value: string): ReportInstance;

  getRespondentName(): string;
  setRespondentName(value: string): ReportInstance;

  getDateComplete(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateComplete(value?: google_protobuf_timestamp_pb.Timestamp): ReportInstance;
  hasDateComplete(): boolean;
  clearDateComplete(): ReportInstance;

  getSurveyName(): string;
  setSurveyName(value: string): ReportInstance;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReportInstance.AsObject;
  static toObject(includeInstance: boolean, msg: ReportInstance): ReportInstance.AsObject;
  static serializeBinaryToWriter(message: ReportInstance, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReportInstance;
  static deserializeBinaryFromReader(message: ReportInstance, reader: jspb.BinaryReader): ReportInstance;
}

export namespace ReportInstance {
  export type AsObject = {
    guid: string,
    organizationName: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    contactInfo: string,
    respondentName: string,
    dateComplete?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    surveyName: string,
  }
}

export class ReportInstanceList extends jspb.Message {
  getInstancesList(): Array<ReportInstance>;
  setInstancesList(value: Array<ReportInstance>): ReportInstanceList;
  clearInstancesList(): ReportInstanceList;
  addInstances(value?: ReportInstance, index?: number): ReportInstance;

  getPagination(): pando_api_shared_pb.PaginationResult | undefined;
  setPagination(value?: pando_api_shared_pb.PaginationResult): ReportInstanceList;
  hasPagination(): boolean;
  clearPagination(): ReportInstanceList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReportInstanceList.AsObject;
  static toObject(includeInstance: boolean, msg: ReportInstanceList): ReportInstanceList.AsObject;
  static serializeBinaryToWriter(message: ReportInstanceList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReportInstanceList;
  static deserializeBinaryFromReader(message: ReportInstanceList, reader: jspb.BinaryReader): ReportInstanceList;
}

export namespace ReportInstanceList {
  export type AsObject = {
    instancesList: Array<ReportInstance.AsObject>,
    pagination?: pando_api_shared_pb.PaginationResult.AsObject,
  }
}

export class GetSurveyCompletionStatusesRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetSurveyCompletionStatusesRequest;

  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetSurveyCompletionStatusesRequest;
  hasStart(): boolean;
  clearStart(): GetSurveyCompletionStatusesRequest;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetSurveyCompletionStatusesRequest;
  hasEnd(): boolean;
  clearEnd(): GetSurveyCompletionStatusesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSurveyCompletionStatusesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSurveyCompletionStatusesRequest): GetSurveyCompletionStatusesRequest.AsObject;
  static serializeBinaryToWriter(message: GetSurveyCompletionStatusesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSurveyCompletionStatusesRequest;
  static deserializeBinaryFromReader(message: GetSurveyCompletionStatusesRequest, reader: jspb.BinaryReader): GetSurveyCompletionStatusesRequest;
}

export namespace GetSurveyCompletionStatusesRequest {
  export type AsObject = {
    organizationCode: string,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class SurveyCompletionStatus extends jspb.Message {
  getSurveyName(): string;
  setSurveyName(value: string): SurveyCompletionStatus;

  getCompleteCount(): number;
  setCompleteCount(value: number): SurveyCompletionStatus;

  getIncompleteCount(): number;
  setIncompleteCount(value: number): SurveyCompletionStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyCompletionStatus.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyCompletionStatus): SurveyCompletionStatus.AsObject;
  static serializeBinaryToWriter(message: SurveyCompletionStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyCompletionStatus;
  static deserializeBinaryFromReader(message: SurveyCompletionStatus, reader: jspb.BinaryReader): SurveyCompletionStatus;
}

export namespace SurveyCompletionStatus {
  export type AsObject = {
    surveyName: string,
    completeCount: number,
    incompleteCount: number,
  }
}

export class SurveyCompletionStatusList extends jspb.Message {
  getSurveyCompletionStatusesList(): Array<SurveyCompletionStatus>;
  setSurveyCompletionStatusesList(value: Array<SurveyCompletionStatus>): SurveyCompletionStatusList;
  clearSurveyCompletionStatusesList(): SurveyCompletionStatusList;
  addSurveyCompletionStatuses(value?: SurveyCompletionStatus, index?: number): SurveyCompletionStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SurveyCompletionStatusList.AsObject;
  static toObject(includeInstance: boolean, msg: SurveyCompletionStatusList): SurveyCompletionStatusList.AsObject;
  static serializeBinaryToWriter(message: SurveyCompletionStatusList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SurveyCompletionStatusList;
  static deserializeBinaryFromReader(message: SurveyCompletionStatusList, reader: jspb.BinaryReader): SurveyCompletionStatusList;
}

export namespace SurveyCompletionStatusList {
  export type AsObject = {
    surveyCompletionStatusesList: Array<SurveyCompletionStatus.AsObject>,
  }
}

