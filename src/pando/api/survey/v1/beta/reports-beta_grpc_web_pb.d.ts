import * as grpcWeb from 'grpc-web';

import * as pando_api_survey_v1_beta_reports$beta_pb from '../../../../../pando/api/survey/v1/beta/reports-beta_pb';


export class SurveyReportBetaServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getSurveyInstances(
    request: pando_api_survey_v1_beta_reports$beta_pb.GetReportInstancesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_reports$beta_pb.ReportInstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_reports$beta_pb.ReportInstanceList>;

  getSurveyCompletionStatuses(
    request: pando_api_survey_v1_beta_reports$beta_pb.GetSurveyCompletionStatusesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_beta_reports$beta_pb.SurveyCompletionStatusList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_beta_reports$beta_pb.SurveyCompletionStatusList>;

}

export class SurveyReportBetaServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getSurveyInstances(
    request: pando_api_survey_v1_beta_reports$beta_pb.GetReportInstancesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_reports$beta_pb.ReportInstanceList>;

  getSurveyCompletionStatuses(
    request: pando_api_survey_v1_beta_reports$beta_pb.GetSurveyCompletionStatusesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_beta_reports$beta_pb.SurveyCompletionStatusList>;

}

