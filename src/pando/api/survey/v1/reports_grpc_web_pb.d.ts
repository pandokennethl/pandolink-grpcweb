import * as grpcWeb from 'grpc-web';

import * as pando_api_survey_v1_reports_pb from '../../../../pando/api/survey/v1/reports_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../pando/api/survey/v1/shared_pb';


export class SurveyReportServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getSurveyInstanceStatuses(
    request: pando_api_survey_v1_reports_pb.GetSurveyInstanceStatusesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_reports_pb.SurveyInstancesStatusList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_reports_pb.SurveyInstancesStatusList>;

}

export class SurveyReportServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_reports_pb.GetInstancesReportRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getSurveyInstanceStatuses(
    request: pando_api_survey_v1_reports_pb.GetSurveyInstanceStatusesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_reports_pb.SurveyInstancesStatusList>;

}

