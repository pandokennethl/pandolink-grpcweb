import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../pando/api/survey/v1/shared_pb';
import * as pando_api_survey_v1_admin_pb from '../../../../pando/api/survey/v1/admin_pb';


export class SurveyAdminServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createSurvey(
    request: pando_api_survey_v1_shared_pb.Survey,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Survey) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Survey>;

  createQuestion(
    request: pando_api_survey_v1_shared_pb.Question,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Question) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Question>;

  createPlaybackData(
    request: pando_api_survey_v1_shared_pb.PlaybackData,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.PlaybackData) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.PlaybackData>;

  createSurveyQuestionDestination(
    request: pando_api_survey_v1_shared_pb.SurveyQuestionDestination,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyQuestionDestination) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  createSurveyVariable(
    request: pando_api_survey_v1_admin_pb.SurveyVariable,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.SurveyVariable) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.SurveyVariable>;

  createVariable(
    request: pando_api_survey_v1_shared_pb.Variable,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Variable) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Variable>;

  createVariables(
    request: pando_api_survey_v1_admin_pb.CreateVariablesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.VariableList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.VariableList>;

  createInstance(
    request: pando_api_survey_v1_admin_pb.CreateInstanceRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Instance) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Instance>;

  createAnswerOption(
    request: pando_api_survey_v1_shared_pb.AnswerOption,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.AnswerOption) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.AnswerOption>;

  createSurveyVersion(
    request: pando_api_survey_v1_admin_pb.CreateSurveyVersionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Survey) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Survey>;

  getSurvey(
    request: pando_api_survey_v1_admin_pb.GetSurveyRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Survey) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Survey>;

  getSurveys(
    request: pando_api_survey_v1_admin_pb.GetSurveysRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyDetailList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyDetailList>;

  getQuestion(
    request: pando_api_survey_v1_admin_pb.GetQuestionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Question) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Question>;

  getQuestions(
    request: pando_api_survey_v1_admin_pb.GetQuestionsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyQuestionList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyQuestionList>;

  getAnswerOption(
    request: pando_api_survey_v1_admin_pb.GetAnswerOptionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.AnswerOption) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.AnswerOption>;

  getAnswerOptions(
    request: pando_api_survey_v1_admin_pb.GetAnswerOptionsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.AnswerOptionList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.AnswerOptionList>;

  getPlaybackDataList(
    request: pando_api_survey_v1_admin_pb.GetPlaybackDataListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.PlaybackDataList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.PlaybackDataList>;

  getSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.GetSurveyQuestionDestinationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyQuestionDestination) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  getSurveyQuestionDestinations(
    request: pando_api_survey_v1_admin_pb.GetSurveyQuestionDestinationsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyQuestionDestinationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyQuestionDestinationList>;

  getVariable(
    request: pando_api_survey_v1_admin_pb.GetVariableRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Variable) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Variable>;

  getVariables(
    request: pando_api_survey_v1_admin_pb.GetVariablesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.VariableList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.VariableList>;

  getSurveyVariables(
    request: pando_api_survey_v1_admin_pb.GetSurveyVariablesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.VariableList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.VariableList>;

  getSurveyMediaList(
    request: pando_api_survey_v1_admin_pb.GetSurveyMediaListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.MediaList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.MediaList>;

  getVariableMediaList(
    request: pando_api_survey_v1_admin_pb.GetVariableMediaListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.MediaList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.MediaList>;

  getSurveyVersions(
    request: pando_api_survey_v1_admin_pb.GetSurveyVersionsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyList>;

  getSurveyDocuments(
    request: pando_api_survey_v1_admin_pb.GetSurveyDocumentsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyDocumentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyDocumentList>;

  getVariableTemplate(
    request: pando_api_survey_v1_admin_pb.GetVariableTemplateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.GetVariableTemplateResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.GetVariableTemplateResponse>;

  updateSurvey(
    request: pando_api_survey_v1_admin_pb.UpdateSurveyRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Survey) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Survey>;

  updateQuestion(
    request: pando_api_survey_v1_admin_pb.UpdateQuestionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Question) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Question>;

  updatePlaybackData(
    request: pando_api_survey_v1_admin_pb.UpdatePlaybackDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.PlaybackData) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.PlaybackData>;

  updateSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.UpdateSurveyQuestionDestinationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.SurveyQuestionDestination) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  updateVariable(
    request: pando_api_survey_v1_admin_pb.UpdateVariableRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Variable) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Variable>;

  updateAnswerOption(
    request: pando_api_survey_v1_admin_pb.UpdateAnswerOptionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.AnswerOption) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.AnswerOption>;

  deleteSurvey(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteQuestion(
    request: pando_api_survey_v1_admin_pb.DeleteQuestionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deletePlaybackData(
    request: pando_api_survey_v1_admin_pb.DeletePlaybackDataRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteInstance(
    request: pando_api_survey_v1_admin_pb.DeleteInstanceRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteMedia(
    request: pando_api_survey_v1_admin_pb.DeleteMediaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteAnswerOption(
    request: pando_api_survey_v1_admin_pb.DeleteAnswerOptionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteSurveyMedia(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyMediaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteSurveyVariable(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyVariableRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteVariableMedia(
    request: pando_api_survey_v1_admin_pb.DeleteVariableMediaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyQuestionDestinationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteSurveyVersion(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyVersionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteVariable(
    request: pando_api_survey_v1_admin_pb.DeleteVariableRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execAddSurveyESignTemplate(
    request: pando_api_survey_v1_admin_pb.SurveyESignTemplate,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.SurveyESignTemplate) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.SurveyESignTemplate>;

  execRemoveSurveyESignTemplate(
    request: pando_api_survey_v1_admin_pb.SurveyESignTemplate,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execSendInstanceLink(
    request: pando_api_survey_v1_admin_pb.SendInstanceLink,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.SendInstanceLink) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.SendInstanceLink>;

  execAddSurveyInstanceESignSignatory(
    request: pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory>;

  execRemoveSurveyInstanceESignSignatory(
    request: pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

}

export class SurveyAdminServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createSurvey(
    request: pando_api_survey_v1_shared_pb.Survey,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Survey>;

  createQuestion(
    request: pando_api_survey_v1_shared_pb.Question,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Question>;

  createPlaybackData(
    request: pando_api_survey_v1_shared_pb.PlaybackData,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.PlaybackData>;

  createSurveyQuestionDestination(
    request: pando_api_survey_v1_shared_pb.SurveyQuestionDestination,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  createSurveyVariable(
    request: pando_api_survey_v1_admin_pb.SurveyVariable,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.SurveyVariable>;

  createVariable(
    request: pando_api_survey_v1_shared_pb.Variable,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Variable>;

  createVariables(
    request: pando_api_survey_v1_admin_pb.CreateVariablesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.VariableList>;

  createInstance(
    request: pando_api_survey_v1_admin_pb.CreateInstanceRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Instance>;

  createAnswerOption(
    request: pando_api_survey_v1_shared_pb.AnswerOption,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.AnswerOption>;

  createSurveyVersion(
    request: pando_api_survey_v1_admin_pb.CreateSurveyVersionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Survey>;

  getSurvey(
    request: pando_api_survey_v1_admin_pb.GetSurveyRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Survey>;

  getSurveys(
    request: pando_api_survey_v1_admin_pb.GetSurveysRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyDetailList>;

  getQuestion(
    request: pando_api_survey_v1_admin_pb.GetQuestionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Question>;

  getQuestions(
    request: pando_api_survey_v1_admin_pb.GetQuestionsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyQuestionList>;

  getAnswerOption(
    request: pando_api_survey_v1_admin_pb.GetAnswerOptionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.AnswerOption>;

  getAnswerOptions(
    request: pando_api_survey_v1_admin_pb.GetAnswerOptionsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.AnswerOptionList>;

  getPlaybackDataList(
    request: pando_api_survey_v1_admin_pb.GetPlaybackDataListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.PlaybackDataList>;

  getSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.GetSurveyQuestionDestinationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  getSurveyQuestionDestinations(
    request: pando_api_survey_v1_admin_pb.GetSurveyQuestionDestinationsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyQuestionDestinationList>;

  getVariable(
    request: pando_api_survey_v1_admin_pb.GetVariableRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Variable>;

  getVariables(
    request: pando_api_survey_v1_admin_pb.GetVariablesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.VariableList>;

  getSurveyVariables(
    request: pando_api_survey_v1_admin_pb.GetSurveyVariablesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.VariableList>;

  getSurveyMediaList(
    request: pando_api_survey_v1_admin_pb.GetSurveyMediaListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.MediaList>;

  getVariableMediaList(
    request: pando_api_survey_v1_admin_pb.GetVariableMediaListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.MediaList>;

  getSurveyVersions(
    request: pando_api_survey_v1_admin_pb.GetSurveyVersionsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyList>;

  getSurveyDocuments(
    request: pando_api_survey_v1_admin_pb.GetSurveyDocumentsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyDocumentList>;

  getVariableTemplate(
    request: pando_api_survey_v1_admin_pb.GetVariableTemplateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.GetVariableTemplateResponse>;

  updateSurvey(
    request: pando_api_survey_v1_admin_pb.UpdateSurveyRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Survey>;

  updateQuestion(
    request: pando_api_survey_v1_admin_pb.UpdateQuestionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Question>;

  updatePlaybackData(
    request: pando_api_survey_v1_admin_pb.UpdatePlaybackDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.PlaybackData>;

  updateSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.UpdateSurveyQuestionDestinationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.SurveyQuestionDestination>;

  updateVariable(
    request: pando_api_survey_v1_admin_pb.UpdateVariableRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Variable>;

  updateAnswerOption(
    request: pando_api_survey_v1_admin_pb.UpdateAnswerOptionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.AnswerOption>;

  deleteSurvey(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteQuestion(
    request: pando_api_survey_v1_admin_pb.DeleteQuestionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deletePlaybackData(
    request: pando_api_survey_v1_admin_pb.DeletePlaybackDataRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteInstance(
    request: pando_api_survey_v1_admin_pb.DeleteInstanceRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteMedia(
    request: pando_api_survey_v1_admin_pb.DeleteMediaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteAnswerOption(
    request: pando_api_survey_v1_admin_pb.DeleteAnswerOptionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteSurveyMedia(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyMediaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteSurveyVariable(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyVariableRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteVariableMedia(
    request: pando_api_survey_v1_admin_pb.DeleteVariableMediaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteSurveyQuestionDestination(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyQuestionDestinationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteSurveyVersion(
    request: pando_api_survey_v1_admin_pb.DeleteSurveyVersionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteVariable(
    request: pando_api_survey_v1_admin_pb.DeleteVariableRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execAddSurveyESignTemplate(
    request: pando_api_survey_v1_admin_pb.SurveyESignTemplate,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.SurveyESignTemplate>;

  execRemoveSurveyESignTemplate(
    request: pando_api_survey_v1_admin_pb.SurveyESignTemplate,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execSendInstanceLink(
    request: pando_api_survey_v1_admin_pb.SendInstanceLink,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.SendInstanceLink>;

  execAddSurveyInstanceESignSignatory(
    request: pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory>;

  execRemoveSurveyInstanceESignSignatory(
    request: pando_api_survey_v1_admin_pb.SurveyInstanceESignSignatory,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

}

