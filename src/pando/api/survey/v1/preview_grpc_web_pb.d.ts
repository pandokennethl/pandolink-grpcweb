import * as grpcWeb from 'grpc-web';

import * as pando_api_survey_v1_preview_pb from '../../../../pando/api/survey/v1/preview_pb';


export class PreviewServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getNextQuestionPreview(
    request: pando_api_survey_v1_preview_pb.GetNextQuestionPreviewRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_preview_pb.GetNextQuestionPreviewResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_preview_pb.GetNextQuestionPreviewResponse>;

}

export class PreviewServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getNextQuestionPreview(
    request: pando_api_survey_v1_preview_pb.GetNextQuestionPreviewRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_preview_pb.GetNextQuestionPreviewResponse>;

}

