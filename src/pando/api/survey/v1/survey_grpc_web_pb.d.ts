import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_survey_v1_shared_pb from '../../../../pando/api/survey/v1/shared_pb';
import * as pando_api_survey_v1_survey_pb from '../../../../pando/api/survey/v1/survey_pb';


export class SurveyServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createAnswer(
    request: pando_api_survey_v1_shared_pb.Answer,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Answer) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Answer>;

  createAttempt(
    request: pando_api_survey_v1_shared_pb.Attempt,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Attempt) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Attempt>;

  getInstance(
    request: pando_api_survey_v1_survey_pb.GetInstanceRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Instance) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Instance>;

  getInstances(
    request: pando_api_survey_v1_survey_pb.GetInstancesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getAttempt(
    request: pando_api_survey_v1_survey_pb.GetAttemptRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Attempt) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Attempt>;

  getAttempts(
    request: pando_api_survey_v1_survey_pb.GetAttemptsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.AttemptList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.AttemptList>;

  getSurveyMedia(
    request: pando_api_survey_v1_survey_pb.GetMediaRequest,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.FileChunk>;

  getAnswerMedia(
    request: pando_api_survey_v1_survey_pb.GetMediaRequest,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.FileChunk>;

  getNextQuestion(
    request: pando_api_survey_v1_survey_pb.GetNextQuestionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_survey_pb.GetNextQuestionResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_survey_pb.GetNextQuestionResponse>;

  saveAnswer(
    request: pando_api_survey_v1_survey_pb.SaveAnswerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Answer) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Answer>;

  execCompleteSurvey(
    request: pando_api_survey_v1_survey_pb.CompleteSurveyRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.Attempt) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.Attempt>;

  execFacialRecognition(
    request: pando_api_survey_v1_survey_pb.FacialRecognitionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_survey_pb.FacialRecognitionResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_survey_pb.FacialRecognitionResponse>;

  execCompleteQuestion(
    request: pando_api_survey_v1_survey_pb.ExecCompleteQuestionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_survey_pb.ExecCompleteQuestionResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_survey_pb.ExecCompleteQuestionResponse>;

  execAddInstanceOwner(
    request: pando_api_survey_v1_survey_pb.ExecAddInstanceOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execRemoveInstanceOwner(
    request: pando_api_survey_v1_survey_pb.ExecRemoveInstanceOwnerRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getSurveyInstancesToDo(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_shared_pb.InstanceList) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_shared_pb.InstanceList>;

  getAttemptQuestions(
    request: pando_api_survey_v1_survey_pb.GetAttemptQuestionsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_survey_pb.GetAttemptQuestionsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_survey_pb.GetAttemptQuestionsResponse>;

  getQuestionWithAnswerTimestamps(
    request: pando_api_survey_v1_survey_pb.GetQuestionWithAnswerTimestampsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_survey_v1_survey_pb.GetQuestionWithAnswerTimestampsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_survey_v1_survey_pb.GetQuestionWithAnswerTimestampsResponse>;

}

export class SurveyServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createAnswer(
    request: pando_api_survey_v1_shared_pb.Answer,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Answer>;

  createAttempt(
    request: pando_api_survey_v1_shared_pb.Attempt,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Attempt>;

  getInstance(
    request: pando_api_survey_v1_survey_pb.GetInstanceRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Instance>;

  getInstances(
    request: pando_api_survey_v1_survey_pb.GetInstancesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getAttempt(
    request: pando_api_survey_v1_survey_pb.GetAttemptRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Attempt>;

  getAttempts(
    request: pando_api_survey_v1_survey_pb.GetAttemptsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.AttemptList>;

  getSurveyMedia(
    request: pando_api_survey_v1_survey_pb.GetMediaRequest,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.FileChunk>;

  getAnswerMedia(
    request: pando_api_survey_v1_survey_pb.GetMediaRequest,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.FileChunk>;

  getNextQuestion(
    request: pando_api_survey_v1_survey_pb.GetNextQuestionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_survey_pb.GetNextQuestionResponse>;

  saveAnswer(
    request: pando_api_survey_v1_survey_pb.SaveAnswerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Answer>;

  execCompleteSurvey(
    request: pando_api_survey_v1_survey_pb.CompleteSurveyRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.Attempt>;

  execFacialRecognition(
    request: pando_api_survey_v1_survey_pb.FacialRecognitionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_survey_pb.FacialRecognitionResponse>;

  execCompleteQuestion(
    request: pando_api_survey_v1_survey_pb.ExecCompleteQuestionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_survey_pb.ExecCompleteQuestionResponse>;

  execAddInstanceOwner(
    request: pando_api_survey_v1_survey_pb.ExecAddInstanceOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execRemoveInstanceOwner(
    request: pando_api_survey_v1_survey_pb.ExecRemoveInstanceOwnerRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getIncompleteSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getCompletedSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getRecentlyCompletedSurveyInstances(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getSurveyInstancesToDo(
    request: pando_api_survey_v1_survey_pb.GetSurveyInstanceListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_shared_pb.InstanceList>;

  getAttemptQuestions(
    request: pando_api_survey_v1_survey_pb.GetAttemptQuestionsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_survey_pb.GetAttemptQuestionsResponse>;

  getQuestionWithAnswerTimestamps(
    request: pando_api_survey_v1_survey_pb.GetQuestionWithAnswerTimestampsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_survey_v1_survey_pb.GetQuestionWithAnswerTimestampsResponse>;

}

