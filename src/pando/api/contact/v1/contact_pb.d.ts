import * as jspb from 'google-protobuf'

import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class Account extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): Account;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Account;

  getPrimaryContactGuid(): string;
  setPrimaryContactGuid(value: string): Account;

  getPrimaryAddressGuid(): string;
  setPrimaryAddressGuid(value: string): Account;

  getOrganizationAccountId(): string;
  setOrganizationAccountId(value: string): Account;

  getCreatedByUserId(): string;
  setCreatedByUserId(value: string): Account;

  getContactGuidsList(): Array<string>;
  setContactGuidsList(value: Array<string>): Account;
  clearContactGuidsList(): Account;
  addContactGuids(value: string, index?: number): Account;

  getAddressGuidsList(): Array<string>;
  setAddressGuidsList(value: Array<string>): Account;
  clearAddressGuidsList(): Account;
  addAddressGuids(value: string, index?: number): Account;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Account.AsObject;
  static toObject(includeInstance: boolean, msg: Account): Account.AsObject;
  static serializeBinaryToWriter(message: Account, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Account;
  static deserializeBinaryFromReader(message: Account, reader: jspb.BinaryReader): Account;
}

export namespace Account {
  export type AsObject = {
    accountGuid: string,
    organizationCode: string,
    primaryContactGuid: string,
    primaryAddressGuid: string,
    organizationAccountId: string,
    createdByUserId: string,
    contactGuidsList: Array<string>,
    addressGuidsList: Array<string>,
  }
}

export class Address extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): Address;

  getAddress1(): string;
  setAddress1(value: string): Address;

  getAddress2(): string;
  setAddress2(value: string): Address;

  getCity(): string;
  setCity(value: string): Address;

  getCounty(): string;
  setCounty(value: string): Address;

  getPostalCode(): string;
  setPostalCode(value: string): Address;

  getStateCode(): string;
  setStateCode(value: string): Address;

  getCountryCode(): string;
  setCountryCode(value: string): Address;

  getCreatedByUserId(): string;
  setCreatedByUserId(value: string): Address;

  getAddressVerificationList(): Array<AddressVerification>;
  setAddressVerificationList(value: Array<AddressVerification>): Address;
  clearAddressVerificationList(): Address;
  addAddressVerification(value?: AddressVerification, index?: number): AddressVerification;

  getAddressTypesList(): Array<AddressType>;
  setAddressTypesList(value: Array<AddressType>): Address;
  clearAddressTypesList(): Address;
  addAddressTypes(value?: AddressType, index?: number): AddressType;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Address;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Address.AsObject;
  static toObject(includeInstance: boolean, msg: Address): Address.AsObject;
  static serializeBinaryToWriter(message: Address, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Address;
  static deserializeBinaryFromReader(message: Address, reader: jspb.BinaryReader): Address;
}

export namespace Address {
  export type AsObject = {
    addressGuid: string,
    address1: string,
    address2: string,
    city: string,
    county: string,
    postalCode: string,
    stateCode: string,
    countryCode: string,
    createdByUserId: string,
    addressVerificationList: Array<AddressVerification.AsObject>,
    addressTypesList: Array<AddressType.AsObject>,
    organizationCode: string,
  }
}

export class AddressVerification extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): AddressVerification;

  getAddressVerificationType(): AddressVerificationType;
  setAddressVerificationType(value: AddressVerificationType): AddressVerification;

  getExternalId(): string;
  setExternalId(value: string): AddressVerification;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): AddressVerification;
  hasDateCreated(): boolean;
  clearDateCreated(): AddressVerification;

  getCreatedByUserId(): string;
  setCreatedByUserId(value: string): AddressVerification;

  getIsVerified(): boolean;
  setIsVerified(value: boolean): AddressVerification;

  getError(): string;
  setError(value: string): AddressVerification;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddressVerification.AsObject;
  static toObject(includeInstance: boolean, msg: AddressVerification): AddressVerification.AsObject;
  static serializeBinaryToWriter(message: AddressVerification, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddressVerification;
  static deserializeBinaryFromReader(message: AddressVerification, reader: jspb.BinaryReader): AddressVerification;
}

export namespace AddressVerification {
  export type AsObject = {
    addressGuid: string,
    addressVerificationType: AddressVerificationType,
    externalId: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdByUserId: string,
    isVerified: boolean,
    error: string,
  }
}

export class AddressType extends jspb.Message {
  getAddressTypeGuid(): string;
  setAddressTypeGuid(value: string): AddressType;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): AddressType;

  getName(): string;
  setName(value: string): AddressType;

  getDescription(): string;
  setDescription(value: string): AddressType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddressType.AsObject;
  static toObject(includeInstance: boolean, msg: AddressType): AddressType.AsObject;
  static serializeBinaryToWriter(message: AddressType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddressType;
  static deserializeBinaryFromReader(message: AddressType, reader: jspb.BinaryReader): AddressType;
}

export namespace AddressType {
  export type AsObject = {
    addressTypeGuid: string,
    organizationCode: string,
    name: string,
    description: string,
  }
}

export class Contact extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): Contact;

  getFirstName(): string;
  setFirstName(value: string): Contact;

  getLastName(): string;
  setLastName(value: string): Contact;

  getPreferredContactMethodGuid(): string;
  setPreferredContactMethodGuid(value: string): Contact;

  getCreatedByUserId(): string;
  setCreatedByUserId(value: string): Contact;

  getSecureDataMap(): jspb.Map<string, string>;
  clearSecureDataMap(): Contact;

  getContactMethodsList(): Array<ContactMethod>;
  setContactMethodsList(value: Array<ContactMethod>): Contact;
  clearContactMethodsList(): Contact;
  addContactMethods(value?: ContactMethod, index?: number): ContactMethod;

  getContactQualificationsList(): Array<ContactQualification>;
  setContactQualificationsList(value: Array<ContactQualification>): Contact;
  clearContactQualificationsList(): Contact;
  addContactQualifications(value?: ContactQualification, index?: number): ContactQualification;

  getContactTypesList(): Array<ContactType>;
  setContactTypesList(value: Array<ContactType>): Contact;
  clearContactTypesList(): Contact;
  addContactTypes(value?: ContactType, index?: number): ContactType;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Contact;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Contact.AsObject;
  static toObject(includeInstance: boolean, msg: Contact): Contact.AsObject;
  static serializeBinaryToWriter(message: Contact, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Contact;
  static deserializeBinaryFromReader(message: Contact, reader: jspb.BinaryReader): Contact;
}

export namespace Contact {
  export type AsObject = {
    contactGuid: string,
    firstName: string,
    lastName: string,
    preferredContactMethodGuid: string,
    createdByUserId: string,
    secureDataMap: Array<[string, string]>,
    contactMethodsList: Array<ContactMethod.AsObject>,
    contactQualificationsList: Array<ContactQualification.AsObject>,
    contactTypesList: Array<ContactType.AsObject>,
    organizationCode: string,
  }
}

export class ContactType extends jspb.Message {
  getContactTypeGuid(): string;
  setContactTypeGuid(value: string): ContactType;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): ContactType;

  getName(): string;
  setName(value: string): ContactType;

  getDescription(): string;
  setDescription(value: string): ContactType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactType.AsObject;
  static toObject(includeInstance: boolean, msg: ContactType): ContactType.AsObject;
  static serializeBinaryToWriter(message: ContactType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactType;
  static deserializeBinaryFromReader(message: ContactType, reader: jspb.BinaryReader): ContactType;
}

export namespace ContactType {
  export type AsObject = {
    contactTypeGuid: string,
    organizationCode: string,
    name: string,
    description: string,
  }
}

export class ContactMethod extends jspb.Message {
  getContactMethodGuid(): string;
  setContactMethodGuid(value: string): ContactMethod;

  getContactGuid(): string;
  setContactGuid(value: string): ContactMethod;

  getContactMethodType(): pando_api_shared_pb.ContactType;
  setContactMethodType(value: pando_api_shared_pb.ContactType): ContactMethod;

  getValue(): string;
  setValue(value: string): ContactMethod;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactMethod.AsObject;
  static toObject(includeInstance: boolean, msg: ContactMethod): ContactMethod.AsObject;
  static serializeBinaryToWriter(message: ContactMethod, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactMethod;
  static deserializeBinaryFromReader(message: ContactMethod, reader: jspb.BinaryReader): ContactMethod;
}

export namespace ContactMethod {
  export type AsObject = {
    contactMethodGuid: string,
    contactGuid: string,
    contactMethodType: pando_api_shared_pb.ContactType,
    value: string,
  }
}

export class ContactQualification extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): ContactQualification;

  getContactQualificationType(): ContactQualificationType;
  setContactQualificationType(value: ContactQualificationType): ContactQualification;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): ContactQualification;
  hasDateCreated(): boolean;
  clearDateCreated(): ContactQualification;

  getCreatedByUserId(): string;
  setCreatedByUserId(value: string): ContactQualification;

  getCreditScore(): number;
  setCreditScore(value: number): ContactQualification;

  getError(): string;
  setError(value: string): ContactQualification;

  getSecureDataMap(): jspb.Map<string, string>;
  clearSecureDataMap(): ContactQualification;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactQualification.AsObject;
  static toObject(includeInstance: boolean, msg: ContactQualification): ContactQualification.AsObject;
  static serializeBinaryToWriter(message: ContactQualification, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactQualification;
  static deserializeBinaryFromReader(message: ContactQualification, reader: jspb.BinaryReader): ContactQualification;
}

export namespace ContactQualification {
  export type AsObject = {
    contactGuid: string,
    contactQualificationType: ContactQualificationType,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdByUserId: string,
    creditScore: number,
    error: string,
    secureDataMap: Array<[string, string]>,
  }
}

export class Country extends jspb.Message {
  getCode(): string;
  setCode(value: string): Country;

  getName(): string;
  setName(value: string): Country;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Country.AsObject;
  static toObject(includeInstance: boolean, msg: Country): Country.AsObject;
  static serializeBinaryToWriter(message: Country, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Country;
  static deserializeBinaryFromReader(message: Country, reader: jspb.BinaryReader): Country;
}

export namespace Country {
  export type AsObject = {
    code: string,
    name: string,
  }
}

export class State extends jspb.Message {
  getCode(): string;
  setCode(value: string): State;

  getName(): string;
  setName(value: string): State;

  getCountryCode(): string;
  setCountryCode(value: string): State;

  getCountryName(): string;
  setCountryName(value: string): State;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): State.AsObject;
  static toObject(includeInstance: boolean, msg: State): State.AsObject;
  static serializeBinaryToWriter(message: State, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): State;
  static deserializeBinaryFromReader(message: State, reader: jspb.BinaryReader): State;
}

export namespace State {
  export type AsObject = {
    code: string,
    name: string,
    countryCode: string,
    countryName: string,
  }
}

export class GetAccountRequest extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): GetAccountRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAccountRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAccountRequest): GetAccountRequest.AsObject;
  static serializeBinaryToWriter(message: GetAccountRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAccountRequest;
  static deserializeBinaryFromReader(message: GetAccountRequest, reader: jspb.BinaryReader): GetAccountRequest;
}

export namespace GetAccountRequest {
  export type AsObject = {
    accountGuid: string,
  }
}

export class GetAddressRequest extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): GetAddressRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAddressRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAddressRequest): GetAddressRequest.AsObject;
  static serializeBinaryToWriter(message: GetAddressRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAddressRequest;
  static deserializeBinaryFromReader(message: GetAddressRequest, reader: jspb.BinaryReader): GetAddressRequest;
}

export namespace GetAddressRequest {
  export type AsObject = {
    addressGuid: string,
  }
}

export class GetContactRequest extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): GetContactRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetContactRequest): GetContactRequest.AsObject;
  static serializeBinaryToWriter(message: GetContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetContactRequest;
  static deserializeBinaryFromReader(message: GetContactRequest, reader: jspb.BinaryReader): GetContactRequest;
}

export namespace GetContactRequest {
  export type AsObject = {
    contactGuid: string,
  }
}

export class GetAccountsRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): GetAccountsRequest;

  getStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStart(value?: google_protobuf_timestamp_pb.Timestamp): GetAccountsRequest;
  hasStart(): boolean;
  clearStart(): GetAccountsRequest;

  getEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEnd(value?: google_protobuf_timestamp_pb.Timestamp): GetAccountsRequest;
  hasEnd(): boolean;
  clearEnd(): GetAccountsRequest;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetAccountsRequest;

  getFilter(): string;
  setFilter(value: string): GetAccountsRequest;

  getPage(): number;
  setPage(value: number): GetAccountsRequest;

  getResultsPerPage(): number;
  setResultsPerPage(value: number): GetAccountsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAccountsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAccountsRequest): GetAccountsRequest.AsObject;
  static serializeBinaryToWriter(message: GetAccountsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAccountsRequest;
  static deserializeBinaryFromReader(message: GetAccountsRequest, reader: jspb.BinaryReader): GetAccountsRequest;
}

export namespace GetAccountsRequest {
  export type AsObject = {
    userId: string,
    start?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    end?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    organizationCode: string,
    filter: string,
    page: number,
    resultsPerPage: number,
  }
}

export class GetAddressesRequest extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): GetAddressesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAddressesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAddressesRequest): GetAddressesRequest.AsObject;
  static serializeBinaryToWriter(message: GetAddressesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAddressesRequest;
  static deserializeBinaryFromReader(message: GetAddressesRequest, reader: jspb.BinaryReader): GetAddressesRequest;
}

export namespace GetAddressesRequest {
  export type AsObject = {
    accountGuid: string,
  }
}

export class GetContactsRequest extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): GetContactsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetContactsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetContactsRequest): GetContactsRequest.AsObject;
  static serializeBinaryToWriter(message: GetContactsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetContactsRequest;
  static deserializeBinaryFromReader(message: GetContactsRequest, reader: jspb.BinaryReader): GetContactsRequest;
}

export namespace GetContactsRequest {
  export type AsObject = {
    accountGuid: string,
  }
}

export class UpdateAccountRequest extends jspb.Message {
  getAccount(): Account | undefined;
  setAccount(value?: Account): UpdateAccountRequest;
  hasAccount(): boolean;
  clearAccount(): UpdateAccountRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateAccountRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateAccountRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateAccountRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateAccountRequest): UpdateAccountRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateAccountRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateAccountRequest;
  static deserializeBinaryFromReader(message: UpdateAccountRequest, reader: jspb.BinaryReader): UpdateAccountRequest;
}

export namespace UpdateAccountRequest {
  export type AsObject = {
    account?: Account.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class UpdateContactRequest extends jspb.Message {
  getContact(): Contact | undefined;
  setContact(value?: Contact): UpdateContactRequest;
  hasContact(): boolean;
  clearContact(): UpdateContactRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateContactRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateContactRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateContactRequest): UpdateContactRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateContactRequest;
  static deserializeBinaryFromReader(message: UpdateContactRequest, reader: jspb.BinaryReader): UpdateContactRequest;
}

export namespace UpdateContactRequest {
  export type AsObject = {
    contact?: Contact.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteAccountRequest extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): DeleteAccountRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteAccountRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteAccountRequest): DeleteAccountRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteAccountRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteAccountRequest;
  static deserializeBinaryFromReader(message: DeleteAccountRequest, reader: jspb.BinaryReader): DeleteAccountRequest;
}

export namespace DeleteAccountRequest {
  export type AsObject = {
    accountGuid: string,
  }
}

export class DeleteAddressRequest extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): DeleteAddressRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteAddressRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteAddressRequest): DeleteAddressRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteAddressRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteAddressRequest;
  static deserializeBinaryFromReader(message: DeleteAddressRequest, reader: jspb.BinaryReader): DeleteAddressRequest;
}

export namespace DeleteAddressRequest {
  export type AsObject = {
    addressGuid: string,
  }
}

export class DeleteContactRequest extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): DeleteContactRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteContactRequest): DeleteContactRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteContactRequest;
  static deserializeBinaryFromReader(message: DeleteContactRequest, reader: jspb.BinaryReader): DeleteContactRequest;
}

export namespace DeleteContactRequest {
  export type AsObject = {
    contactGuid: string,
  }
}

export class ExecVerifyAddressRequest extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): ExecVerifyAddressRequest;

  getAddress(): Address | undefined;
  setAddress(value?: Address): ExecVerifyAddressRequest;
  hasAddress(): boolean;
  clearAddress(): ExecVerifyAddressRequest;

  getRequestedAddressCase(): ExecVerifyAddressRequest.RequestedAddressCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecVerifyAddressRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecVerifyAddressRequest): ExecVerifyAddressRequest.AsObject;
  static serializeBinaryToWriter(message: ExecVerifyAddressRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecVerifyAddressRequest;
  static deserializeBinaryFromReader(message: ExecVerifyAddressRequest, reader: jspb.BinaryReader): ExecVerifyAddressRequest;
}

export namespace ExecVerifyAddressRequest {
  export type AsObject = {
    addressGuid: string,
    address?: Address.AsObject,
  }

  export enum RequestedAddressCase { 
    REQUESTED_ADDRESS_NOT_SET = 0,
    ADDRESS_GUID = 1,
    ADDRESS = 2,
  }
}

export class ExecVerifyAddressResponse extends jspb.Message {
  getAddressVerification(): AddressVerification | undefined;
  setAddressVerification(value?: AddressVerification): ExecVerifyAddressResponse;
  hasAddressVerification(): boolean;
  clearAddressVerification(): ExecVerifyAddressResponse;

  getUpdatedAddress(): Address | undefined;
  setUpdatedAddress(value?: Address): ExecVerifyAddressResponse;
  hasUpdatedAddress(): boolean;
  clearUpdatedAddress(): ExecVerifyAddressResponse;

  getVerifiedCase(): ExecVerifyAddressResponse.VerifiedCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecVerifyAddressResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ExecVerifyAddressResponse): ExecVerifyAddressResponse.AsObject;
  static serializeBinaryToWriter(message: ExecVerifyAddressResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecVerifyAddressResponse;
  static deserializeBinaryFromReader(message: ExecVerifyAddressResponse, reader: jspb.BinaryReader): ExecVerifyAddressResponse;
}

export namespace ExecVerifyAddressResponse {
  export type AsObject = {
    addressVerification?: AddressVerification.AsObject,
    updatedAddress?: Address.AsObject,
  }

  export enum VerifiedCase { 
    VERIFIED_NOT_SET = 0,
    ADDRESS_VERIFICATION = 1,
    UPDATED_ADDRESS = 2,
  }
}

export class ExecQualifyContactRequest extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): ExecQualifyContactRequest;

  getContact(): Contact | undefined;
  setContact(value?: Contact): ExecQualifyContactRequest;
  hasContact(): boolean;
  clearContact(): ExecQualifyContactRequest;

  getRequestedContactCase(): ExecQualifyContactRequest.RequestedContactCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecQualifyContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ExecQualifyContactRequest): ExecQualifyContactRequest.AsObject;
  static serializeBinaryToWriter(message: ExecQualifyContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecQualifyContactRequest;
  static deserializeBinaryFromReader(message: ExecQualifyContactRequest, reader: jspb.BinaryReader): ExecQualifyContactRequest;
}

export namespace ExecQualifyContactRequest {
  export type AsObject = {
    contactGuid: string,
    contact?: Contact.AsObject,
  }

  export enum RequestedContactCase { 
    REQUESTED_CONTACT_NOT_SET = 0,
    CONTACT_GUID = 1,
    CONTACT = 2,
  }
}

export class ExecQualifyContactResponse extends jspb.Message {
  getContactQualification(): ContactQualification | undefined;
  setContactQualification(value?: ContactQualification): ExecQualifyContactResponse;
  hasContactQualification(): boolean;
  clearContactQualification(): ExecQualifyContactResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExecQualifyContactResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ExecQualifyContactResponse): ExecQualifyContactResponse.AsObject;
  static serializeBinaryToWriter(message: ExecQualifyContactResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExecQualifyContactResponse;
  static deserializeBinaryFromReader(message: ExecQualifyContactResponse, reader: jspb.BinaryReader): ExecQualifyContactResponse;
}

export namespace ExecQualifyContactResponse {
  export type AsObject = {
    contactQualification?: ContactQualification.AsObject,
  }
}

export class GetAccountsResponse extends jspb.Message {
  getAccountsList(): Array<Account>;
  setAccountsList(value: Array<Account>): GetAccountsResponse;
  clearAccountsList(): GetAccountsResponse;
  addAccounts(value?: Account, index?: number): Account;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAccountsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetAccountsResponse): GetAccountsResponse.AsObject;
  static serializeBinaryToWriter(message: GetAccountsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAccountsResponse;
  static deserializeBinaryFromReader(message: GetAccountsResponse, reader: jspb.BinaryReader): GetAccountsResponse;
}

export namespace GetAccountsResponse {
  export type AsObject = {
    accountsList: Array<Account.AsObject>,
  }
}

export class GetAddressesResponse extends jspb.Message {
  getAddressesList(): Array<Address>;
  setAddressesList(value: Array<Address>): GetAddressesResponse;
  clearAddressesList(): GetAddressesResponse;
  addAddresses(value?: Address, index?: number): Address;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAddressesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetAddressesResponse): GetAddressesResponse.AsObject;
  static serializeBinaryToWriter(message: GetAddressesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAddressesResponse;
  static deserializeBinaryFromReader(message: GetAddressesResponse, reader: jspb.BinaryReader): GetAddressesResponse;
}

export namespace GetAddressesResponse {
  export type AsObject = {
    addressesList: Array<Address.AsObject>,
  }
}

export class GetContactsResponse extends jspb.Message {
  getContactsList(): Array<Contact>;
  setContactsList(value: Array<Contact>): GetContactsResponse;
  clearContactsList(): GetContactsResponse;
  addContacts(value?: Contact, index?: number): Contact;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetContactsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetContactsResponse): GetContactsResponse.AsObject;
  static serializeBinaryToWriter(message: GetContactsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetContactsResponse;
  static deserializeBinaryFromReader(message: GetContactsResponse, reader: jspb.BinaryReader): GetContactsResponse;
}

export namespace GetContactsResponse {
  export type AsObject = {
    contactsList: Array<Contact.AsObject>,
  }
}

export class GetCountriesRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): GetCountriesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCountriesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCountriesRequest): GetCountriesRequest.AsObject;
  static serializeBinaryToWriter(message: GetCountriesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCountriesRequest;
  static deserializeBinaryFromReader(message: GetCountriesRequest, reader: jspb.BinaryReader): GetCountriesRequest;
}

export namespace GetCountriesRequest {
  export type AsObject = {
    organizationCode: string,
  }
}

export class GetStatesRequest extends jspb.Message {
  getCountryCode(): string;
  setCountryCode(value: string): GetStatesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStatesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetStatesRequest): GetStatesRequest.AsObject;
  static serializeBinaryToWriter(message: GetStatesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStatesRequest;
  static deserializeBinaryFromReader(message: GetStatesRequest, reader: jspb.BinaryReader): GetStatesRequest;
}

export namespace GetStatesRequest {
  export type AsObject = {
    countryCode: string,
  }
}

export class GetCountriesResponse extends jspb.Message {
  getCountriesList(): Array<Country>;
  setCountriesList(value: Array<Country>): GetCountriesResponse;
  clearCountriesList(): GetCountriesResponse;
  addCountries(value?: Country, index?: number): Country;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCountriesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCountriesResponse): GetCountriesResponse.AsObject;
  static serializeBinaryToWriter(message: GetCountriesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCountriesResponse;
  static deserializeBinaryFromReader(message: GetCountriesResponse, reader: jspb.BinaryReader): GetCountriesResponse;
}

export namespace GetCountriesResponse {
  export type AsObject = {
    countriesList: Array<Country.AsObject>,
  }
}

export class GetStatesResponse extends jspb.Message {
  getStatesList(): Array<State>;
  setStatesList(value: Array<State>): GetStatesResponse;
  clearStatesList(): GetStatesResponse;
  addStates(value?: State, index?: number): State;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetStatesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetStatesResponse): GetStatesResponse.AsObject;
  static serializeBinaryToWriter(message: GetStatesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetStatesResponse;
  static deserializeBinaryFromReader(message: GetStatesResponse, reader: jspb.BinaryReader): GetStatesResponse;
}

export namespace GetStatesResponse {
  export type AsObject = {
    statesList: Array<State.AsObject>,
  }
}

export enum AddressVerificationType { 
  MELISSA = 0,
}
export enum ContactQualificationType { 
  ORGANIZATION = 0,
}
