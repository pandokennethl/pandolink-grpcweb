import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_contact_v1_contact_pb from '../../../../pando/api/contact/v1/contact_pb';


export class ContactServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createAccount(
    request: pando_api_contact_v1_contact_pb.Account,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Account) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Account>;

  createAddress(
    request: pando_api_contact_v1_contact_pb.Address,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Address) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Address>;

  createContact(
    request: pando_api_contact_v1_contact_pb.Contact,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Contact) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Contact>;

  getAccount(
    request: pando_api_contact_v1_contact_pb.GetAccountRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Account) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Account>;

  getAddress(
    request: pando_api_contact_v1_contact_pb.GetAddressRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Address) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Address>;

  getContact(
    request: pando_api_contact_v1_contact_pb.GetContactRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Contact) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Contact>;

  getAccounts(
    request: pando_api_contact_v1_contact_pb.GetAccountsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.GetAccountsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.GetAccountsResponse>;

  getAddresses(
    request: pando_api_contact_v1_contact_pb.GetAddressesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.GetAddressesResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.GetAddressesResponse>;

  getContacts(
    request: pando_api_contact_v1_contact_pb.GetContactsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.GetContactsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.GetContactsResponse>;

  getCountries(
    request: pando_api_contact_v1_contact_pb.GetCountriesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.GetCountriesResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.GetCountriesResponse>;

  getStates(
    request: pando_api_contact_v1_contact_pb.GetStatesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.GetStatesResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.GetStatesResponse>;

  updateAccount(
    request: pando_api_contact_v1_contact_pb.UpdateAccountRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Account) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Account>;

  updateContact(
    request: pando_api_contact_v1_contact_pb.UpdateContactRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.Contact) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.Contact>;

  deleteAccount(
    request: pando_api_contact_v1_contact_pb.DeleteAccountRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteAddress(
    request: pando_api_contact_v1_contact_pb.DeleteAddressRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  deleteContact(
    request: pando_api_contact_v1_contact_pb.DeleteContactRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execVerifyAddress(
    request: pando_api_contact_v1_contact_pb.ExecVerifyAddressRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.ExecVerifyAddressResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.ExecVerifyAddressResponse>;

  execQualifyContact(
    request: pando_api_contact_v1_contact_pb.ExecQualifyContactRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_contact_v1_contact_pb.ExecQualifyContactResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_contact_v1_contact_pb.ExecQualifyContactResponse>;

}

export class ContactServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createAccount(
    request: pando_api_contact_v1_contact_pb.Account,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Account>;

  createAddress(
    request: pando_api_contact_v1_contact_pb.Address,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Address>;

  createContact(
    request: pando_api_contact_v1_contact_pb.Contact,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Contact>;

  getAccount(
    request: pando_api_contact_v1_contact_pb.GetAccountRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Account>;

  getAddress(
    request: pando_api_contact_v1_contact_pb.GetAddressRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Address>;

  getContact(
    request: pando_api_contact_v1_contact_pb.GetContactRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Contact>;

  getAccounts(
    request: pando_api_contact_v1_contact_pb.GetAccountsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.GetAccountsResponse>;

  getAddresses(
    request: pando_api_contact_v1_contact_pb.GetAddressesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.GetAddressesResponse>;

  getContacts(
    request: pando_api_contact_v1_contact_pb.GetContactsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.GetContactsResponse>;

  getCountries(
    request: pando_api_contact_v1_contact_pb.GetCountriesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.GetCountriesResponse>;

  getStates(
    request: pando_api_contact_v1_contact_pb.GetStatesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.GetStatesResponse>;

  updateAccount(
    request: pando_api_contact_v1_contact_pb.UpdateAccountRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Account>;

  updateContact(
    request: pando_api_contact_v1_contact_pb.UpdateContactRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.Contact>;

  deleteAccount(
    request: pando_api_contact_v1_contact_pb.DeleteAccountRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteAddress(
    request: pando_api_contact_v1_contact_pb.DeleteAddressRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  deleteContact(
    request: pando_api_contact_v1_contact_pb.DeleteContactRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execVerifyAddress(
    request: pando_api_contact_v1_contact_pb.ExecVerifyAddressRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.ExecVerifyAddressResponse>;

  execQualifyContact(
    request: pando_api_contact_v1_contact_pb.ExecQualifyContactRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_contact_v1_contact_pb.ExecQualifyContactResponse>;

}

