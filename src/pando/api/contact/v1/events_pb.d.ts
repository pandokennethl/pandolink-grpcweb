import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class AccountAccessed extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): AccountAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AccountAccessed;
  hasUserData(): boolean;
  clearUserData(): AccountAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AccountAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: AccountAccessed): AccountAccessed.AsObject;
  static serializeBinaryToWriter(message: AccountAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AccountAccessed;
  static deserializeBinaryFromReader(message: AccountAccessed, reader: jspb.BinaryReader): AccountAccessed;
}

export namespace AccountAccessed {
  export type AsObject = {
    accountGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class AddressAccessed extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): AddressAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AddressAccessed;
  hasUserData(): boolean;
  clearUserData(): AddressAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddressAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: AddressAccessed): AddressAccessed.AsObject;
  static serializeBinaryToWriter(message: AddressAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddressAccessed;
  static deserializeBinaryFromReader(message: AddressAccessed, reader: jspb.BinaryReader): AddressAccessed;
}

export namespace AddressAccessed {
  export type AsObject = {
    addressGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class ContactAccessed extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): ContactAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): ContactAccessed;
  hasUserData(): boolean;
  clearUserData(): ContactAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: ContactAccessed): ContactAccessed.AsObject;
  static serializeBinaryToWriter(message: ContactAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactAccessed;
  static deserializeBinaryFromReader(message: ContactAccessed, reader: jspb.BinaryReader): ContactAccessed;
}

export namespace ContactAccessed {
  export type AsObject = {
    contactGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class AddressesAccessed extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): AddressesAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AddressesAccessed;
  hasUserData(): boolean;
  clearUserData(): AddressesAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddressesAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: AddressesAccessed): AddressesAccessed.AsObject;
  static serializeBinaryToWriter(message: AddressesAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddressesAccessed;
  static deserializeBinaryFromReader(message: AddressesAccessed, reader: jspb.BinaryReader): AddressesAccessed;
}

export namespace AddressesAccessed {
  export type AsObject = {
    accountGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class ContactsAccessed extends jspb.Message {
  getAccountGuid(): string;
  setAccountGuid(value: string): ContactsAccessed;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): ContactsAccessed;
  hasUserData(): boolean;
  clearUserData(): ContactsAccessed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactsAccessed.AsObject;
  static toObject(includeInstance: boolean, msg: ContactsAccessed): ContactsAccessed.AsObject;
  static serializeBinaryToWriter(message: ContactsAccessed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactsAccessed;
  static deserializeBinaryFromReader(message: ContactsAccessed, reader: jspb.BinaryReader): ContactsAccessed;
}

export namespace ContactsAccessed {
  export type AsObject = {
    accountGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class AddressVerified extends jspb.Message {
  getAddressGuid(): string;
  setAddressGuid(value: string): AddressVerified;

  getAddressVerificationGuid(): string;
  setAddressVerificationGuid(value: string): AddressVerified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AddressVerified;
  hasUserData(): boolean;
  clearUserData(): AddressVerified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddressVerified.AsObject;
  static toObject(includeInstance: boolean, msg: AddressVerified): AddressVerified.AsObject;
  static serializeBinaryToWriter(message: AddressVerified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddressVerified;
  static deserializeBinaryFromReader(message: AddressVerified, reader: jspb.BinaryReader): AddressVerified;
}

export namespace AddressVerified {
  export type AsObject = {
    addressGuid: string,
    addressVerificationGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class ContactQualified extends jspb.Message {
  getContactGuid(): string;
  setContactGuid(value: string): ContactQualified;

  getContactQualificationGuid(): string;
  setContactQualificationGuid(value: string): ContactQualified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): ContactQualified;
  hasUserData(): boolean;
  clearUserData(): ContactQualified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactQualified.AsObject;
  static toObject(includeInstance: boolean, msg: ContactQualified): ContactQualified.AsObject;
  static serializeBinaryToWriter(message: ContactQualified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactQualified;
  static deserializeBinaryFromReader(message: ContactQualified, reader: jspb.BinaryReader): ContactQualified;
}

export namespace ContactQualified {
  export type AsObject = {
    contactGuid: string,
    contactQualificationGuid: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export enum Events { 
  ACCOUNT_CREATED = 0,
  ACCOUNT_RETRIEVED = 1,
  ACCOUNTS_RETRIEVED = 2,
  ACCOUNT_UPDATED = 3,
  ACCOUNT_DELETED = 4,
  ADDRESS_CREATED = 5,
  ADDRESS_RETRIEVED = 6,
  ADDRESSES_RETRIEVED = 7,
  ADDRESS_UPDATED = 8,
  ADDRESS_DELETED = 9,
  ADDRESS_VERIFIED = 10,
  CONTACT_CREATED = 11,
  CONTACT_RETRIEVED = 12,
  CONTACTS_RETRIEVED = 13,
  CONTACT_UPDATED = 14,
  CONTACT_DELETED = 15,
  CONTACT_QUALIFIED = 16,
}
