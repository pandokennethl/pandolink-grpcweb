import * as jspb from 'google-protobuf'

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_alarm_v0_panel$model_pb from '../../../../pando/api/alarm/v0/panel-model_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetPanelTypeRequest extends jspb.Message {
  getPanelCode(): string;
  setPanelCode(value: string): GetPanelTypeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetPanelTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPanelTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPanelTypeRequest): GetPanelTypeRequest.AsObject;
  static serializeBinaryToWriter(message: GetPanelTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPanelTypeRequest;
  static deserializeBinaryFromReader(message: GetPanelTypeRequest, reader: jspb.BinaryReader): GetPanelTypeRequest;
}

export namespace GetPanelTypeRequest {
  export type AsObject = {
    panelCode: string,
    includeInactive: boolean,
  }
}

export class GetPanelConfigurationRequest extends jspb.Message {
  getPanelConfigurationGuid(): string;
  setPanelConfigurationGuid(value: string): GetPanelConfigurationRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetPanelConfigurationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPanelConfigurationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPanelConfigurationRequest): GetPanelConfigurationRequest.AsObject;
  static serializeBinaryToWriter(message: GetPanelConfigurationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPanelConfigurationRequest;
  static deserializeBinaryFromReader(message: GetPanelConfigurationRequest, reader: jspb.BinaryReader): GetPanelConfigurationRequest;
}

export namespace GetPanelConfigurationRequest {
  export type AsObject = {
    panelConfigurationGuid: string,
    includeInactive: boolean,
  }
}

export class GetPanelEquipmentGroupsRequest extends jspb.Message {
  getGroupCodesList(): Array<string>;
  setGroupCodesList(value: Array<string>): GetPanelEquipmentGroupsRequest;
  clearGroupCodesList(): GetPanelEquipmentGroupsRequest;
  addGroupCodes(value: string, index?: number): GetPanelEquipmentGroupsRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetPanelEquipmentGroupsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPanelEquipmentGroupsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPanelEquipmentGroupsRequest): GetPanelEquipmentGroupsRequest.AsObject;
  static serializeBinaryToWriter(message: GetPanelEquipmentGroupsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPanelEquipmentGroupsRequest;
  static deserializeBinaryFromReader(message: GetPanelEquipmentGroupsRequest, reader: jspb.BinaryReader): GetPanelEquipmentGroupsRequest;
}

export namespace GetPanelEquipmentGroupsRequest {
  export type AsObject = {
    groupCodesList: Array<string>,
    includeInactive: boolean,
  }
}

export class GetPanelConfigurationGuidListRequest extends jspb.Message {
  getPanelConfigurationGuidsList(): Array<string>;
  setPanelConfigurationGuidsList(value: Array<string>): GetPanelConfigurationGuidListRequest;
  clearPanelConfigurationGuidsList(): GetPanelConfigurationGuidListRequest;
  addPanelConfigurationGuids(value: string, index?: number): GetPanelConfigurationGuidListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetPanelConfigurationGuidListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPanelConfigurationGuidListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPanelConfigurationGuidListRequest): GetPanelConfigurationGuidListRequest.AsObject;
  static serializeBinaryToWriter(message: GetPanelConfigurationGuidListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPanelConfigurationGuidListRequest;
  static deserializeBinaryFromReader(message: GetPanelConfigurationGuidListRequest, reader: jspb.BinaryReader): GetPanelConfigurationGuidListRequest;
}

export namespace GetPanelConfigurationGuidListRequest {
  export type AsObject = {
    panelConfigurationGuidsList: Array<string>,
    includeInactive: boolean,
  }
}

export class EditCompatibleEquipmentRequest extends jspb.Message {
  getPanelCode(): string;
  setPanelCode(value: string): EditCompatibleEquipmentRequest;

  getEquipmentGuidsList(): Array<string>;
  setEquipmentGuidsList(value: Array<string>): EditCompatibleEquipmentRequest;
  clearEquipmentGuidsList(): EditCompatibleEquipmentRequest;
  addEquipmentGuids(value: string, index?: number): EditCompatibleEquipmentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EditCompatibleEquipmentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: EditCompatibleEquipmentRequest): EditCompatibleEquipmentRequest.AsObject;
  static serializeBinaryToWriter(message: EditCompatibleEquipmentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EditCompatibleEquipmentRequest;
  static deserializeBinaryFromReader(message: EditCompatibleEquipmentRequest, reader: jspb.BinaryReader): EditCompatibleEquipmentRequest;
}

export namespace EditCompatibleEquipmentRequest {
  export type AsObject = {
    panelCode: string,
    equipmentGuidsList: Array<string>,
  }
}

export class EditPanelEquipmentRequest extends jspb.Message {
  getPanelEquipmentListList(): Array<pando_api_alarm_v0_panel$model_pb.PanelEquipment>;
  setPanelEquipmentListList(value: Array<pando_api_alarm_v0_panel$model_pb.PanelEquipment>): EditPanelEquipmentRequest;
  clearPanelEquipmentListList(): EditPanelEquipmentRequest;
  addPanelEquipmentList(value?: pando_api_alarm_v0_panel$model_pb.PanelEquipment, index?: number): pando_api_alarm_v0_panel$model_pb.PanelEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EditPanelEquipmentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: EditPanelEquipmentRequest): EditPanelEquipmentRequest.AsObject;
  static serializeBinaryToWriter(message: EditPanelEquipmentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EditPanelEquipmentRequest;
  static deserializeBinaryFromReader(message: EditPanelEquipmentRequest, reader: jspb.BinaryReader): EditPanelEquipmentRequest;
}

export namespace EditPanelEquipmentRequest {
  export type AsObject = {
    panelEquipmentListList: Array<pando_api_alarm_v0_panel$model_pb.PanelEquipment.AsObject>,
  }
}

