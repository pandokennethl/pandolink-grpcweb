import * as jspb from 'google-protobuf'

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_alarm_v0_signal$model_pb from '../../../../pando/api/alarm/v0/signal-model_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetRequest extends jspb.Message {
  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): GetRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRequest): GetRequest.AsObject;
  static serializeBinaryToWriter(message: GetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRequest;
  static deserializeBinaryFromReader(message: GetRequest, reader: jspb.BinaryReader): GetRequest;
}

export namespace GetRequest {
  export type AsObject = {
    equipmentGuid: string,
    includeInactive: boolean,
  }
}

export class GetModelListRequest extends jspb.Message {
  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetModelListRequest;

  getEquipmentGuidsList(): Array<string>;
  setEquipmentGuidsList(value: Array<string>): GetModelListRequest;
  clearEquipmentGuidsList(): GetModelListRequest;
  addEquipmentGuids(value: string, index?: number): GetModelListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetModelListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetModelListRequest): GetModelListRequest.AsObject;
  static serializeBinaryToWriter(message: GetModelListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetModelListRequest;
  static deserializeBinaryFromReader(message: GetModelListRequest, reader: jspb.BinaryReader): GetModelListRequest;
}

export namespace GetModelListRequest {
  export type AsObject = {
    includeInactive: boolean,
    equipmentGuidsList: Array<string>,
  }
}

export class EditSignalsRequest extends jspb.Message {
  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): EditSignalsRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): EditSignalsRequest;

  getSignalTypeGuidsList(): Array<string>;
  setSignalTypeGuidsList(value: Array<string>): EditSignalsRequest;
  clearSignalTypeGuidsList(): EditSignalsRequest;
  addSignalTypeGuids(value: string, index?: number): EditSignalsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EditSignalsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: EditSignalsRequest): EditSignalsRequest.AsObject;
  static serializeBinaryToWriter(message: EditSignalsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EditSignalsRequest;
  static deserializeBinaryFromReader(message: EditSignalsRequest, reader: jspb.BinaryReader): EditSignalsRequest;
}

export namespace EditSignalsRequest {
  export type AsObject = {
    equipmentGuid: string,
    includeInactive: boolean,
    signalTypeGuidsList: Array<string>,
  }
}

export class GetEquipmentByExtensionRequest extends jspb.Message {
  getExtenstionName(): string;
  setExtenstionName(value: string): GetEquipmentByExtensionRequest;

  getExtenstionValue(): string;
  setExtenstionValue(value: string): GetEquipmentByExtensionRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetEquipmentByExtensionRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEquipmentByExtensionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetEquipmentByExtensionRequest): GetEquipmentByExtensionRequest.AsObject;
  static serializeBinaryToWriter(message: GetEquipmentByExtensionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEquipmentByExtensionRequest;
  static deserializeBinaryFromReader(message: GetEquipmentByExtensionRequest, reader: jspb.BinaryReader): GetEquipmentByExtensionRequest;
}

export namespace GetEquipmentByExtensionRequest {
  export type AsObject = {
    extenstionName: string,
    extenstionValue: string,
    includeInactive: boolean,
  }
}

