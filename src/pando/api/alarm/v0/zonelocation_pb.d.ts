import * as jspb from 'google-protobuf'

import * as pando_api_alarm_v0_zonelocation$model_pb from '../../../../pando/api/alarm/v0/zonelocation-model_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetModelRequest extends jspb.Message {
  getZoneLocationGuid(): string;
  setZoneLocationGuid(value: string): GetModelRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetModelRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetModelRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetModelRequest): GetModelRequest.AsObject;
  static serializeBinaryToWriter(message: GetModelRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetModelRequest;
  static deserializeBinaryFromReader(message: GetModelRequest, reader: jspb.BinaryReader): GetModelRequest;
}

export namespace GetModelRequest {
  export type AsObject = {
    zoneLocationGuid: string,
    includeInactive: boolean,
  }
}

export class GetModelListRequest extends jspb.Message {
  getZoneLocationGuidsList(): Array<string>;
  setZoneLocationGuidsList(value: Array<string>): GetModelListRequest;
  clearZoneLocationGuidsList(): GetModelListRequest;
  addZoneLocationGuids(value: string, index?: number): GetModelListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetModelListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetModelListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetModelListRequest): GetModelListRequest.AsObject;
  static serializeBinaryToWriter(message: GetModelListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetModelListRequest;
  static deserializeBinaryFromReader(message: GetModelListRequest, reader: jspb.BinaryReader): GetModelListRequest;
}

export namespace GetModelListRequest {
  export type AsObject = {
    zoneLocationGuidsList: Array<string>,
    includeInactive: boolean,
  }
}

export class FindModelsByZoneLocationRequest extends jspb.Message {
  getZoneLocationExpression(): string;
  setZoneLocationExpression(value: string): FindModelsByZoneLocationRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): FindModelsByZoneLocationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FindModelsByZoneLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: FindModelsByZoneLocationRequest): FindModelsByZoneLocationRequest.AsObject;
  static serializeBinaryToWriter(message: FindModelsByZoneLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FindModelsByZoneLocationRequest;
  static deserializeBinaryFromReader(message: FindModelsByZoneLocationRequest, reader: jspb.BinaryReader): FindModelsByZoneLocationRequest;
}

export namespace FindModelsByZoneLocationRequest {
  export type AsObject = {
    zoneLocationExpression: string,
    includeInactive: boolean,
  }
}

export class GetModelByDeviceTypeRequest extends jspb.Message {
  getDeviceTypeCode(): string;
  setDeviceTypeCode(value: string): GetModelByDeviceTypeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetModelByDeviceTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetModelByDeviceTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetModelByDeviceTypeRequest): GetModelByDeviceTypeRequest.AsObject;
  static serializeBinaryToWriter(message: GetModelByDeviceTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetModelByDeviceTypeRequest;
  static deserializeBinaryFromReader(message: GetModelByDeviceTypeRequest, reader: jspb.BinaryReader): GetModelByDeviceTypeRequest;
}

export namespace GetModelByDeviceTypeRequest {
  export type AsObject = {
    deviceTypeCode: string,
    includeInactive: boolean,
  }
}

export class GetDeviceTypeListRequest extends jspb.Message {
  getDeviceTypeCodesList(): Array<string>;
  setDeviceTypeCodesList(value: Array<string>): GetDeviceTypeListRequest;
  clearDeviceTypeCodesList(): GetDeviceTypeListRequest;
  addDeviceTypeCodes(value: string, index?: number): GetDeviceTypeListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetDeviceTypeListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDeviceTypeListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDeviceTypeListRequest): GetDeviceTypeListRequest.AsObject;
  static serializeBinaryToWriter(message: GetDeviceTypeListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDeviceTypeListRequest;
  static deserializeBinaryFromReader(message: GetDeviceTypeListRequest, reader: jspb.BinaryReader): GetDeviceTypeListRequest;
}

export namespace GetDeviceTypeListRequest {
  export type AsObject = {
    deviceTypeCodesList: Array<string>,
    includeInactive: boolean,
  }
}

