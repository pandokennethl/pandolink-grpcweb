import * as jspb from 'google-protobuf'

import * as pando_api_alarm_v0_zonetype$model_pb from '../../../../pando/api/alarm/v0/zonetype-model_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetTypeRequest extends jspb.Message {
  getZoneTypeGuid(): string;
  setZoneTypeGuid(value: string): GetTypeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeRequest): GetTypeRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeRequest;
  static deserializeBinaryFromReader(message: GetTypeRequest, reader: jspb.BinaryReader): GetTypeRequest;
}

export namespace GetTypeRequest {
  export type AsObject = {
    zoneTypeGuid: string,
    includeInactive: boolean,
  }
}

export class GetTypeListRequest extends jspb.Message {
  getCentralStationCode(): string;
  setCentralStationCode(value: string): GetTypeListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeListRequest): GetTypeListRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeListRequest;
  static deserializeBinaryFromReader(message: GetTypeListRequest, reader: jspb.BinaryReader): GetTypeListRequest;
}

export namespace GetTypeListRequest {
  export type AsObject = {
    centralStationCode: string,
    includeInactive: boolean,
  }
}

