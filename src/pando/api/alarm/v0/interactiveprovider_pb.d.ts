import * as jspb from 'google-protobuf'

import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetTypeRequest extends jspb.Message {
  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): GetTypeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeRequest): GetTypeRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeRequest;
  static deserializeBinaryFromReader(message: GetTypeRequest, reader: jspb.BinaryReader): GetTypeRequest;
}

export namespace GetTypeRequest {
  export type AsObject = {
    interactiveProviderCode: string,
    includeInactive: boolean,
  }
}

export class GetConfigurationRequest extends jspb.Message {
  getClientInteractiveProviderGuid(): string;
  setClientInteractiveProviderGuid(value: string): GetConfigurationRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetConfigurationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetConfigurationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetConfigurationRequest): GetConfigurationRequest.AsObject;
  static serializeBinaryToWriter(message: GetConfigurationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetConfigurationRequest;
  static deserializeBinaryFromReader(message: GetConfigurationRequest, reader: jspb.BinaryReader): GetConfigurationRequest;
}

export namespace GetConfigurationRequest {
  export type AsObject = {
    clientInteractiveProviderGuid: string,
    includeInactive: boolean,
  }
}

export class GetConfigurationListRequest extends jspb.Message {
  getClientInteractiveProviderGuidsList(): Array<string>;
  setClientInteractiveProviderGuidsList(value: Array<string>): GetConfigurationListRequest;
  clearClientInteractiveProviderGuidsList(): GetConfigurationListRequest;
  addClientInteractiveProviderGuids(value: string, index?: number): GetConfigurationListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetConfigurationListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetConfigurationListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetConfigurationListRequest): GetConfigurationListRequest.AsObject;
  static serializeBinaryToWriter(message: GetConfigurationListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetConfigurationListRequest;
  static deserializeBinaryFromReader(message: GetConfigurationListRequest, reader: jspb.BinaryReader): GetConfigurationListRequest;
}

export namespace GetConfigurationListRequest {
  export type AsObject = {
    clientInteractiveProviderGuidsList: Array<string>,
    includeInactive: boolean,
  }
}

export class Type extends jspb.Message {
  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): Type;

  getCredentialPrefix(): string;
  setCredentialPrefix(value: string): Type;

  getDisplayName(): string;
  setDisplayName(value: string): Type;

  getIsActive(): boolean;
  setIsActive(value: boolean): Type;

  getDateCreated(): string;
  setDateCreated(value: string): Type;

  getDateLastUpdated(): string;
  setDateLastUpdated(value: string): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Type.AsObject;
  static toObject(includeInstance: boolean, msg: Type): Type.AsObject;
  static serializeBinaryToWriter(message: Type, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Type;
  static deserializeBinaryFromReader(message: Type, reader: jspb.BinaryReader): Type;
}

export namespace Type {
  export type AsObject = {
    interactiveProviderCode: string,
    credentialPrefix: string,
    displayName: string,
    isActive: boolean,
    dateCreated: string,
    dateLastUpdated: string,
  }
}

export class TypeList extends jspb.Message {
  getTypesList(): Array<Type>;
  setTypesList(value: Array<Type>): TypeList;
  clearTypesList(): TypeList;
  addTypes(value?: Type, index?: number): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TypeList.AsObject;
  static toObject(includeInstance: boolean, msg: TypeList): TypeList.AsObject;
  static serializeBinaryToWriter(message: TypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TypeList;
  static deserializeBinaryFromReader(message: TypeList, reader: jspb.BinaryReader): TypeList;
}

export namespace TypeList {
  export type AsObject = {
    typesList: Array<Type.AsObject>,
  }
}

export class Setting extends jspb.Message {
  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): Setting;

  getSettingCode(): string;
  setSettingCode(value: string): Setting;

  getDescription(): string;
  setDescription(value: string): Setting;

  getDataType(): string;
  setDataType(value: string): Setting;

  getDateCreated(): string;
  setDateCreated(value: string): Setting;

  getDateLastUpdated(): string;
  setDateLastUpdated(value: string): Setting;

  getDefaultValue(): string;
  setDefaultValue(value: string): Setting;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Setting.AsObject;
  static toObject(includeInstance: boolean, msg: Setting): Setting.AsObject;
  static serializeBinaryToWriter(message: Setting, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Setting;
  static deserializeBinaryFromReader(message: Setting, reader: jspb.BinaryReader): Setting;
}

export namespace Setting {
  export type AsObject = {
    interactiveProviderCode: string,
    settingCode: string,
    description: string,
    dataType: string,
    dateCreated: string,
    dateLastUpdated: string,
    defaultValue: string,
  }
}

export class SettingValue extends jspb.Message {
  getClientInteractiveProviderGuid(): string;
  setClientInteractiveProviderGuid(value: string): SettingValue;

  getSettingCode(): string;
  setSettingCode(value: string): SettingValue;

  getDataType(): string;
  setDataType(value: string): SettingValue;

  getSettingValue(): string;
  setSettingValue(value: string): SettingValue;

  getDateLastUpdated(): string;
  setDateLastUpdated(value: string): SettingValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SettingValue.AsObject;
  static toObject(includeInstance: boolean, msg: SettingValue): SettingValue.AsObject;
  static serializeBinaryToWriter(message: SettingValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SettingValue;
  static deserializeBinaryFromReader(message: SettingValue, reader: jspb.BinaryReader): SettingValue;
}

export namespace SettingValue {
  export type AsObject = {
    clientInteractiveProviderGuid: string,
    settingCode: string,
    dataType: string,
    settingValue: string,
    dateLastUpdated: string,
  }
}

export class Configuration extends jspb.Message {
  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): Configuration;

  getClientInteractiveProviderGuid(): string;
  setClientInteractiveProviderGuid(value: string): Configuration;

  getDescription(): string;
  setDescription(value: string): Configuration;

  getIsActive(): boolean;
  setIsActive(value: boolean): Configuration;

  getSettingValuesList(): Array<SettingValue>;
  setSettingValuesList(value: Array<SettingValue>): Configuration;
  clearSettingValuesList(): Configuration;
  addSettingValues(value?: SettingValue, index?: number): SettingValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Configuration.AsObject;
  static toObject(includeInstance: boolean, msg: Configuration): Configuration.AsObject;
  static serializeBinaryToWriter(message: Configuration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Configuration;
  static deserializeBinaryFromReader(message: Configuration, reader: jspb.BinaryReader): Configuration;
}

export namespace Configuration {
  export type AsObject = {
    interactiveProviderCode: string,
    clientInteractiveProviderGuid: string,
    description: string,
    isActive: boolean,
    settingValuesList: Array<SettingValue.AsObject>,
  }
}

export class ConfigurationList extends jspb.Message {
  getConfigurationsList(): Array<Configuration>;
  setConfigurationsList(value: Array<Configuration>): ConfigurationList;
  clearConfigurationsList(): ConfigurationList;
  addConfigurations(value?: Configuration, index?: number): Configuration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConfigurationList.AsObject;
  static toObject(includeInstance: boolean, msg: ConfigurationList): ConfigurationList.AsObject;
  static serializeBinaryToWriter(message: ConfigurationList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConfigurationList;
  static deserializeBinaryFromReader(message: ConfigurationList, reader: jspb.BinaryReader): ConfigurationList;
}

export namespace ConfigurationList {
  export type AsObject = {
    configurationsList: Array<Configuration.AsObject>,
  }
}

export class SettingList extends jspb.Message {
  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): SettingList;

  getSettingsList(): Array<Setting>;
  setSettingsList(value: Array<Setting>): SettingList;
  clearSettingsList(): SettingList;
  addSettings(value?: Setting, index?: number): Setting;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SettingList.AsObject;
  static toObject(includeInstance: boolean, msg: SettingList): SettingList.AsObject;
  static serializeBinaryToWriter(message: SettingList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SettingList;
  static deserializeBinaryFromReader(message: SettingList, reader: jspb.BinaryReader): SettingList;
}

export namespace SettingList {
  export type AsObject = {
    interactiveProviderCode: string,
    settingsList: Array<Setting.AsObject>,
  }
}

