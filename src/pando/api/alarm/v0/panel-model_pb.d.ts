import * as jspb from 'google-protobuf'



export class Type extends jspb.Message {
  getPanelCode(): string;
  setPanelCode(value: string): Type;

  getDisplayName(): string;
  setDisplayName(value: string): Type;

  getManufacturer(): string;
  setManufacturer(value: string): Type;

  getIsActive(): boolean;
  setIsActive(value: boolean): Type;

  getAllowDiy(): boolean;
  setAllowDiy(value: boolean): Type;

  getAllowExisting(): boolean;
  setAllowExisting(value: boolean): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Type.AsObject;
  static toObject(includeInstance: boolean, msg: Type): Type.AsObject;
  static serializeBinaryToWriter(message: Type, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Type;
  static deserializeBinaryFromReader(message: Type, reader: jspb.BinaryReader): Type;
}

export namespace Type {
  export type AsObject = {
    panelCode: string,
    displayName: string,
    manufacturer: string,
    isActive: boolean,
    allowDiy: boolean,
    allowExisting: boolean,
  }
}

export class Configuration extends jspb.Message {
  getPanelCode(): string;
  setPanelCode(value: string): Configuration;

  getPanelConfigurationGuid(): string;
  setPanelConfigurationGuid(value: string): Configuration;

  getCentralStationCode(): string;
  setCentralStationCode(value: string): Configuration;

  getInteractiveProviderCode(): string;
  setInteractiveProviderCode(value: string): Configuration;

  getIsActive(): boolean;
  setIsActive(value: boolean): Configuration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Configuration.AsObject;
  static toObject(includeInstance: boolean, msg: Configuration): Configuration.AsObject;
  static serializeBinaryToWriter(message: Configuration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Configuration;
  static deserializeBinaryFromReader(message: Configuration, reader: jspb.BinaryReader): Configuration;
}

export namespace Configuration {
  export type AsObject = {
    panelCode: string,
    panelConfigurationGuid: string,
    centralStationCode: string,
    interactiveProviderCode: string,
    isActive: boolean,
  }
}

export class PanelEquipment extends jspb.Message {
  getPanelCode(): string;
  setPanelCode(value: string): PanelEquipment;

  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): PanelEquipment;

  getPanelConfigurationGuid(): string;
  setPanelConfigurationGuid(value: string): PanelEquipment;

  getDisplayName(): string;
  setDisplayName(value: string): PanelEquipment;

  getGroupCode(): string;
  setGroupCode(value: string): PanelEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PanelEquipment.AsObject;
  static toObject(includeInstance: boolean, msg: PanelEquipment): PanelEquipment.AsObject;
  static serializeBinaryToWriter(message: PanelEquipment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PanelEquipment;
  static deserializeBinaryFromReader(message: PanelEquipment, reader: jspb.BinaryReader): PanelEquipment;
}

export namespace PanelEquipment {
  export type AsObject = {
    panelCode: string,
    equipmentGuid: string,
    panelConfigurationGuid: string,
    displayName: string,
    groupCode: string,
  }
}

export class PanelEquipmentGroup extends jspb.Message {
  getGroupCode(): string;
  setGroupCode(value: string): PanelEquipmentGroup;

  getDisplayName(): string;
  setDisplayName(value: string): PanelEquipmentGroup;

  getIsActive(): boolean;
  setIsActive(value: boolean): PanelEquipmentGroup;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PanelEquipmentGroup.AsObject;
  static toObject(includeInstance: boolean, msg: PanelEquipmentGroup): PanelEquipmentGroup.AsObject;
  static serializeBinaryToWriter(message: PanelEquipmentGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PanelEquipmentGroup;
  static deserializeBinaryFromReader(message: PanelEquipmentGroup, reader: jspb.BinaryReader): PanelEquipmentGroup;
}

export namespace PanelEquipmentGroup {
  export type AsObject = {
    groupCode: string,
    displayName: string,
    isActive: boolean,
  }
}

export class TypeList extends jspb.Message {
  getTypesList(): Array<Type>;
  setTypesList(value: Array<Type>): TypeList;
  clearTypesList(): TypeList;
  addTypes(value?: Type, index?: number): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TypeList.AsObject;
  static toObject(includeInstance: boolean, msg: TypeList): TypeList.AsObject;
  static serializeBinaryToWriter(message: TypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TypeList;
  static deserializeBinaryFromReader(message: TypeList, reader: jspb.BinaryReader): TypeList;
}

export namespace TypeList {
  export type AsObject = {
    typesList: Array<Type.AsObject>,
  }
}

export class ConfigurationList extends jspb.Message {
  getConfigurationsList(): Array<Configuration>;
  setConfigurationsList(value: Array<Configuration>): ConfigurationList;
  clearConfigurationsList(): ConfigurationList;
  addConfigurations(value?: Configuration, index?: number): Configuration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ConfigurationList.AsObject;
  static toObject(includeInstance: boolean, msg: ConfigurationList): ConfigurationList.AsObject;
  static serializeBinaryToWriter(message: ConfigurationList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ConfigurationList;
  static deserializeBinaryFromReader(message: ConfigurationList, reader: jspb.BinaryReader): ConfigurationList;
}

export namespace ConfigurationList {
  export type AsObject = {
    configurationsList: Array<Configuration.AsObject>,
  }
}

export class PanelEquipmentList extends jspb.Message {
  getPanelEquipmentsList(): Array<PanelEquipment>;
  setPanelEquipmentsList(value: Array<PanelEquipment>): PanelEquipmentList;
  clearPanelEquipmentsList(): PanelEquipmentList;
  addPanelEquipments(value?: PanelEquipment, index?: number): PanelEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PanelEquipmentList.AsObject;
  static toObject(includeInstance: boolean, msg: PanelEquipmentList): PanelEquipmentList.AsObject;
  static serializeBinaryToWriter(message: PanelEquipmentList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PanelEquipmentList;
  static deserializeBinaryFromReader(message: PanelEquipmentList, reader: jspb.BinaryReader): PanelEquipmentList;
}

export namespace PanelEquipmentList {
  export type AsObject = {
    panelEquipmentsList: Array<PanelEquipment.AsObject>,
  }
}

export class PanelEquipmentGroupList extends jspb.Message {
  getPanelEquipmentGroupsList(): Array<PanelEquipmentGroup>;
  setPanelEquipmentGroupsList(value: Array<PanelEquipmentGroup>): PanelEquipmentGroupList;
  clearPanelEquipmentGroupsList(): PanelEquipmentGroupList;
  addPanelEquipmentGroups(value?: PanelEquipmentGroup, index?: number): PanelEquipmentGroup;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PanelEquipmentGroupList.AsObject;
  static toObject(includeInstance: boolean, msg: PanelEquipmentGroupList): PanelEquipmentGroupList.AsObject;
  static serializeBinaryToWriter(message: PanelEquipmentGroupList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PanelEquipmentGroupList;
  static deserializeBinaryFromReader(message: PanelEquipmentGroupList, reader: jspb.BinaryReader): PanelEquipmentGroupList;
}

export namespace PanelEquipmentGroupList {
  export type AsObject = {
    panelEquipmentGroupsList: Array<PanelEquipmentGroup.AsObject>,
  }
}

