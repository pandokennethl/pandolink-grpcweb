import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_zonelocation$model_pb from '../../../../pando/api/alarm/v0/zonelocation-model_pb';
import * as pando_api_alarm_v0_zonelocation_pb from '../../../../pando/api/alarm/v0/zonelocation_pb';


export class ZoneLocationServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllModels(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  getModel(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.Model) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.Model>;

  getModelList(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  getAllDeviceTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList>;

  getDeviceTypeList(
    request: pando_api_alarm_v0_zonelocation_pb.GetDeviceTypeListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList>;

  getModelsFromDeviceType(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelByDeviceTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  findModelsByZoneLocation(
    request: pando_api_alarm_v0_zonelocation_pb.FindModelsByZoneLocationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonelocation$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

}

export class ZoneLocationServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllModels(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  getModel(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.Model>;

  getModelList(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  getAllDeviceTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList>;

  getDeviceTypeList(
    request: pando_api_alarm_v0_zonelocation_pb.GetDeviceTypeListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.DeviceTypeList>;

  getModelsFromDeviceType(
    request: pando_api_alarm_v0_zonelocation_pb.GetModelByDeviceTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

  findModelsByZoneLocation(
    request: pando_api_alarm_v0_zonelocation_pb.FindModelsByZoneLocationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonelocation$model_pb.ModelList>;

}

