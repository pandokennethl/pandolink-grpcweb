import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_zonetype$model_pb from '../../../../pando/api/alarm/v0/zonetype-model_pb';
import * as pando_api_alarm_v0_zonetype_pb from '../../../../pando/api/alarm/v0/zonetype_pb';


export class ZoneTypeServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  getType(
    request: pando_api_alarm_v0_zonetype_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.Model) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.Model>;

  getTypeList(
    request: pando_api_alarm_v0_zonetype_pb.GetTypeListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  createZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  updateZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  deleteZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_zonetype$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

}

export class ZoneTypeServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  getType(
    request: pando_api_alarm_v0_zonetype_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.Model>;

  getTypeList(
    request: pando_api_alarm_v0_zonetype_pb.GetTypeListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  createZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  updateZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

  deleteZoneType(
    request: pando_api_alarm_v0_zonetype$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_zonetype$model_pb.ModelList>;

}

