import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_clientsettings_pb from '../../../../pando/api/alarm/v0/clientsettings_pb';


export class ClientSettingsServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.GetClientSettingsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  getClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.GetClientSettingsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  addClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  updateClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  removeClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  addClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  updateClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  removeClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

}

export class ClientSettingsServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.GetClientSettingsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  getClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.GetClientSettingsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  addClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  updateClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  removeClientEquipment(
    request: pando_api_alarm_v0_clientsettings_pb.ClientEquipment,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientEquipmentResponse>;

  addClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  updateClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

  removeClientPanelType(
    request: pando_api_alarm_v0_clientsettings_pb.ClientPanelType,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_clientsettings_pb.GetClientPanelTypeResponse>;

}

