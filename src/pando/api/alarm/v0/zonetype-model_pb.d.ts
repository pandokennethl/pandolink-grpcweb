import * as jspb from 'google-protobuf'



export class Model extends jspb.Message {
  getZoneTypeGuid(): string;
  setZoneTypeGuid(value: string): Model;

  getZoneTypeName(): string;
  setZoneTypeName(value: string): Model;

  getZoneTypeDescription(): string;
  setZoneTypeDescription(value: string): Model;

  getZoneTypeValue(): string;
  setZoneTypeValue(value: string): Model;

  getEventCode(): string;
  setEventCode(value: string): Model;

  getCentralStationCode(): string;
  setCentralStationCode(value: string): Model;

  getIsActive(): boolean;
  setIsActive(value: boolean): Model;

  getValue1(): string;
  setValue1(value: string): Model;

  getValue2(): string;
  setValue2(value: string): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Model.AsObject;
  static toObject(includeInstance: boolean, msg: Model): Model.AsObject;
  static serializeBinaryToWriter(message: Model, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Model;
  static deserializeBinaryFromReader(message: Model, reader: jspb.BinaryReader): Model;
}

export namespace Model {
  export type AsObject = {
    zoneTypeGuid: string,
    zoneTypeName: string,
    zoneTypeDescription: string,
    zoneTypeValue: string,
    eventCode: string,
    centralStationCode: string,
    isActive: boolean,
    value1: string,
    value2: string,
  }
}

export class ModelList extends jspb.Message {
  getCentralStationCode(): string;
  setCentralStationCode(value: string): ModelList;

  getModelsList(): Array<Model>;
  setModelsList(value: Array<Model>): ModelList;
  clearModelsList(): ModelList;
  addModels(value?: Model, index?: number): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModelList.AsObject;
  static toObject(includeInstance: boolean, msg: ModelList): ModelList.AsObject;
  static serializeBinaryToWriter(message: ModelList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModelList;
  static deserializeBinaryFromReader(message: ModelList, reader: jspb.BinaryReader): ModelList;
}

export namespace ModelList {
  export type AsObject = {
    centralStationCode: string,
    modelsList: Array<Model.AsObject>,
  }
}

