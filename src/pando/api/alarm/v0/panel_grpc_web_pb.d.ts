import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_panel$model_pb from '../../../../pando/api/alarm/v0/panel-model_pb';
import * as pando_api_alarm_v0_panel_pb from '../../../../pando/api/alarm/v0/panel_pb';


export class PanelServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.TypeList>;

  getType(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.Type) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.Type>;

  getConfiguration(
    request: pando_api_alarm_v0_panel_pb.GetPanelConfigurationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.Configuration) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.Configuration>;

  getConfigurationFromGuildList(
    request: pando_api_alarm_v0_panel_pb.GetPanelConfigurationGuidListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  getConfigurationList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  getPanelEquipmentList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  getCompatibleEquipmentList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getPanelEquipmentGroups(
    request: pando_api_alarm_v0_panel_pb.GetPanelEquipmentGroupsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

  createType(
    request: pando_api_alarm_v0_panel$model_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_panel$model_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.TypeList>;

  createConfiguration(
    request: pando_api_alarm_v0_panel$model_pb.Configuration,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  updateConfiguration(
    request: pando_api_alarm_v0_panel$model_pb.Configuration,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  addPanelCompatibleEquipment(
    request: pando_api_alarm_v0_panel_pb.EditCompatibleEquipmentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  removePanelCompatibleEquipment(
    request: pando_api_alarm_v0_panel_pb.EditCompatibleEquipmentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  addPanelEquipment(
    request: pando_api_alarm_v0_panel_pb.EditPanelEquipmentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  removePanelEquipment(
    request: pando_api_alarm_v0_panel_pb.EditPanelEquipmentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  createPanelEquipmentGroup(
    request: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroup,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

  updatePanelEquipmentGroup(
    request: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroup,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

}

export class PanelServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.TypeList>;

  getType(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.Type>;

  getConfiguration(
    request: pando_api_alarm_v0_panel_pb.GetPanelConfigurationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.Configuration>;

  getConfigurationFromGuildList(
    request: pando_api_alarm_v0_panel_pb.GetPanelConfigurationGuidListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  getConfigurationList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  getPanelEquipmentList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  getCompatibleEquipmentList(
    request: pando_api_alarm_v0_panel_pb.GetPanelTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getPanelEquipmentGroups(
    request: pando_api_alarm_v0_panel_pb.GetPanelEquipmentGroupsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

  createType(
    request: pando_api_alarm_v0_panel$model_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_panel$model_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.TypeList>;

  createConfiguration(
    request: pando_api_alarm_v0_panel$model_pb.Configuration,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  updateConfiguration(
    request: pando_api_alarm_v0_panel$model_pb.Configuration,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.ConfigurationList>;

  addPanelCompatibleEquipment(
    request: pando_api_alarm_v0_panel_pb.EditCompatibleEquipmentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  removePanelCompatibleEquipment(
    request: pando_api_alarm_v0_panel_pb.EditCompatibleEquipmentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  addPanelEquipment(
    request: pando_api_alarm_v0_panel_pb.EditPanelEquipmentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  removePanelEquipment(
    request: pando_api_alarm_v0_panel_pb.EditPanelEquipmentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentList>;

  createPanelEquipmentGroup(
    request: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroup,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

  updatePanelEquipmentGroup(
    request: pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroup,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_panel$model_pb.PanelEquipmentGroupList>;

}

