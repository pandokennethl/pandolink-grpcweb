import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_centralstation_pb from '../../../../pando/api/alarm/v0/centralstation_pb';
import * as pando_api_alarm_v0_centralstationtemplate$model_pb from '../../../../pando/api/alarm/v0/centralstationtemplate-model_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class CentralStationServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.TypeList>;

  getAllConfigurations(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  getType(
    request: pando_api_alarm_v0_centralstation_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.Type) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.Type>;

  getTemplateList(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

  getConfiguration(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.Configuration) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.Configuration>;

  getConfigurationList(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  getConfigurationsFromType(
    request: pando_api_alarm_v0_centralstation_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  createType(
    request: pando_api_alarm_v0_centralstation_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_centralstation_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.TypeList>;

  createSetting(
    request: pando_api_alarm_v0_centralstation_pb.Setting,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.SettingList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.SettingList>;

  updateSetting(
    request: pando_api_alarm_v0_centralstation_pb.Setting,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.SettingList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.SettingList>;

  updateSettingValue(
    request: pando_api_alarm_v0_centralstation_pb.SettingValue,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstation_pb.Configuration) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstation_pb.Configuration>;

  createTemplate(
    request: pando_api_alarm_v0_centralstationtemplate$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

  updateTemplate(
    request: pando_api_alarm_v0_centralstationtemplate$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

}

export class CentralStationServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.TypeList>;

  getAllConfigurations(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  getType(
    request: pando_api_alarm_v0_centralstation_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.Type>;

  getTemplateList(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

  getConfiguration(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.Configuration>;

  getConfigurationList(
    request: pando_api_alarm_v0_centralstation_pb.GetConfigurationListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  getConfigurationsFromType(
    request: pando_api_alarm_v0_centralstation_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.ConfigurationList>;

  createType(
    request: pando_api_alarm_v0_centralstation_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_centralstation_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.TypeList>;

  createSetting(
    request: pando_api_alarm_v0_centralstation_pb.Setting,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.SettingList>;

  updateSetting(
    request: pando_api_alarm_v0_centralstation_pb.Setting,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.SettingList>;

  updateSettingValue(
    request: pando_api_alarm_v0_centralstation_pb.SettingValue,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstation_pb.Configuration>;

  createTemplate(
    request: pando_api_alarm_v0_centralstationtemplate$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

  updateTemplate(
    request: pando_api_alarm_v0_centralstationtemplate$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_centralstationtemplate$model_pb.ModelList>;

}

