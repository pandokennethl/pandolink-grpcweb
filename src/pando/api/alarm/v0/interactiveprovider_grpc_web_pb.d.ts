import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_interactiveprovider_pb from '../../../../pando/api/alarm/v0/interactiveprovider_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class InteractiveProviderServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  getAllConfigurations(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  getType(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.Type) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.Type>;

  getConfiguration(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetConfigurationRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.Configuration) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.Configuration>;

  getConfigurationList(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetConfigurationListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  getConfigurationFromType(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  createType(
    request: pando_api_alarm_v0_interactiveprovider_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_interactiveprovider_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  createSetting(
    request: pando_api_alarm_v0_interactiveprovider_pb.Setting,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.SettingList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.SettingList>;

  updateSetting(
    request: pando_api_alarm_v0_interactiveprovider_pb.Setting,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.SettingList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.SettingList>;

  updateSettingValue(
    request: pando_api_alarm_v0_interactiveprovider_pb.SettingValue,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_interactiveprovider_pb.Configuration) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_interactiveprovider_pb.Configuration>;

}

export class InteractiveProviderServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  getAllConfigurations(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  getType(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.Type>;

  getConfiguration(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetConfigurationRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.Configuration>;

  getConfigurationList(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetConfigurationListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  getConfigurationFromType(
    request: pando_api_alarm_v0_interactiveprovider_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.ConfigurationList>;

  createType(
    request: pando_api_alarm_v0_interactiveprovider_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_interactiveprovider_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.TypeList>;

  createSetting(
    request: pando_api_alarm_v0_interactiveprovider_pb.Setting,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.SettingList>;

  updateSetting(
    request: pando_api_alarm_v0_interactiveprovider_pb.Setting,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.SettingList>;

  updateSettingValue(
    request: pando_api_alarm_v0_interactiveprovider_pb.SettingValue,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_interactiveprovider_pb.Configuration>;

}

