import * as jspb from 'google-protobuf'



export class GetClientSettingsRequest extends jspb.Message {
  getAccessCode(): string;
  setAccessCode(value: string): GetClientSettingsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetClientSettingsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetClientSettingsRequest): GetClientSettingsRequest.AsObject;
  static serializeBinaryToWriter(message: GetClientSettingsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetClientSettingsRequest;
  static deserializeBinaryFromReader(message: GetClientSettingsRequest, reader: jspb.BinaryReader): GetClientSettingsRequest;
}

export namespace GetClientSettingsRequest {
  export type AsObject = {
    accessCode: string,
  }
}

export class GetClientEquipmentResponse extends jspb.Message {
  getEquipmentList(): Array<ClientEquipment>;
  setEquipmentList(value: Array<ClientEquipment>): GetClientEquipmentResponse;
  clearEquipmentList(): GetClientEquipmentResponse;
  addEquipment(value?: ClientEquipment, index?: number): ClientEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetClientEquipmentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetClientEquipmentResponse): GetClientEquipmentResponse.AsObject;
  static serializeBinaryToWriter(message: GetClientEquipmentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetClientEquipmentResponse;
  static deserializeBinaryFromReader(message: GetClientEquipmentResponse, reader: jspb.BinaryReader): GetClientEquipmentResponse;
}

export namespace GetClientEquipmentResponse {
  export type AsObject = {
    equipmentList: Array<ClientEquipment.AsObject>,
  }
}

export class GetClientPanelTypeResponse extends jspb.Message {
  getTypesList(): Array<ClientPanelType>;
  setTypesList(value: Array<ClientPanelType>): GetClientPanelTypeResponse;
  clearTypesList(): GetClientPanelTypeResponse;
  addTypes(value?: ClientPanelType, index?: number): ClientPanelType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetClientPanelTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetClientPanelTypeResponse): GetClientPanelTypeResponse.AsObject;
  static serializeBinaryToWriter(message: GetClientPanelTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetClientPanelTypeResponse;
  static deserializeBinaryFromReader(message: GetClientPanelTypeResponse, reader: jspb.BinaryReader): GetClientPanelTypeResponse;
}

export namespace GetClientPanelTypeResponse {
  export type AsObject = {
    typesList: Array<ClientPanelType.AsObject>,
  }
}

export class ClientEquipment extends jspb.Message {
  getAccessCode(): string;
  setAccessCode(value: string): ClientEquipment;

  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): ClientEquipment;

  getDisplayName(): string;
  setDisplayName(value: string): ClientEquipment;

  getDescription(): string;
  setDescription(value: string): ClientEquipment;

  getPartNumber(): string;
  setPartNumber(value: string): ClientEquipment;

  getCost(): string;
  setCost(value: string): ClientEquipment;

  getRetailPrice(): string;
  setRetailPrice(value: string): ClientEquipment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientEquipment.AsObject;
  static toObject(includeInstance: boolean, msg: ClientEquipment): ClientEquipment.AsObject;
  static serializeBinaryToWriter(message: ClientEquipment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientEquipment;
  static deserializeBinaryFromReader(message: ClientEquipment, reader: jspb.BinaryReader): ClientEquipment;
}

export namespace ClientEquipment {
  export type AsObject = {
    accessCode: string,
    equipmentGuid: string,
    displayName: string,
    description: string,
    partNumber: string,
    cost: string,
    retailPrice: string,
  }
}

export class ClientPanelType extends jspb.Message {
  getAccessCode(): string;
  setAccessCode(value: string): ClientPanelType;

  getPanelCode(): string;
  setPanelCode(value: string): ClientPanelType;

  getDisplayName(): string;
  setDisplayName(value: string): ClientPanelType;

  getAllowDiy(): string;
  setAllowDiy(value: string): ClientPanelType;

  getAllowExisting(): string;
  setAllowExisting(value: string): ClientPanelType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ClientPanelType.AsObject;
  static toObject(includeInstance: boolean, msg: ClientPanelType): ClientPanelType.AsObject;
  static serializeBinaryToWriter(message: ClientPanelType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ClientPanelType;
  static deserializeBinaryFromReader(message: ClientPanelType, reader: jspb.BinaryReader): ClientPanelType;
}

export namespace ClientPanelType {
  export type AsObject = {
    accessCode: string,
    panelCode: string,
    displayName: string,
    allowDiy: string,
    allowExisting: string,
  }
}

