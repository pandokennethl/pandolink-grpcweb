import * as jspb from 'google-protobuf'



export class Type extends jspb.Message {
  getSignalTypeGuid(): string;
  setSignalTypeGuid(value: string): Type;

  getSignalTypeName(): string;
  setSignalTypeName(value: string): Type;

  getSignalTypeDescription(): string;
  setSignalTypeDescription(value: string): Type;

  getIsActive(): boolean;
  setIsActive(value: boolean): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Type.AsObject;
  static toObject(includeInstance: boolean, msg: Type): Type.AsObject;
  static serializeBinaryToWriter(message: Type, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Type;
  static deserializeBinaryFromReader(message: Type, reader: jspb.BinaryReader): Type;
}

export namespace Type {
  export type AsObject = {
    signalTypeGuid: string,
    signalTypeName: string,
    signalTypeDescription: string,
    isActive: boolean,
  }
}

export class TypeList extends jspb.Message {
  getTypesList(): Array<Type>;
  setTypesList(value: Array<Type>): TypeList;
  clearTypesList(): TypeList;
  addTypes(value?: Type, index?: number): Type;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TypeList.AsObject;
  static toObject(includeInstance: boolean, msg: TypeList): TypeList.AsObject;
  static serializeBinaryToWriter(message: TypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TypeList;
  static deserializeBinaryFromReader(message: TypeList, reader: jspb.BinaryReader): TypeList;
}

export namespace TypeList {
  export type AsObject = {
    typesList: Array<Type.AsObject>,
  }
}

export class EquipmentType extends jspb.Message {
  getSignalTypeGuid(): string;
  setSignalTypeGuid(value: string): EquipmentType;

  getSignalTypeName(): string;
  setSignalTypeName(value: string): EquipmentType;

  getSignalTypeDescription(): string;
  setSignalTypeDescription(value: string): EquipmentType;

  getIsActive(): boolean;
  setIsActive(value: boolean): EquipmentType;

  getEquipmentGuidsList(): Array<string>;
  setEquipmentGuidsList(value: Array<string>): EquipmentType;
  clearEquipmentGuidsList(): EquipmentType;
  addEquipmentGuids(value: string, index?: number): EquipmentType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EquipmentType.AsObject;
  static toObject(includeInstance: boolean, msg: EquipmentType): EquipmentType.AsObject;
  static serializeBinaryToWriter(message: EquipmentType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EquipmentType;
  static deserializeBinaryFromReader(message: EquipmentType, reader: jspb.BinaryReader): EquipmentType;
}

export namespace EquipmentType {
  export type AsObject = {
    signalTypeGuid: string,
    signalTypeName: string,
    signalTypeDescription: string,
    isActive: boolean,
    equipmentGuidsList: Array<string>,
  }
}

export class EquipmentTypeList extends jspb.Message {
  getSignalTypeEquipmentList(): Array<EquipmentType>;
  setSignalTypeEquipmentList(value: Array<EquipmentType>): EquipmentTypeList;
  clearSignalTypeEquipmentList(): EquipmentTypeList;
  addSignalTypeEquipment(value?: EquipmentType, index?: number): EquipmentType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EquipmentTypeList.AsObject;
  static toObject(includeInstance: boolean, msg: EquipmentTypeList): EquipmentTypeList.AsObject;
  static serializeBinaryToWriter(message: EquipmentTypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EquipmentTypeList;
  static deserializeBinaryFromReader(message: EquipmentTypeList, reader: jspb.BinaryReader): EquipmentTypeList;
}

export namespace EquipmentTypeList {
  export type AsObject = {
    signalTypeEquipmentList: Array<EquipmentType.AsObject>,
  }
}

