import * as jspb from 'google-protobuf'



export class Model extends jspb.Message {
  getZoneLocationGuid(): string;
  setZoneLocationGuid(value: string): Model;

  getZoneLocationExpression(): string;
  setZoneLocationExpression(value: string): Model;

  getFirstNumber(): number;
  setFirstNumber(value: number): Model;

  getPanelCode(): string;
  setPanelCode(value: string): Model;

  getIsActive(): boolean;
  setIsActive(value: boolean): Model;

  getNameLookup(): string;
  setNameLookup(value: string): Model;

  getZoneTypeStartValue(): string;
  setZoneTypeStartValue(value: string): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Model.AsObject;
  static toObject(includeInstance: boolean, msg: Model): Model.AsObject;
  static serializeBinaryToWriter(message: Model, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Model;
  static deserializeBinaryFromReader(message: Model, reader: jspb.BinaryReader): Model;
}

export namespace Model {
  export type AsObject = {
    zoneLocationGuid: string,
    zoneLocationExpression: string,
    firstNumber: number,
    panelCode: string,
    isActive: boolean,
    nameLookup: string,
    zoneTypeStartValue: string,
  }
}

export class DeviceType extends jspb.Message {
  getDeviceTypeCode(): string;
  setDeviceTypeCode(value: string): DeviceType;

  getDisplayName(): string;
  setDisplayName(value: string): DeviceType;

  getIsActive(): boolean;
  setIsActive(value: boolean): DeviceType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceType.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceType): DeviceType.AsObject;
  static serializeBinaryToWriter(message: DeviceType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceType;
  static deserializeBinaryFromReader(message: DeviceType, reader: jspb.BinaryReader): DeviceType;
}

export namespace DeviceType {
  export type AsObject = {
    deviceTypeCode: string,
    displayName: string,
    isActive: boolean,
  }
}

export class ModelList extends jspb.Message {
  getModelsList(): Array<Model>;
  setModelsList(value: Array<Model>): ModelList;
  clearModelsList(): ModelList;
  addModels(value?: Model, index?: number): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModelList.AsObject;
  static toObject(includeInstance: boolean, msg: ModelList): ModelList.AsObject;
  static serializeBinaryToWriter(message: ModelList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModelList;
  static deserializeBinaryFromReader(message: ModelList, reader: jspb.BinaryReader): ModelList;
}

export namespace ModelList {
  export type AsObject = {
    modelsList: Array<Model.AsObject>,
  }
}

export class DeviceTypeList extends jspb.Message {
  getDeviceTypesList(): Array<DeviceType>;
  setDeviceTypesList(value: Array<DeviceType>): DeviceTypeList;
  clearDeviceTypesList(): DeviceTypeList;
  addDeviceTypes(value?: DeviceType, index?: number): DeviceType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeviceTypeList.AsObject;
  static toObject(includeInstance: boolean, msg: DeviceTypeList): DeviceTypeList.AsObject;
  static serializeBinaryToWriter(message: DeviceTypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeviceTypeList;
  static deserializeBinaryFromReader(message: DeviceTypeList, reader: jspb.BinaryReader): DeviceTypeList;
}

export namespace DeviceTypeList {
  export type AsObject = {
    deviceTypesList: Array<DeviceType.AsObject>,
  }
}

