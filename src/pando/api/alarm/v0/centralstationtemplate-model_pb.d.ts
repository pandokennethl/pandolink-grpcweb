import * as jspb from 'google-protobuf'



export class Model extends jspb.Message {
  getCentralStationTemplateGuid(): string;
  setCentralStationTemplateGuid(value: string): Model;

  getClientCentralStationGuid(): string;
  setClientCentralStationGuid(value: string): Model;

  getPanelCode(): string;
  setPanelCode(value: string): Model;

  getTemplateValue(): string;
  setTemplateValue(value: string): Model;

  getIsEcv(): boolean;
  setIsEcv(value: boolean): Model;

  getIsActive(): boolean;
  setIsActive(value: boolean): Model;

  getFlag1(): boolean;
  setFlag1(value: boolean): Model;

  getFlag2(): boolean;
  setFlag2(value: boolean): Model;

  getValue1(): string;
  setValue1(value: string): Model;

  getValue2(): string;
  setValue2(value: string): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Model.AsObject;
  static toObject(includeInstance: boolean, msg: Model): Model.AsObject;
  static serializeBinaryToWriter(message: Model, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Model;
  static deserializeBinaryFromReader(message: Model, reader: jspb.BinaryReader): Model;
}

export namespace Model {
  export type AsObject = {
    centralStationTemplateGuid: string,
    clientCentralStationGuid: string,
    panelCode: string,
    templateValue: string,
    isEcv: boolean,
    isActive: boolean,
    flag1: boolean,
    flag2: boolean,
    value1: string,
    value2: string,
  }
}

export class ModelList extends jspb.Message {
  getClientCentralStationGuid(): string;
  setClientCentralStationGuid(value: string): ModelList;

  getModelsList(): Array<Model>;
  setModelsList(value: Array<Model>): ModelList;
  clearModelsList(): ModelList;
  addModels(value?: Model, index?: number): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModelList.AsObject;
  static toObject(includeInstance: boolean, msg: ModelList): ModelList.AsObject;
  static serializeBinaryToWriter(message: ModelList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModelList;
  static deserializeBinaryFromReader(message: ModelList, reader: jspb.BinaryReader): ModelList;
}

export namespace ModelList {
  export type AsObject = {
    clientCentralStationGuid: string,
    modelsList: Array<Model.AsObject>,
  }
}

