import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_signal$model_pb from '../../../../pando/api/alarm/v0/signal-model_pb';
import * as pando_api_alarm_v0_signal_pb from '../../../../pando/api/alarm/v0/signal_pb';


export class SignalServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getType(
    request: pando_api_alarm_v0_signal_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.Type) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.Type>;

  getTypeList(
    request: pando_api_alarm_v0_signal_pb.GetTypeListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getTypeListFromEquipmentGuid(
    request: pando_api_alarm_v0_signal_pb.GetTypeListFromEquipmentGuidRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.EquipmentTypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.EquipmentTypeList>;

  getEquipmentModelList(
    request: pando_api_alarm_v0_signal_pb.GetTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  createType(
    request: pando_api_alarm_v0_signal$model_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_signal$model_pb.Type,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

}

export class SignalServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllTypes(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getType(
    request: pando_api_alarm_v0_signal_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.Type>;

  getTypeList(
    request: pando_api_alarm_v0_signal_pb.GetTypeListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getTypeListFromEquipmentGuid(
    request: pando_api_alarm_v0_signal_pb.GetTypeListFromEquipmentGuidRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.EquipmentTypeList>;

  getEquipmentModelList(
    request: pando_api_alarm_v0_signal_pb.GetTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  createType(
    request: pando_api_alarm_v0_signal$model_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  updateType(
    request: pando_api_alarm_v0_signal$model_pb.Type,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

}

