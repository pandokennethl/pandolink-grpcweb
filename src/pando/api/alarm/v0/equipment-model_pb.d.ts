import * as jspb from 'google-protobuf'



export class Model extends jspb.Message {
  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): Model;

  getPartNumber(): string;
  setPartNumber(value: string): Model;

  getDisplayName(): string;
  setDisplayName(value: string): Model;

  getDescription(): string;
  setDescription(value: string): Model;

  getManufacturer(): string;
  setManufacturer(value: string): Model;

  getCost(): number;
  setCost(value: number): Model;

  getRetailPrice(): number;
  setRetailPrice(value: number): Model;

  getIsActive(): boolean;
  setIsActive(value: boolean): Model;

  getDateCreated(): string;
  setDateCreated(value: string): Model;

  getDateLastUpdated(): string;
  setDateLastUpdated(value: string): Model;

  getAllowExisting(): boolean;
  setAllowExisting(value: boolean): Model;

  getShippingWeightLbs(): number;
  setShippingWeightLbs(value: number): Model;

  getIsProgrammable(): boolean;
  setIsProgrammable(value: boolean): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Model.AsObject;
  static toObject(includeInstance: boolean, msg: Model): Model.AsObject;
  static serializeBinaryToWriter(message: Model, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Model;
  static deserializeBinaryFromReader(message: Model, reader: jspb.BinaryReader): Model;
}

export namespace Model {
  export type AsObject = {
    equipmentGuid: string,
    partNumber: string,
    displayName: string,
    description: string,
    manufacturer: string,
    cost: number,
    retailPrice: number,
    isActive: boolean,
    dateCreated: string,
    dateLastUpdated: string,
    allowExisting: boolean,
    shippingWeightLbs: number,
    isProgrammable: boolean,
  }
}

export class Extension extends jspb.Message {
  getColumnName(): string;
  setColumnName(value: string): Extension;

  getColumnType(): string;
  setColumnType(value: string): Extension;

  getColumnDescription(): string;
  setColumnDescription(value: string): Extension;

  getIsActive(): boolean;
  setIsActive(value: boolean): Extension;

  getDefaultValue(): string;
  setDefaultValue(value: string): Extension;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Extension.AsObject;
  static toObject(includeInstance: boolean, msg: Extension): Extension.AsObject;
  static serializeBinaryToWriter(message: Extension, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Extension;
  static deserializeBinaryFromReader(message: Extension, reader: jspb.BinaryReader): Extension;
}

export namespace Extension {
  export type AsObject = {
    columnName: string,
    columnType: string,
    columnDescription: string,
    isActive: boolean,
    defaultValue: string,
  }
}

export class ExtensionValue extends jspb.Message {
  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): ExtensionValue;

  getColumnName(): string;
  setColumnName(value: string): ExtensionValue;

  getColumnValue(): string;
  setColumnValue(value: string): ExtensionValue;

  getIsActive(): boolean;
  setIsActive(value: boolean): ExtensionValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExtensionValue.AsObject;
  static toObject(includeInstance: boolean, msg: ExtensionValue): ExtensionValue.AsObject;
  static serializeBinaryToWriter(message: ExtensionValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExtensionValue;
  static deserializeBinaryFromReader(message: ExtensionValue, reader: jspb.BinaryReader): ExtensionValue;
}

export namespace ExtensionValue {
  export type AsObject = {
    equipmentGuid: string,
    columnName: string,
    columnValue: string,
    isActive: boolean,
  }
}

export class ModelList extends jspb.Message {
  getEquipmentListList(): Array<Model>;
  setEquipmentListList(value: Array<Model>): ModelList;
  clearEquipmentListList(): ModelList;
  addEquipmentList(value?: Model, index?: number): Model;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ModelList.AsObject;
  static toObject(includeInstance: boolean, msg: ModelList): ModelList.AsObject;
  static serializeBinaryToWriter(message: ModelList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ModelList;
  static deserializeBinaryFromReader(message: ModelList, reader: jspb.BinaryReader): ModelList;
}

export namespace ModelList {
  export type AsObject = {
    equipmentListList: Array<Model.AsObject>,
  }
}

export class ExtensionList extends jspb.Message {
  getExtensionsList(): Array<Extension>;
  setExtensionsList(value: Array<Extension>): ExtensionList;
  clearExtensionsList(): ExtensionList;
  addExtensions(value?: Extension, index?: number): Extension;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExtensionList.AsObject;
  static toObject(includeInstance: boolean, msg: ExtensionList): ExtensionList.AsObject;
  static serializeBinaryToWriter(message: ExtensionList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExtensionList;
  static deserializeBinaryFromReader(message: ExtensionList, reader: jspb.BinaryReader): ExtensionList;
}

export namespace ExtensionList {
  export type AsObject = {
    extensionsList: Array<Extension.AsObject>,
  }
}

export class ExtensionValueList extends jspb.Message {
  getEquipmentGuid(): string;
  setEquipmentGuid(value: string): ExtensionValueList;

  getExtensionValuesList(): Array<ExtensionValue>;
  setExtensionValuesList(value: Array<ExtensionValue>): ExtensionValueList;
  clearExtensionValuesList(): ExtensionValueList;
  addExtensionValues(value?: ExtensionValue, index?: number): ExtensionValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExtensionValueList.AsObject;
  static toObject(includeInstance: boolean, msg: ExtensionValueList): ExtensionValueList.AsObject;
  static serializeBinaryToWriter(message: ExtensionValueList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExtensionValueList;
  static deserializeBinaryFromReader(message: ExtensionValueList, reader: jspb.BinaryReader): ExtensionValueList;
}

export namespace ExtensionValueList {
  export type AsObject = {
    equipmentGuid: string,
    extensionValuesList: Array<ExtensionValue.AsObject>,
  }
}

