import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_utility_pb from '../../../../pando/api/alarm/v0/utility_pb';


export class UtilityServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  ping(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_utility_pb.OkResult) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_utility_pb.OkResult>;

  clearCache(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_utility_pb.OkResult) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_utility_pb.OkResult>;

}

export class UtilityServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  ping(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_utility_pb.OkResult>;

  clearCache(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_utility_pb.OkResult>;

}

