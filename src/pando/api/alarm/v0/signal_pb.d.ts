import * as jspb from 'google-protobuf'

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_alarm_v0_signal$model_pb from '../../../../pando/api/alarm/v0/signal-model_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class GetTypeRequest extends jspb.Message {
  getSignalTypeGuid(): string;
  setSignalTypeGuid(value: string): GetTypeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeRequest): GetTypeRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeRequest;
  static deserializeBinaryFromReader(message: GetTypeRequest, reader: jspb.BinaryReader): GetTypeRequest;
}

export namespace GetTypeRequest {
  export type AsObject = {
    signalTypeGuid: string,
    includeInactive: boolean,
  }
}

export class GetTypeListRequest extends jspb.Message {
  getSignalTypeGuidsList(): Array<string>;
  setSignalTypeGuidsList(value: Array<string>): GetTypeListRequest;
  clearSignalTypeGuidsList(): GetTypeListRequest;
  addSignalTypeGuids(value: string, index?: number): GetTypeListRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeListRequest): GetTypeListRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeListRequest;
  static deserializeBinaryFromReader(message: GetTypeListRequest, reader: jspb.BinaryReader): GetTypeListRequest;
}

export namespace GetTypeListRequest {
  export type AsObject = {
    signalTypeGuidsList: Array<string>,
    includeInactive: boolean,
  }
}

export class GetTypeListFromEquipmentGuidRequest extends jspb.Message {
  getEquipmentGuidsList(): Array<string>;
  setEquipmentGuidsList(value: Array<string>): GetTypeListFromEquipmentGuidRequest;
  clearEquipmentGuidsList(): GetTypeListFromEquipmentGuidRequest;
  addEquipmentGuids(value: string, index?: number): GetTypeListFromEquipmentGuidRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): GetTypeListFromEquipmentGuidRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeListFromEquipmentGuidRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeListFromEquipmentGuidRequest): GetTypeListFromEquipmentGuidRequest.AsObject;
  static serializeBinaryToWriter(message: GetTypeListFromEquipmentGuidRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeListFromEquipmentGuidRequest;
  static deserializeBinaryFromReader(message: GetTypeListFromEquipmentGuidRequest, reader: jspb.BinaryReader): GetTypeListFromEquipmentGuidRequest;
}

export namespace GetTypeListFromEquipmentGuidRequest {
  export type AsObject = {
    equipmentGuidsList: Array<string>,
    includeInactive: boolean,
  }
}

