import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class OkResult extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): OkResult;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OkResult.AsObject;
  static toObject(includeInstance: boolean, msg: OkResult): OkResult.AsObject;
  static serializeBinaryToWriter(message: OkResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OkResult;
  static deserializeBinaryFromReader(message: OkResult, reader: jspb.BinaryReader): OkResult;
}

export namespace OkResult {
  export type AsObject = {
    isOk: boolean,
  }
}

