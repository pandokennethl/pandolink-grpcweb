import * as grpcWeb from 'grpc-web';

import * as pando_api_alarm_v0_equipment$model_pb from '../../../../pando/api/alarm/v0/equipment-model_pb';
import * as pando_api_alarm_v0_equipment_pb from '../../../../pando/api/alarm/v0/equipment_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_alarm_v0_signal$model_pb from '../../../../pando/api/alarm/v0/signal-model_pb';


export class EquipmentServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllModels(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getExtensionList(
    request: pando_api_shared_pb.Empty,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ExtensionList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  getModel(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.Model) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.Model>;

  getModelList(
    request: pando_api_alarm_v0_equipment_pb.GetModelListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getSignalList(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getExtensionValueList(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ExtensionValueList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ExtensionValueList>;

  createModel(
    request: pando_api_alarm_v0_equipment$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  updateModel(
    request: pando_api_alarm_v0_equipment$model_pb.Model,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  createExtension(
    request: pando_api_alarm_v0_equipment$model_pb.Extension,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ExtensionList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  updateExtension(
    request: pando_api_alarm_v0_equipment$model_pb.Extension,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ExtensionList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  updateExtensionValue(
    request: pando_api_alarm_v0_equipment$model_pb.ExtensionValue,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ExtensionValueList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ExtensionValueList>;

  addSignal(
    request: pando_api_alarm_v0_equipment_pb.EditSignalsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  removeSignal(
    request: pando_api_alarm_v0_equipment_pb.EditSignalsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_signal$model_pb.TypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getEquipmentByExtension(
    request: pando_api_alarm_v0_equipment_pb.GetEquipmentByExtensionRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_alarm_v0_equipment$model_pb.ModelList) => void
  ): grpcWeb.ClientReadableStream<pando_api_alarm_v0_equipment$model_pb.ModelList>;

}

export class EquipmentServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getAllModels(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getExtensionList(
    request: pando_api_shared_pb.Empty,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  getModel(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.Model>;

  getModelList(
    request: pando_api_alarm_v0_equipment_pb.GetModelListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  getSignalList(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getExtensionValueList(
    request: pando_api_alarm_v0_equipment_pb.GetRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ExtensionValueList>;

  createModel(
    request: pando_api_alarm_v0_equipment$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  updateModel(
    request: pando_api_alarm_v0_equipment$model_pb.Model,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

  createExtension(
    request: pando_api_alarm_v0_equipment$model_pb.Extension,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  updateExtension(
    request: pando_api_alarm_v0_equipment$model_pb.Extension,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ExtensionList>;

  updateExtensionValue(
    request: pando_api_alarm_v0_equipment$model_pb.ExtensionValue,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ExtensionValueList>;

  addSignal(
    request: pando_api_alarm_v0_equipment_pb.EditSignalsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  removeSignal(
    request: pando_api_alarm_v0_equipment_pb.EditSignalsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_signal$model_pb.TypeList>;

  getEquipmentByExtension(
    request: pando_api_alarm_v0_equipment_pb.GetEquipmentByExtensionRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_alarm_v0_equipment$model_pb.ModelList>;

}

