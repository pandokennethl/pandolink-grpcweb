import * as jspb from 'google-protobuf'



export class ResourceClaimed extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): ResourceClaimed;

  getClaimCode(): string;
  setClaimCode(value: string): ResourceClaimed;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourceClaimed.AsObject;
  static toObject(includeInstance: boolean, msg: ResourceClaimed): ResourceClaimed.AsObject;
  static serializeBinaryToWriter(message: ResourceClaimed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourceClaimed;
  static deserializeBinaryFromReader(message: ResourceClaimed, reader: jspb.BinaryReader): ResourceClaimed;
}

export namespace ResourceClaimed {
  export type AsObject = {
    userId: string,
    claimCode: string,
  }
}

export enum SystemEvents { 
  RESOURCE_CLAIMED = 0,
}
