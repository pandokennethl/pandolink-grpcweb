import * as jspb from 'google-protobuf'

import * as google_protobuf_descriptor_pb from 'google-protobuf/google/protobuf/descriptor_pb';


export class EventMetadata extends jspb.Message {
  getDataSchema(): string;
  setDataSchema(value: string): EventMetadata;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventMetadata.AsObject;
  static toObject(includeInstance: boolean, msg: EventMetadata): EventMetadata.AsObject;
  static serializeBinaryToWriter(message: EventMetadata, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventMetadata;
  static deserializeBinaryFromReader(message: EventMetadata, reader: jspb.BinaryReader): EventMetadata;
}

export namespace EventMetadata {
  export type AsObject = {
    dataSchema: string,
  }
}

