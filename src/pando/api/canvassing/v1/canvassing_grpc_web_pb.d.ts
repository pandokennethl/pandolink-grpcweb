import * as grpcWeb from 'grpc-web';

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as pando_api_canvassing_v1_canvassing_pb from '../../../../pando/api/canvassing/v1/canvassing_pb';


export class CanvassingServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createArea(
    request: pando_api_canvassing_v1_canvassing_pb.CreateAreaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Area) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Area>;

  deleteArea(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getArea(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Area) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Area>;

  updateArea(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateAreaRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Area) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Area>;

  execAssignAreaMember(
    request: pando_api_canvassing_v1_canvassing_pb.AssignMemberRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.AreaMember) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.AreaMember>;

  execUnassignAreaMember(
    request: pando_api_canvassing_v1_canvassing_pb.AssignMemberRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  createAreaNote(
    request: pando_api_canvassing_v1_canvassing_pb.CreateAreaNoteRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.AreaNote) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.AreaNote>;

  getAreaNoteList(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.AreaNoteList) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.AreaNoteList>;

  deleteAreaNote(
    request: pando_api_canvassing_v1_canvassing_pb.NoteIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  createLead(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Lead) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Lead>;

  updateLead(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Lead) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Lead>;

  getLead(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Lead) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Lead>;

  deleteLead(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  execSetLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.SetLeadTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.Lead) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.Lead>;

  createLeadNote(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadNoteRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadNote) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadNote>;

  deleteLeadNote(
    request: pando_api_canvassing_v1_canvassing_pb.LeadNoteIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getLeadNoteList(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadNoteList) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadNoteList>;

  createLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadType) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  getLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadType) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  getLeadTypeList(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeListRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadTypeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadTypeList>;

  updateLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadTypeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadType) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  deleteLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getAreasWithinBounds(
    request: pando_api_canvassing_v1_canvassing_pb.BoundsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.AreaBoundsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.AreaBoundsResponse>;

  getLeadsWithinBounds(
    request: pando_api_canvassing_v1_canvassing_pb.BoundsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadBoundsResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadBoundsResponse>;

  createLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadAttributeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadAttribute) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadAttribute>;

  updateLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadAttributeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadAttribute) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadAttribute>;

  deleteLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.LeadAttributeIdRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_shared_pb.Empty) => void
  ): grpcWeb.ClientReadableStream<pando_api_shared_pb.Empty>;

  getLeadAttributeList(
    request: pando_api_canvassing_v1_canvassing_pb.ListLeadAttributeRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_canvassing_v1_canvassing_pb.LeadAttributeList) => void
  ): grpcWeb.ClientReadableStream<pando_api_canvassing_v1_canvassing_pb.LeadAttributeList>;

}

export class CanvassingServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  createArea(
    request: pando_api_canvassing_v1_canvassing_pb.CreateAreaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Area>;

  deleteArea(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getArea(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Area>;

  updateArea(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateAreaRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Area>;

  execAssignAreaMember(
    request: pando_api_canvassing_v1_canvassing_pb.AssignMemberRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.AreaMember>;

  execUnassignAreaMember(
    request: pando_api_canvassing_v1_canvassing_pb.AssignMemberRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  createAreaNote(
    request: pando_api_canvassing_v1_canvassing_pb.CreateAreaNoteRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.AreaNote>;

  getAreaNoteList(
    request: pando_api_canvassing_v1_canvassing_pb.AreaIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.AreaNoteList>;

  deleteAreaNote(
    request: pando_api_canvassing_v1_canvassing_pb.NoteIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  createLead(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Lead>;

  updateLead(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Lead>;

  getLead(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Lead>;

  deleteLead(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  execSetLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.SetLeadTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.Lead>;

  createLeadNote(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadNoteRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadNote>;

  deleteLeadNote(
    request: pando_api_canvassing_v1_canvassing_pb.LeadNoteIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getLeadNoteList(
    request: pando_api_canvassing_v1_canvassing_pb.LeadIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadNoteList>;

  createLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  getLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  getLeadTypeList(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeListRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadTypeList>;

  updateLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadTypeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadType>;

  deleteLeadType(
    request: pando_api_canvassing_v1_canvassing_pb.LeadTypeIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getAreasWithinBounds(
    request: pando_api_canvassing_v1_canvassing_pb.BoundsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.AreaBoundsResponse>;

  getLeadsWithinBounds(
    request: pando_api_canvassing_v1_canvassing_pb.BoundsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadBoundsResponse>;

  createLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.CreateLeadAttributeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadAttribute>;

  updateLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.UpdateLeadAttributeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadAttribute>;

  deleteLeadAttribute(
    request: pando_api_canvassing_v1_canvassing_pb.LeadAttributeIdRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_shared_pb.Empty>;

  getLeadAttributeList(
    request: pando_api_canvassing_v1_canvassing_pb.ListLeadAttributeRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_canvassing_v1_canvassing_pb.LeadAttributeList>;

}

