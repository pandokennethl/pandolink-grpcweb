import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as pando_api_extensions_pb from '../../../../pando/api/extensions_pb';
import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';


export class LeadAttributeList extends jspb.Message {
  getAttributesList(): Array<LeadAttribute>;
  setAttributesList(value: Array<LeadAttribute>): LeadAttributeList;
  clearAttributesList(): LeadAttributeList;
  addAttributes(value?: LeadAttribute, index?: number): LeadAttribute;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadAttributeList.AsObject;
  static toObject(includeInstance: boolean, msg: LeadAttributeList): LeadAttributeList.AsObject;
  static serializeBinaryToWriter(message: LeadAttributeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadAttributeList;
  static deserializeBinaryFromReader(message: LeadAttributeList, reader: jspb.BinaryReader): LeadAttributeList;
}

export namespace LeadAttributeList {
  export type AsObject = {
    attributesList: Array<LeadAttribute.AsObject>,
  }
}

export class LeadAttributeIdRequest extends jspb.Message {
  getLeadAttributeId(): number;
  setLeadAttributeId(value: number): LeadAttributeIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadAttributeIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeadAttributeIdRequest): LeadAttributeIdRequest.AsObject;
  static serializeBinaryToWriter(message: LeadAttributeIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadAttributeIdRequest;
  static deserializeBinaryFromReader(message: LeadAttributeIdRequest, reader: jspb.BinaryReader): LeadAttributeIdRequest;
}

export namespace LeadAttributeIdRequest {
  export type AsObject = {
    leadAttributeId: number,
  }
}

export class ListLeadAttributeRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): ListLeadAttributeRequest;

  getIncludeInactive(): boolean;
  setIncludeInactive(value: boolean): ListLeadAttributeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListLeadAttributeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListLeadAttributeRequest): ListLeadAttributeRequest.AsObject;
  static serializeBinaryToWriter(message: ListLeadAttributeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListLeadAttributeRequest;
  static deserializeBinaryFromReader(message: ListLeadAttributeRequest, reader: jspb.BinaryReader): ListLeadAttributeRequest;
}

export namespace ListLeadAttributeRequest {
  export type AsObject = {
    organizationCode: string,
    includeInactive: boolean,
  }
}

export class CreateLeadAttributeRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateLeadAttributeRequest;

  getName(): string;
  setName(value: string): CreateLeadAttributeRequest;

  getType(): LeadAttributeType;
  setType(value: LeadAttributeType): CreateLeadAttributeRequest;

  getActive(): boolean;
  setActive(value: boolean): CreateLeadAttributeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLeadAttributeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLeadAttributeRequest): CreateLeadAttributeRequest.AsObject;
  static serializeBinaryToWriter(message: CreateLeadAttributeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLeadAttributeRequest;
  static deserializeBinaryFromReader(message: CreateLeadAttributeRequest, reader: jspb.BinaryReader): CreateLeadAttributeRequest;
}

export namespace CreateLeadAttributeRequest {
  export type AsObject = {
    organizationCode: string,
    name: string,
    type: LeadAttributeType,
    active: boolean,
  }
}

export class UpdateLeadAttributeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): UpdateLeadAttributeRequest;

  getActive(): string;
  setActive(value: string): UpdateLeadAttributeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateLeadAttributeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateLeadAttributeRequest): UpdateLeadAttributeRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateLeadAttributeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateLeadAttributeRequest;
  static deserializeBinaryFromReader(message: UpdateLeadAttributeRequest, reader: jspb.BinaryReader): UpdateLeadAttributeRequest;
}

export namespace UpdateLeadAttributeRequest {
  export type AsObject = {
    id: number,
    active: string,
  }
}

export class LeadAttribute extends jspb.Message {
  getId(): number;
  setId(value: number): LeadAttribute;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): LeadAttribute;

  getName(): string;
  setName(value: string): LeadAttribute;

  getType(): LeadAttributeType;
  setType(value: LeadAttributeType): LeadAttribute;

  getActive(): boolean;
  setActive(value: boolean): LeadAttribute;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadAttribute.AsObject;
  static toObject(includeInstance: boolean, msg: LeadAttribute): LeadAttribute.AsObject;
  static serializeBinaryToWriter(message: LeadAttribute, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadAttribute;
  static deserializeBinaryFromReader(message: LeadAttribute, reader: jspb.BinaryReader): LeadAttribute;
}

export namespace LeadAttribute {
  export type AsObject = {
    id: number,
    organizationCode: string,
    name: string,
    type: LeadAttributeType,
    active: boolean,
  }
}

export class LeadAttributeValue extends jspb.Message {
  getAttributeId(): number;
  setAttributeId(value: number): LeadAttributeValue;

  getValue(): string;
  setValue(value: string): LeadAttributeValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadAttributeValue.AsObject;
  static toObject(includeInstance: boolean, msg: LeadAttributeValue): LeadAttributeValue.AsObject;
  static serializeBinaryToWriter(message: LeadAttributeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadAttributeValue;
  static deserializeBinaryFromReader(message: LeadAttributeValue, reader: jspb.BinaryReader): LeadAttributeValue;
}

export namespace LeadAttributeValue {
  export type AsObject = {
    attributeId: number,
    value: string,
  }
}

export class LeadTypeListRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): LeadTypeListRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadTypeListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeadTypeListRequest): LeadTypeListRequest.AsObject;
  static serializeBinaryToWriter(message: LeadTypeListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadTypeListRequest;
  static deserializeBinaryFromReader(message: LeadTypeListRequest, reader: jspb.BinaryReader): LeadTypeListRequest;
}

export namespace LeadTypeListRequest {
  export type AsObject = {
    organizationCode: string,
  }
}

export class LeadTypeList extends jspb.Message {
  getLeadTypesList(): Array<LeadType>;
  setLeadTypesList(value: Array<LeadType>): LeadTypeList;
  clearLeadTypesList(): LeadTypeList;
  addLeadTypes(value?: LeadType, index?: number): LeadType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadTypeList.AsObject;
  static toObject(includeInstance: boolean, msg: LeadTypeList): LeadTypeList.AsObject;
  static serializeBinaryToWriter(message: LeadTypeList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadTypeList;
  static deserializeBinaryFromReader(message: LeadTypeList, reader: jspb.BinaryReader): LeadTypeList;
}

export namespace LeadTypeList {
  export type AsObject = {
    leadTypesList: Array<LeadType.AsObject>,
  }
}

export class SetLeadTypeRequest extends jspb.Message {
  getLeadId(): number;
  setLeadId(value: number): SetLeadTypeRequest;

  getTypeId(): number;
  setTypeId(value: number): SetLeadTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetLeadTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetLeadTypeRequest): SetLeadTypeRequest.AsObject;
  static serializeBinaryToWriter(message: SetLeadTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetLeadTypeRequest;
  static deserializeBinaryFromReader(message: SetLeadTypeRequest, reader: jspb.BinaryReader): SetLeadTypeRequest;
}

export namespace SetLeadTypeRequest {
  export type AsObject = {
    leadId: number,
    typeId: number,
  }
}

export class LeadNoteList extends jspb.Message {
  getNotesList(): Array<LeadNote>;
  setNotesList(value: Array<LeadNote>): LeadNoteList;
  clearNotesList(): LeadNoteList;
  addNotes(value?: LeadNote, index?: number): LeadNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadNoteList.AsObject;
  static toObject(includeInstance: boolean, msg: LeadNoteList): LeadNoteList.AsObject;
  static serializeBinaryToWriter(message: LeadNoteList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadNoteList;
  static deserializeBinaryFromReader(message: LeadNoteList, reader: jspb.BinaryReader): LeadNoteList;
}

export namespace LeadNoteList {
  export type AsObject = {
    notesList: Array<LeadNote.AsObject>,
  }
}

export class Group extends jspb.Message {
  getCount(): number;
  setCount(value: number): Group;

  getCoordinate(): Coordinate | undefined;
  setCoordinate(value?: Coordinate): Group;
  hasCoordinate(): boolean;
  clearCoordinate(): Group;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Group.AsObject;
  static toObject(includeInstance: boolean, msg: Group): Group.AsObject;
  static serializeBinaryToWriter(message: Group, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Group;
  static deserializeBinaryFromReader(message: Group, reader: jspb.BinaryReader): Group;
}

export namespace Group {
  export type AsObject = {
    count: number,
    coordinate?: Coordinate.AsObject,
  }
}

export class AreaBoundsResponse extends jspb.Message {
  getItemsList(): Array<AreaBoundsResponseItem>;
  setItemsList(value: Array<AreaBoundsResponseItem>): AreaBoundsResponse;
  clearItemsList(): AreaBoundsResponse;
  addItems(value?: AreaBoundsResponseItem, index?: number): AreaBoundsResponseItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaBoundsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AreaBoundsResponse): AreaBoundsResponse.AsObject;
  static serializeBinaryToWriter(message: AreaBoundsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaBoundsResponse;
  static deserializeBinaryFromReader(message: AreaBoundsResponse, reader: jspb.BinaryReader): AreaBoundsResponse;
}

export namespace AreaBoundsResponse {
  export type AsObject = {
    itemsList: Array<AreaBoundsResponseItem.AsObject>,
  }
}

export class AreaBoundsResponseItem extends jspb.Message {
  getArea(): AreaShort | undefined;
  setArea(value?: AreaShort): AreaBoundsResponseItem;
  hasArea(): boolean;
  clearArea(): AreaBoundsResponseItem;

  getGroup(): Group | undefined;
  setGroup(value?: Group): AreaBoundsResponseItem;
  hasGroup(): boolean;
  clearGroup(): AreaBoundsResponseItem;

  getItemCase(): AreaBoundsResponseItem.ItemCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaBoundsResponseItem.AsObject;
  static toObject(includeInstance: boolean, msg: AreaBoundsResponseItem): AreaBoundsResponseItem.AsObject;
  static serializeBinaryToWriter(message: AreaBoundsResponseItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaBoundsResponseItem;
  static deserializeBinaryFromReader(message: AreaBoundsResponseItem, reader: jspb.BinaryReader): AreaBoundsResponseItem;
}

export namespace AreaBoundsResponseItem {
  export type AsObject = {
    area?: AreaShort.AsObject,
    group?: Group.AsObject,
  }

  export enum ItemCase { 
    ITEM_NOT_SET = 0,
    AREA = 1,
    GROUP = 2,
  }
}

export class LeadBoundsResponse extends jspb.Message {
  getItemsList(): Array<LeadBoundsResponseItem>;
  setItemsList(value: Array<LeadBoundsResponseItem>): LeadBoundsResponse;
  clearItemsList(): LeadBoundsResponse;
  addItems(value?: LeadBoundsResponseItem, index?: number): LeadBoundsResponseItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadBoundsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LeadBoundsResponse): LeadBoundsResponse.AsObject;
  static serializeBinaryToWriter(message: LeadBoundsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadBoundsResponse;
  static deserializeBinaryFromReader(message: LeadBoundsResponse, reader: jspb.BinaryReader): LeadBoundsResponse;
}

export namespace LeadBoundsResponse {
  export type AsObject = {
    itemsList: Array<LeadBoundsResponseItem.AsObject>,
  }
}

export class LeadBoundsResponseItem extends jspb.Message {
  getLead(): LeadShort | undefined;
  setLead(value?: LeadShort): LeadBoundsResponseItem;
  hasLead(): boolean;
  clearLead(): LeadBoundsResponseItem;

  getGroup(): Group | undefined;
  setGroup(value?: Group): LeadBoundsResponseItem;
  hasGroup(): boolean;
  clearGroup(): LeadBoundsResponseItem;

  getItemCase(): LeadBoundsResponseItem.ItemCase;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadBoundsResponseItem.AsObject;
  static toObject(includeInstance: boolean, msg: LeadBoundsResponseItem): LeadBoundsResponseItem.AsObject;
  static serializeBinaryToWriter(message: LeadBoundsResponseItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadBoundsResponseItem;
  static deserializeBinaryFromReader(message: LeadBoundsResponseItem, reader: jspb.BinaryReader): LeadBoundsResponseItem;
}

export namespace LeadBoundsResponseItem {
  export type AsObject = {
    lead?: LeadShort.AsObject,
    group?: Group.AsObject,
  }

  export enum ItemCase { 
    ITEM_NOT_SET = 0,
    LEAD = 1,
    GROUP = 2,
  }
}

export class LeadShort extends jspb.Message {
  getId(): number;
  setId(value: number): LeadShort;

  getTypeId(): number;
  setTypeId(value: number): LeadShort;

  getCoordinate(): Coordinate | undefined;
  setCoordinate(value?: Coordinate): LeadShort;
  hasCoordinate(): boolean;
  clearCoordinate(): LeadShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadShort.AsObject;
  static toObject(includeInstance: boolean, msg: LeadShort): LeadShort.AsObject;
  static serializeBinaryToWriter(message: LeadShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadShort;
  static deserializeBinaryFromReader(message: LeadShort, reader: jspb.BinaryReader): LeadShort;
}

export namespace LeadShort {
  export type AsObject = {
    id: number,
    typeId: number,
    coordinate?: Coordinate.AsObject,
  }
}

export class BoundsRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): BoundsRequest;

  getCoordinatesList(): Array<Coordinate>;
  setCoordinatesList(value: Array<Coordinate>): BoundsRequest;
  clearCoordinatesList(): BoundsRequest;
  addCoordinates(value?: Coordinate, index?: number): Coordinate;

  getDateStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateStart(value?: google_protobuf_timestamp_pb.Timestamp): BoundsRequest;
  hasDateStart(): boolean;
  clearDateStart(): BoundsRequest;

  getDateEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateEnd(value?: google_protobuf_timestamp_pb.Timestamp): BoundsRequest;
  hasDateEnd(): boolean;
  clearDateEnd(): BoundsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BoundsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: BoundsRequest): BoundsRequest.AsObject;
  static serializeBinaryToWriter(message: BoundsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BoundsRequest;
  static deserializeBinaryFromReader(message: BoundsRequest, reader: jspb.BinaryReader): BoundsRequest;
}

export namespace BoundsRequest {
  export type AsObject = {
    organizationCode: string,
    coordinatesList: Array<Coordinate.AsObject>,
    dateStart?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateEnd?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class AreaShort extends jspb.Message {
  getId(): number;
  setId(value: number): AreaShort;

  getName(): string;
  setName(value: string): AreaShort;

  getColor(): string;
  setColor(value: string): AreaShort;

  getCoordinatesList(): Array<Coordinate>;
  setCoordinatesList(value: Array<Coordinate>): AreaShort;
  clearCoordinatesList(): AreaShort;
  addCoordinates(value?: Coordinate, index?: number): Coordinate;

  getCenter(): Coordinate | undefined;
  setCenter(value?: Coordinate): AreaShort;
  hasCenter(): boolean;
  clearCenter(): AreaShort;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaShort.AsObject;
  static toObject(includeInstance: boolean, msg: AreaShort): AreaShort.AsObject;
  static serializeBinaryToWriter(message: AreaShort, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaShort;
  static deserializeBinaryFromReader(message: AreaShort, reader: jspb.BinaryReader): AreaShort;
}

export namespace AreaShort {
  export type AsObject = {
    id: number,
    name: string,
    color: string,
    coordinatesList: Array<Coordinate.AsObject>,
    center?: Coordinate.AsObject,
  }
}

export class UpdateLeadTypeRequest extends jspb.Message {
  getLeadType(): LeadType | undefined;
  setLeadType(value?: LeadType): UpdateLeadTypeRequest;
  hasLeadType(): boolean;
  clearLeadType(): UpdateLeadTypeRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateLeadTypeRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateLeadTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateLeadTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateLeadTypeRequest): UpdateLeadTypeRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateLeadTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateLeadTypeRequest;
  static deserializeBinaryFromReader(message: UpdateLeadTypeRequest, reader: jspb.BinaryReader): UpdateLeadTypeRequest;
}

export namespace UpdateLeadTypeRequest {
  export type AsObject = {
    leadType?: LeadType.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class LeadTypeIdRequest extends jspb.Message {
  getLeadTypeId(): number;
  setLeadTypeId(value: number): LeadTypeIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadTypeIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeadTypeIdRequest): LeadTypeIdRequest.AsObject;
  static serializeBinaryToWriter(message: LeadTypeIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadTypeIdRequest;
  static deserializeBinaryFromReader(message: LeadTypeIdRequest, reader: jspb.BinaryReader): LeadTypeIdRequest;
}

export namespace LeadTypeIdRequest {
  export type AsObject = {
    leadTypeId: number,
  }
}

export class CreateLeadTypeRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateLeadTypeRequest;

  getName(): string;
  setName(value: string): CreateLeadTypeRequest;

  getDescription(): string;
  setDescription(value: string): CreateLeadTypeRequest;

  getIconColor(): string;
  setIconColor(value: string): CreateLeadTypeRequest;

  getLabel(): string;
  setLabel(value: string): CreateLeadTypeRequest;

  getLabelColor(): string;
  setLabelColor(value: string): CreateLeadTypeRequest;

  getIsAssignable(): boolean;
  setIsAssignable(value: boolean): CreateLeadTypeRequest;

  getIsDisplayed(): boolean;
  setIsDisplayed(value: boolean): CreateLeadTypeRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLeadTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLeadTypeRequest): CreateLeadTypeRequest.AsObject;
  static serializeBinaryToWriter(message: CreateLeadTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLeadTypeRequest;
  static deserializeBinaryFromReader(message: CreateLeadTypeRequest, reader: jspb.BinaryReader): CreateLeadTypeRequest;
}

export namespace CreateLeadTypeRequest {
  export type AsObject = {
    organizationCode: string,
    name: string,
    description: string,
    iconColor: string,
    label: string,
    labelColor: string,
    isAssignable: boolean,
    isDisplayed: boolean,
  }
}

export class LeadType extends jspb.Message {
  getId(): number;
  setId(value: number): LeadType;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): LeadType;

  getCreatedById(): string;
  setCreatedById(value: string): LeadType;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): LeadType;
  hasDateCreated(): boolean;
  clearDateCreated(): LeadType;

  getUpdatedById(): string;
  setUpdatedById(value: string): LeadType;

  getDateUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateUpdated(value?: google_protobuf_timestamp_pb.Timestamp): LeadType;
  hasDateUpdated(): boolean;
  clearDateUpdated(): LeadType;

  getName(): string;
  setName(value: string): LeadType;

  getDescription(): string;
  setDescription(value: string): LeadType;

  getIconColor(): string;
  setIconColor(value: string): LeadType;

  getLabel(): string;
  setLabel(value: string): LeadType;

  getLabelColor(): string;
  setLabelColor(value: string): LeadType;

  getIsAssignable(): boolean;
  setIsAssignable(value: boolean): LeadType;

  getIsDisplayed(): boolean;
  setIsDisplayed(value: boolean): LeadType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadType.AsObject;
  static toObject(includeInstance: boolean, msg: LeadType): LeadType.AsObject;
  static serializeBinaryToWriter(message: LeadType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadType;
  static deserializeBinaryFromReader(message: LeadType, reader: jspb.BinaryReader): LeadType;
}

export namespace LeadType {
  export type AsObject = {
    id: number,
    organizationCode: string,
    createdById: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedById: string,
    dateUpdated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    name: string,
    description: string,
    iconColor: string,
    label: string,
    labelColor: string,
    isAssignable: boolean,
    isDisplayed: boolean,
  }
}

export class LeadNoteIdRequest extends jspb.Message {
  getId(): number;
  setId(value: number): LeadNoteIdRequest;

  getLeadId(): number;
  setLeadId(value: number): LeadNoteIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadNoteIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeadNoteIdRequest): LeadNoteIdRequest.AsObject;
  static serializeBinaryToWriter(message: LeadNoteIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadNoteIdRequest;
  static deserializeBinaryFromReader(message: LeadNoteIdRequest, reader: jspb.BinaryReader): LeadNoteIdRequest;
}

export namespace LeadNoteIdRequest {
  export type AsObject = {
    id: number,
    leadId: number,
  }
}

export class LeadNote extends jspb.Message {
  getId(): number;
  setId(value: number): LeadNote;

  getLeadId(): number;
  setLeadId(value: number): LeadNote;

  getCreatedById(): string;
  setCreatedById(value: string): LeadNote;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): LeadNote;
  hasDateCreated(): boolean;
  clearDateCreated(): LeadNote;

  getText(): string;
  setText(value: string): LeadNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadNote.AsObject;
  static toObject(includeInstance: boolean, msg: LeadNote): LeadNote.AsObject;
  static serializeBinaryToWriter(message: LeadNote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadNote;
  static deserializeBinaryFromReader(message: LeadNote, reader: jspb.BinaryReader): LeadNote;
}

export namespace LeadNote {
  export type AsObject = {
    id: number,
    leadId: number,
    createdById: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    text: string,
  }
}

export class CreateLeadNoteRequest extends jspb.Message {
  getLeadId(): number;
  setLeadId(value: number): CreateLeadNoteRequest;

  getText(): string;
  setText(value: string): CreateLeadNoteRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLeadNoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLeadNoteRequest): CreateLeadNoteRequest.AsObject;
  static serializeBinaryToWriter(message: CreateLeadNoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLeadNoteRequest;
  static deserializeBinaryFromReader(message: CreateLeadNoteRequest, reader: jspb.BinaryReader): CreateLeadNoteRequest;
}

export namespace CreateLeadNoteRequest {
  export type AsObject = {
    leadId: number,
    text: string,
  }
}

export class LeadIdRequest extends jspb.Message {
  getLeadId(): number;
  setLeadId(value: number): LeadIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LeadIdRequest): LeadIdRequest.AsObject;
  static serializeBinaryToWriter(message: LeadIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadIdRequest;
  static deserializeBinaryFromReader(message: LeadIdRequest, reader: jspb.BinaryReader): LeadIdRequest;
}

export namespace LeadIdRequest {
  export type AsObject = {
    leadId: number,
  }
}

export class CreateLeadRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateLeadRequest;

  getLeadTypeId(): number;
  setLeadTypeId(value: number): CreateLeadRequest;

  getCoordinate(): Coordinate | undefined;
  setCoordinate(value?: Coordinate): CreateLeadRequest;
  hasCoordinate(): boolean;
  clearCoordinate(): CreateLeadRequest;

  getAttributeValuesList(): Array<LeadAttributeValue>;
  setAttributeValuesList(value: Array<LeadAttributeValue>): CreateLeadRequest;
  clearAttributeValuesList(): CreateLeadRequest;
  addAttributeValues(value?: LeadAttributeValue, index?: number): LeadAttributeValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateLeadRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateLeadRequest): CreateLeadRequest.AsObject;
  static serializeBinaryToWriter(message: CreateLeadRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateLeadRequest;
  static deserializeBinaryFromReader(message: CreateLeadRequest, reader: jspb.BinaryReader): CreateLeadRequest;
}

export namespace CreateLeadRequest {
  export type AsObject = {
    organizationCode: string,
    leadTypeId: number,
    coordinate?: Coordinate.AsObject,
    attributeValuesList: Array<LeadAttributeValue.AsObject>,
  }
}

export class UpdateLeadRequest extends jspb.Message {
  getLead(): Lead | undefined;
  setLead(value?: Lead): UpdateLeadRequest;
  hasLead(): boolean;
  clearLead(): UpdateLeadRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateLeadRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateLeadRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateLeadRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateLeadRequest): UpdateLeadRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateLeadRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateLeadRequest;
  static deserializeBinaryFromReader(message: UpdateLeadRequest, reader: jspb.BinaryReader): UpdateLeadRequest;
}

export namespace UpdateLeadRequest {
  export type AsObject = {
    lead?: Lead.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class Lead extends jspb.Message {
  getId(): number;
  setId(value: number): Lead;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Lead;

  getCreatedById(): string;
  setCreatedById(value: string): Lead;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): Lead;
  hasDateCreated(): boolean;
  clearDateCreated(): Lead;

  getUpdatedById(): string;
  setUpdatedById(value: string): Lead;

  getDateUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateUpdated(value?: google_protobuf_timestamp_pb.Timestamp): Lead;
  hasDateUpdated(): boolean;
  clearDateUpdated(): Lead;

  getLeadTypeId(): number;
  setLeadTypeId(value: number): Lead;

  getCoordinate(): Coordinate | undefined;
  setCoordinate(value?: Coordinate): Lead;
  hasCoordinate(): boolean;
  clearCoordinate(): Lead;

  getAttributeValuesList(): Array<LeadAttributeValue>;
  setAttributeValuesList(value: Array<LeadAttributeValue>): Lead;
  clearAttributeValuesList(): Lead;
  addAttributeValues(value?: LeadAttributeValue, index?: number): LeadAttributeValue;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Lead.AsObject;
  static toObject(includeInstance: boolean, msg: Lead): Lead.AsObject;
  static serializeBinaryToWriter(message: Lead, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Lead;
  static deserializeBinaryFromReader(message: Lead, reader: jspb.BinaryReader): Lead;
}

export namespace Lead {
  export type AsObject = {
    id: number,
    organizationCode: string,
    createdById: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedById: string,
    dateUpdated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    leadTypeId: number,
    coordinate?: Coordinate.AsObject,
    attributeValuesList: Array<LeadAttributeValue.AsObject>,
  }
}

export class NoteIdRequest extends jspb.Message {
  getAreaId(): number;
  setAreaId(value: number): NoteIdRequest;

  getNoteId(): number;
  setNoteId(value: number): NoteIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NoteIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: NoteIdRequest): NoteIdRequest.AsObject;
  static serializeBinaryToWriter(message: NoteIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NoteIdRequest;
  static deserializeBinaryFromReader(message: NoteIdRequest, reader: jspb.BinaryReader): NoteIdRequest;
}

export namespace NoteIdRequest {
  export type AsObject = {
    areaId: number,
    noteId: number,
  }
}

export class AreaNoteList extends jspb.Message {
  getNotesList(): Array<AreaNote>;
  setNotesList(value: Array<AreaNote>): AreaNoteList;
  clearNotesList(): AreaNoteList;
  addNotes(value?: AreaNote, index?: number): AreaNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaNoteList.AsObject;
  static toObject(includeInstance: boolean, msg: AreaNoteList): AreaNoteList.AsObject;
  static serializeBinaryToWriter(message: AreaNoteList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaNoteList;
  static deserializeBinaryFromReader(message: AreaNoteList, reader: jspb.BinaryReader): AreaNoteList;
}

export namespace AreaNoteList {
  export type AsObject = {
    notesList: Array<AreaNote.AsObject>,
  }
}

export class CreateAreaNoteRequest extends jspb.Message {
  getAreaId(): number;
  setAreaId(value: number): CreateAreaNoteRequest;

  getText(): string;
  setText(value: string): CreateAreaNoteRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateAreaNoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateAreaNoteRequest): CreateAreaNoteRequest.AsObject;
  static serializeBinaryToWriter(message: CreateAreaNoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateAreaNoteRequest;
  static deserializeBinaryFromReader(message: CreateAreaNoteRequest, reader: jspb.BinaryReader): CreateAreaNoteRequest;
}

export namespace CreateAreaNoteRequest {
  export type AsObject = {
    areaId: number,
    text: string,
  }
}

export class AreaNote extends jspb.Message {
  getId(): number;
  setId(value: number): AreaNote;

  getAreaId(): number;
  setAreaId(value: number): AreaNote;

  getCreatedById(): string;
  setCreatedById(value: string): AreaNote;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): AreaNote;
  hasDateCreated(): boolean;
  clearDateCreated(): AreaNote;

  getText(): string;
  setText(value: string): AreaNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaNote.AsObject;
  static toObject(includeInstance: boolean, msg: AreaNote): AreaNote.AsObject;
  static serializeBinaryToWriter(message: AreaNote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaNote;
  static deserializeBinaryFromReader(message: AreaNote, reader: jspb.BinaryReader): AreaNote;
}

export namespace AreaNote {
  export type AsObject = {
    id: number,
    areaId: number,
    createdById: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    text: string,
  }
}

export class AssignMemberRequest extends jspb.Message {
  getAreaId(): number;
  setAreaId(value: number): AssignMemberRequest;

  getUserId(): string;
  setUserId(value: string): AssignMemberRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AssignMemberRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AssignMemberRequest): AssignMemberRequest.AsObject;
  static serializeBinaryToWriter(message: AssignMemberRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AssignMemberRequest;
  static deserializeBinaryFromReader(message: AssignMemberRequest, reader: jspb.BinaryReader): AssignMemberRequest;
}

export namespace AssignMemberRequest {
  export type AsObject = {
    areaId: number,
    userId: string,
  }
}

export class UpdateAreaRequest extends jspb.Message {
  getArea(): Area | undefined;
  setArea(value?: Area): UpdateAreaRequest;
  hasArea(): boolean;
  clearArea(): UpdateAreaRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateAreaRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateAreaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateAreaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateAreaRequest): UpdateAreaRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateAreaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateAreaRequest;
  static deserializeBinaryFromReader(message: UpdateAreaRequest, reader: jspb.BinaryReader): UpdateAreaRequest;
}

export namespace UpdateAreaRequest {
  export type AsObject = {
    area?: Area.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class AreaIdRequest extends jspb.Message {
  getAreaId(): number;
  setAreaId(value: number): AreaIdRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaIdRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AreaIdRequest): AreaIdRequest.AsObject;
  static serializeBinaryToWriter(message: AreaIdRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaIdRequest;
  static deserializeBinaryFromReader(message: AreaIdRequest, reader: jspb.BinaryReader): AreaIdRequest;
}

export namespace AreaIdRequest {
  export type AsObject = {
    areaId: number,
  }
}

export class CreateAreaRequest extends jspb.Message {
  getOrganizationCode(): string;
  setOrganizationCode(value: string): CreateAreaRequest;

  getName(): string;
  setName(value: string): CreateAreaRequest;

  getColor(): string;
  setColor(value: string): CreateAreaRequest;

  getCoordinatesList(): Array<Coordinate>;
  setCoordinatesList(value: Array<Coordinate>): CreateAreaRequest;
  clearCoordinatesList(): CreateAreaRequest;
  addCoordinates(value?: Coordinate, index?: number): Coordinate;

  getDateStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateStart(value?: google_protobuf_timestamp_pb.Timestamp): CreateAreaRequest;
  hasDateStart(): boolean;
  clearDateStart(): CreateAreaRequest;

  getDateEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateEnd(value?: google_protobuf_timestamp_pb.Timestamp): CreateAreaRequest;
  hasDateEnd(): boolean;
  clearDateEnd(): CreateAreaRequest;

  getMembersList(): Array<AreaMember>;
  setMembersList(value: Array<AreaMember>): CreateAreaRequest;
  clearMembersList(): CreateAreaRequest;
  addMembers(value?: AreaMember, index?: number): AreaMember;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateAreaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateAreaRequest): CreateAreaRequest.AsObject;
  static serializeBinaryToWriter(message: CreateAreaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateAreaRequest;
  static deserializeBinaryFromReader(message: CreateAreaRequest, reader: jspb.BinaryReader): CreateAreaRequest;
}

export namespace CreateAreaRequest {
  export type AsObject = {
    organizationCode: string,
    name: string,
    color: string,
    coordinatesList: Array<Coordinate.AsObject>,
    dateStart?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateEnd?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    membersList: Array<AreaMember.AsObject>,
  }
}

export class Area extends jspb.Message {
  getId(): number;
  setId(value: number): Area;

  getOrganizationCode(): string;
  setOrganizationCode(value: string): Area;

  getCreatedById(): string;
  setCreatedById(value: string): Area;

  getDateCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateCreated(value?: google_protobuf_timestamp_pb.Timestamp): Area;
  hasDateCreated(): boolean;
  clearDateCreated(): Area;

  getUpdatedById(): string;
  setUpdatedById(value: string): Area;

  getDateUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateUpdated(value?: google_protobuf_timestamp_pb.Timestamp): Area;
  hasDateUpdated(): boolean;
  clearDateUpdated(): Area;

  getName(): string;
  setName(value: string): Area;

  getColor(): string;
  setColor(value: string): Area;

  getDateStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateStart(value?: google_protobuf_timestamp_pb.Timestamp): Area;
  hasDateStart(): boolean;
  clearDateStart(): Area;

  getDateEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateEnd(value?: google_protobuf_timestamp_pb.Timestamp): Area;
  hasDateEnd(): boolean;
  clearDateEnd(): Area;

  getCoordinatesList(): Array<Coordinate>;
  setCoordinatesList(value: Array<Coordinate>): Area;
  clearCoordinatesList(): Area;
  addCoordinates(value?: Coordinate, index?: number): Coordinate;

  getCenter(): Coordinate | undefined;
  setCenter(value?: Coordinate): Area;
  hasCenter(): boolean;
  clearCenter(): Area;

  getMembersList(): Array<AreaMember>;
  setMembersList(value: Array<AreaMember>): Area;
  clearMembersList(): Area;
  addMembers(value?: AreaMember, index?: number): AreaMember;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Area.AsObject;
  static toObject(includeInstance: boolean, msg: Area): Area.AsObject;
  static serializeBinaryToWriter(message: Area, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Area;
  static deserializeBinaryFromReader(message: Area, reader: jspb.BinaryReader): Area;
}

export namespace Area {
  export type AsObject = {
    id: number,
    organizationCode: string,
    createdById: string,
    dateCreated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedById: string,
    dateUpdated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    name: string,
    color: string,
    dateStart?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateEnd?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    coordinatesList: Array<Coordinate.AsObject>,
    center?: Coordinate.AsObject,
    membersList: Array<AreaMember.AsObject>,
  }
}

export class Coordinate extends jspb.Message {
  getLatitude(): number;
  setLatitude(value: number): Coordinate;

  getLongitude(): number;
  setLongitude(value: number): Coordinate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Coordinate.AsObject;
  static toObject(includeInstance: boolean, msg: Coordinate): Coordinate.AsObject;
  static serializeBinaryToWriter(message: Coordinate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Coordinate;
  static deserializeBinaryFromReader(message: Coordinate, reader: jspb.BinaryReader): Coordinate;
}

export namespace Coordinate {
  export type AsObject = {
    latitude: number,
    longitude: number,
  }
}

export class AreaMember extends jspb.Message {
  getId(): number;
  setId(value: number): AreaMember;

  getAreaId(): number;
  setAreaId(value: number): AreaMember;

  getUserId(): string;
  setUserId(value: string): AreaMember;

  getDateAssigned(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateAssigned(value?: google_protobuf_timestamp_pb.Timestamp): AreaMember;
  hasDateAssigned(): boolean;
  clearDateAssigned(): AreaMember;

  getDateUnassigned(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateUnassigned(value?: google_protobuf_timestamp_pb.Timestamp): AreaMember;
  hasDateUnassigned(): boolean;
  clearDateUnassigned(): AreaMember;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaMember.AsObject;
  static toObject(includeInstance: boolean, msg: AreaMember): AreaMember.AsObject;
  static serializeBinaryToWriter(message: AreaMember, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaMember;
  static deserializeBinaryFromReader(message: AreaMember, reader: jspb.BinaryReader): AreaMember;
}

export namespace AreaMember {
  export type AsObject = {
    id: number,
    areaId: number,
    userId: string,
    dateAssigned?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateUnassigned?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export enum LeadAttributeType { 
  STRING = 0,
  NUMBER = 1,
  DATE = 2,
}
