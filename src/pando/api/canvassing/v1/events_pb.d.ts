import * as jspb from 'google-protobuf'

import * as pando_api_shared_pb from '../../../../pando/api/shared_pb';
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';


export class EventArea extends jspb.Message {
  getId(): number;
  setId(value: number): EventArea;

  getName(): string;
  setName(value: string): EventArea;

  getColor(): string;
  setColor(value: string): EventArea;

  getDateStart(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateStart(value?: google_protobuf_timestamp_pb.Timestamp): EventArea;
  hasDateStart(): boolean;
  clearDateStart(): EventArea;

  getDateEnd(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateEnd(value?: google_protobuf_timestamp_pb.Timestamp): EventArea;
  hasDateEnd(): boolean;
  clearDateEnd(): EventArea;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventArea.AsObject;
  static toObject(includeInstance: boolean, msg: EventArea): EventArea.AsObject;
  static serializeBinaryToWriter(message: EventArea, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventArea;
  static deserializeBinaryFromReader(message: EventArea, reader: jspb.BinaryReader): EventArea;
}

export namespace EventArea {
  export type AsObject = {
    id: number,
    name: string,
    color: string,
    dateStart?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    dateEnd?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class EventLead extends jspb.Message {
  getId(): number;
  setId(value: number): EventLead;

  getLeadTypeId(): number;
  setLeadTypeId(value: number): EventLead;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventLead.AsObject;
  static toObject(includeInstance: boolean, msg: EventLead): EventLead.AsObject;
  static serializeBinaryToWriter(message: EventLead, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventLead;
  static deserializeBinaryFromReader(message: EventLead, reader: jspb.BinaryReader): EventLead;
}

export namespace EventLead {
  export type AsObject = {
    id: number,
    leadTypeId: number,
  }
}

export class EventLeadType extends jspb.Message {
  getId(): number;
  setId(value: number): EventLeadType;

  getName(): string;
  setName(value: string): EventLeadType;

  getDescription(): string;
  setDescription(value: string): EventLeadType;

  getIconColor(): string;
  setIconColor(value: string): EventLeadType;

  getLabel(): string;
  setLabel(value: string): EventLeadType;

  getLabelColor(): string;
  setLabelColor(value: string): EventLeadType;

  getIsAssignable(): boolean;
  setIsAssignable(value: boolean): EventLeadType;

  getIsDisplayed(): boolean;
  setIsDisplayed(value: boolean): EventLeadType;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventLeadType.AsObject;
  static toObject(includeInstance: boolean, msg: EventLeadType): EventLeadType.AsObject;
  static serializeBinaryToWriter(message: EventLeadType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventLeadType;
  static deserializeBinaryFromReader(message: EventLeadType, reader: jspb.BinaryReader): EventLeadType;
}

export namespace EventLeadType {
  export type AsObject = {
    id: number,
    name: string,
    description: string,
    iconColor: string,
    label: string,
    labelColor: string,
    isAssignable: boolean,
    isDisplayed: boolean,
  }
}

export class EventNote extends jspb.Message {
  getId(): number;
  setId(value: number): EventNote;

  getText(): string;
  setText(value: string): EventNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventNote.AsObject;
  static toObject(includeInstance: boolean, msg: EventNote): EventNote.AsObject;
  static serializeBinaryToWriter(message: EventNote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventNote;
  static deserializeBinaryFromReader(message: EventNote, reader: jspb.BinaryReader): EventNote;
}

export namespace EventNote {
  export type AsObject = {
    id: number,
    text: string,
  }
}

export class NoteModified extends jspb.Message {
  getNote(): EventNote | undefined;
  setNote(value?: EventNote): NoteModified;
  hasNote(): boolean;
  clearNote(): NoteModified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): NoteModified;
  hasUserData(): boolean;
  clearUserData(): NoteModified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NoteModified.AsObject;
  static toObject(includeInstance: boolean, msg: NoteModified): NoteModified.AsObject;
  static serializeBinaryToWriter(message: NoteModified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NoteModified;
  static deserializeBinaryFromReader(message: NoteModified, reader: jspb.BinaryReader): NoteModified;
}

export namespace NoteModified {
  export type AsObject = {
    note?: EventNote.AsObject,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class AreaModified extends jspb.Message {
  getArea(): EventArea | undefined;
  setArea(value?: EventArea): AreaModified;
  hasArea(): boolean;
  clearArea(): AreaModified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AreaModified;
  hasUserData(): boolean;
  clearUserData(): AreaModified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaModified.AsObject;
  static toObject(includeInstance: boolean, msg: AreaModified): AreaModified.AsObject;
  static serializeBinaryToWriter(message: AreaModified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaModified;
  static deserializeBinaryFromReader(message: AreaModified, reader: jspb.BinaryReader): AreaModified;
}

export namespace AreaModified {
  export type AsObject = {
    area?: EventArea.AsObject,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class AreaAssignmentChange extends jspb.Message {
  getAreaId(): number;
  setAreaId(value: number): AreaAssignmentChange;

  getChangedAssigneeUserId(): string;
  setChangedAssigneeUserId(value: string): AreaAssignmentChange;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): AreaAssignmentChange;
  hasUserData(): boolean;
  clearUserData(): AreaAssignmentChange;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AreaAssignmentChange.AsObject;
  static toObject(includeInstance: boolean, msg: AreaAssignmentChange): AreaAssignmentChange.AsObject;
  static serializeBinaryToWriter(message: AreaAssignmentChange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AreaAssignmentChange;
  static deserializeBinaryFromReader(message: AreaAssignmentChange, reader: jspb.BinaryReader): AreaAssignmentChange;
}

export namespace AreaAssignmentChange {
  export type AsObject = {
    areaId: number,
    changedAssigneeUserId: string,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class LeadModified extends jspb.Message {
  getLead(): EventLead | undefined;
  setLead(value?: EventLead): LeadModified;
  hasLead(): boolean;
  clearLead(): LeadModified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): LeadModified;
  hasUserData(): boolean;
  clearUserData(): LeadModified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadModified.AsObject;
  static toObject(includeInstance: boolean, msg: LeadModified): LeadModified.AsObject;
  static serializeBinaryToWriter(message: LeadModified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadModified;
  static deserializeBinaryFromReader(message: LeadModified, reader: jspb.BinaryReader): LeadModified;
}

export namespace LeadModified {
  export type AsObject = {
    lead?: EventLead.AsObject,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export class LeadTypeModified extends jspb.Message {
  getLeadType(): EventLeadType | undefined;
  setLeadType(value?: EventLeadType): LeadTypeModified;
  hasLeadType(): boolean;
  clearLeadType(): LeadTypeModified;

  getUserData(): pando_api_shared_pb.UserMetadata | undefined;
  setUserData(value?: pando_api_shared_pb.UserMetadata): LeadTypeModified;
  hasUserData(): boolean;
  clearUserData(): LeadTypeModified;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LeadTypeModified.AsObject;
  static toObject(includeInstance: boolean, msg: LeadTypeModified): LeadTypeModified.AsObject;
  static serializeBinaryToWriter(message: LeadTypeModified, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LeadTypeModified;
  static deserializeBinaryFromReader(message: LeadTypeModified, reader: jspb.BinaryReader): LeadTypeModified;
}

export namespace LeadTypeModified {
  export type AsObject = {
    leadType?: EventLeadType.AsObject,
    userData?: pando_api_shared_pb.UserMetadata.AsObject,
  }
}

export enum Events { 
  NO_TYPE = 0,
  CANVASSING_AREA_CREATED = 1,
  CANVASSING_AREA_UPDATED = 2,
  CANVASSING_AREA_DELETED = 3,
  CANVASSING_AREA_ASSIGNED = 4,
  CANVASSING_AREA_UNASSIGNED = 5,
  CANVASSING_AREA_NOTE_CREATED = 6,
  CANVASSING_AREA_NOTE_DELETED = 7,
  CANVASSING_LEAD_CREATED = 10,
  CANVASSING_LEAD_DELETED = 11,
  CANVASSING_LEAD_UPDATED = 12,
  CANVASSING_LEAD_NOTE_CREATED = 13,
  CANVASSING_LEAD_NOTE_DELETED = 14,
  CANVASSING_LEAD_TYPE_CREATED = 20,
  CANVASSING_LEAD_TYPE_UPDATED = 21,
  CANVASSING_LEAD_TYPE_DELETED = 22,
}
