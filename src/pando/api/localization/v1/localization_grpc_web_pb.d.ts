import * as grpcWeb from 'grpc-web';

import * as pando_api_localization_v1_localization_pb from '../../../../pando/api/localization/v1/localization_pb';


export class LocalizationServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getLocalizedTextsFromServices(
    request: pando_api_localization_v1_localization_pb.GetTextsFromServicesRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_localization_v1_localization_pb.GetLocalizedTextResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_localization_v1_localization_pb.GetLocalizedTextResponse>;

  getLocalizedTextsFromLabels(
    request: pando_api_localization_v1_localization_pb.GetTextsFromLabelsRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.RpcError,
               response: pando_api_localization_v1_localization_pb.GetLocalizedTextResponse) => void
  ): grpcWeb.ClientReadableStream<pando_api_localization_v1_localization_pb.GetLocalizedTextResponse>;

}

export class LocalizationServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getLocalizedTextsFromServices(
    request: pando_api_localization_v1_localization_pb.GetTextsFromServicesRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_localization_v1_localization_pb.GetLocalizedTextResponse>;

  getLocalizedTextsFromLabels(
    request: pando_api_localization_v1_localization_pb.GetTextsFromLabelsRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<pando_api_localization_v1_localization_pb.GetLocalizedTextResponse>;

}

