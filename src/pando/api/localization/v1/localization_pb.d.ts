import * as jspb from 'google-protobuf'

import * as google_api_annotations_pb from '../../../../google/api/annotations_pb';
import * as pando_api_localization_v1_services_pb from '../../../../pando/api/localization/v1/services_pb';
import * as pando_api_localization_v1_labels_pb from '../../../../pando/api/localization/v1/labels_pb';
import * as pando_api_localization_v1_locales_pb from '../../../../pando/api/localization/v1/locales_pb';


export class LocalizedText extends jspb.Message {
  getText(): string;
  setText(value: string): LocalizedText;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocalizedText.AsObject;
  static toObject(includeInstance: boolean, msg: LocalizedText): LocalizedText.AsObject;
  static serializeBinaryToWriter(message: LocalizedText, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocalizedText;
  static deserializeBinaryFromReader(message: LocalizedText, reader: jspb.BinaryReader): LocalizedText;
}

export namespace LocalizedText {
  export type AsObject = {
    text: string,
  }
}

export class GetTextsFromServicesRequest extends jspb.Message {
  getServicesList(): Array<pando_api_localization_v1_services_pb.Service>;
  setServicesList(value: Array<pando_api_localization_v1_services_pb.Service>): GetTextsFromServicesRequest;
  clearServicesList(): GetTextsFromServicesRequest;
  addServices(value: pando_api_localization_v1_services_pb.Service, index?: number): GetTextsFromServicesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTextsFromServicesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTextsFromServicesRequest): GetTextsFromServicesRequest.AsObject;
  static serializeBinaryToWriter(message: GetTextsFromServicesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTextsFromServicesRequest;
  static deserializeBinaryFromReader(message: GetTextsFromServicesRequest, reader: jspb.BinaryReader): GetTextsFromServicesRequest;
}

export namespace GetTextsFromServicesRequest {
  export type AsObject = {
    servicesList: Array<pando_api_localization_v1_services_pb.Service>,
  }
}

export class GetTextsFromLabelsRequest extends jspb.Message {
  getLabelsList(): Array<pando_api_localization_v1_labels_pb.Label>;
  setLabelsList(value: Array<pando_api_localization_v1_labels_pb.Label>): GetTextsFromLabelsRequest;
  clearLabelsList(): GetTextsFromLabelsRequest;
  addLabels(value: pando_api_localization_v1_labels_pb.Label, index?: number): GetTextsFromLabelsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTextsFromLabelsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTextsFromLabelsRequest): GetTextsFromLabelsRequest.AsObject;
  static serializeBinaryToWriter(message: GetTextsFromLabelsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTextsFromLabelsRequest;
  static deserializeBinaryFromReader(message: GetTextsFromLabelsRequest, reader: jspb.BinaryReader): GetTextsFromLabelsRequest;
}

export namespace GetTextsFromLabelsRequest {
  export type AsObject = {
    labelsList: Array<pando_api_localization_v1_labels_pb.Label>,
  }
}

export class GetLocalizedTextResponse extends jspb.Message {
  getEncoding(): CharacterEncoding;
  setEncoding(value: CharacterEncoding): GetLocalizedTextResponse;

  getLocale(): pando_api_localization_v1_locales_pb.Locale;
  setLocale(value: pando_api_localization_v1_locales_pb.Locale): GetLocalizedTextResponse;

  getLocalizedTextsList(): Array<LocalizedText>;
  setLocalizedTextsList(value: Array<LocalizedText>): GetLocalizedTextResponse;
  clearLocalizedTextsList(): GetLocalizedTextResponse;
  addLocalizedTexts(value?: LocalizedText, index?: number): LocalizedText;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetLocalizedTextResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetLocalizedTextResponse): GetLocalizedTextResponse.AsObject;
  static serializeBinaryToWriter(message: GetLocalizedTextResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetLocalizedTextResponse;
  static deserializeBinaryFromReader(message: GetLocalizedTextResponse, reader: jspb.BinaryReader): GetLocalizedTextResponse;
}

export namespace GetLocalizedTextResponse {
  export type AsObject = {
    encoding: CharacterEncoding,
    locale: pando_api_localization_v1_locales_pb.Locale,
    localizedTextsList: Array<LocalizedText.AsObject>,
  }
}

export enum CharacterEncoding { 
  UTF_8 = 0,
  UTF_16 = 1,
  UTF_32 = 2,
}
