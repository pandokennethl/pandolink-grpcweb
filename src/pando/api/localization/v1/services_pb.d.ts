import * as jspb from 'google-protobuf'



export enum Service { 
  TPV_TOOLS = 0,
  TPV_SURVEY = 1,
  DOCUMENTS = 2,
  USERS = 3,
}
